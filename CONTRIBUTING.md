# CONTRIBUTION GUIDELINES AND CODE CONVENTIONS

(document in process of creation)

## Submitting a bug report

Please, use https://gitlab.com/EugeneLoza/decoherence/issues to create a bug report. Also try to stick to the rule "one report - one issue".

## Submitting an idea

Please, also use GitLab issues as above.

## Contribute code

Please, follow Castle Game Engine contribution guidelines:
https://github.com/castle-engine/castle-engine/wiki/Coding-Conventions

There are some minor differences in code style:

* Always use CAPITALS for compiler directives and CamelCase for `$DEFINE` values. E.g. `{$IFDEF DesktopVairable}...{$ELSE}...{$ENDIF}`

* "inc", "dec", "true", "false" keywords are exception and are written in lowercase.

* It's fine (and even advised) to use c-style operators such as "+=", just be careful :)

* Use extended names for compiler directives. E.g. not `{$I somefile.inc}` but `{$INCLUDE somefile.inc}`. Otherwise add a comment on what the compiler directive does (exception is `{$R+}{$Q+}`).

* Always use lowercase for single-letter variables like `i`, `j`, `k`.
First letter is lowercase in words "tmp" and "temp", e.g. `tmpVariable: integer`, `tempFile: File`.

* Use `DClassName` instead of `TClassName` for game-specific objects/classes/records/enums
Not sure if this is wise, but this is made to make a difference with regular FPC or CGE related classes.

* Prefer longer and explanatory names, better if unique, for entities unless it is a local/nested variable/routine. E.g. `SourceData` instead of `Src`.
E.g. you can easily find `SourceStringDictionary` in the code, wherever it is used, but looking for a specific `Source` variable over hundreds of units is not something easily done.

* Always comment every API element, even if its meaning is obvious. A bonus is a comment even for `private` fields.
Virtual procedures usually should receive comment only once - during the first definition, unless `override` significantly changes its behavior.
Leave `{}` if comment is missing for some type/variable/routine definition. This makes it easier to find such missing comments automatically.

* Don't forget to properly comment your code, especially in case it does something inobvious. Use `(* ... *)` comment style to comment different segments of the code (e.g. separate positive or negative influence, or discern between mouse and keyboard behavior), also use this style to briefly describe what a `Unit` does. Use `//...` style comment for one-line or end-line comments. Use `{...}` style comments for all other occasions.

* For API commenting, try to stick with PasDoc specifications. We shall certainly use it in future (it's already usable, just no need for that yet).

* Use a dot-spacer before the `implementation` part and also before the initialization part (and every routine related to initalization). Like this:
```
var
  x: integer;

procedure IncX;
procedure DecX;
procedure InitX;
{............................................................................}
implementation

procedure IncX;
begin
  inc(x);
end;

{-----------------------------------------------------------------------------}

procedure DecX;
begin
  inc(x);
end;

{............................................................................}

procedure InitX;
begin
  x := 0;
end;

end.
```

* Split routines relative to different classes with doubleline `{========================================}`. If part is significant, write a short descriptive name in this line to explain what part of the code follows.

* Never put anything into `initialization`..`finalization` parts. Create a `InitXxx` procedure and reference it in `DecoInit`. This allows to control the correct initialization flow as some routines may expect others already initialized (e.g. to output log correctly). The only exception are tools intended for debugging and are desktop-only, like `Profiler`, however, pay attention that in some cases bugs are more than expected.

* Always put `{$INCLUDE compilerconfig.inc}` before the unit name. Even if it doesn't use any compiler options.

* Use `TryInline` macro instead of `inline`. Note that `TryInline` is not followed by a semicolon: `procedure Foo; TryInline`.

* Always reference explicit `inherited` routine reference, e.g. `inherited Create;`, not just `inherited;`.

* Prefer American English. E.g. "behavior", not "behaviour".

* Always use `Result` variable to assign the `function` result.

* Put all sources into `src` folder. Choose or create an appropriate sub-folder.

* Do not change `version.inc` file, it's the version of master branch.

* Prefer explanatory names for commits and add a valid reference to issues (if required). E.g. not "commit 8", but "make Update global, fixes https://gitlab.com/eugeneloza/decoherence/issues/18570".
No need to be overly talkative, if the commit is trivial thou (e.g. adding a comment or a minor improvement).

* Try to avoid large and many-fold commits, but split them into small commits, each addressing a single issue.

* Put constructor/destructor before each class other routines or after them (but not in the middle).

# Specific types

* `DFloat` is equivalent to `Single` where applicable. If Castle Game Engine or other routine suggests `Single` type, then the type should be explicitly declared as `Single`. However, if the type is expected to be used mostly in game, `DFloat` should be used. Reason is simple, it would be easy to test if some bug is caused by insufficient accuarcy of `Single` or by other causes, simply replacing `DFloat = Double`.

* `DInt8`, `DInt16`, `DInt32`, `DInt64` are integer types, all aliases for `Integer`. However, maybe in future they will be optimized. `DInt8` is expected to be used in situations where small (positive?) number is expected, e.g. number of characters or buttons on screen. `DInt64` should be used when a huge integer is expected (like 64 bit random seed). In all normal cases `DInt32` should be used. Again, if Castle Game Engine or other routine accepts `Integer`, `Cardinal` or `Int64` or other specific integer type, then the specific integer type should also be used. Also: always use `Integer` for loop variables as they are optimized by the compiler.

## Description of folder structure

All the game sources (except core project settings files) are located in `src` folder, it has the following structure:

* **core**  - all the core game units are located here. These are units that to some extent affect all the game or are universal and used by different types of other units (like DecoTimer used in both GUI and Actors).

* **input** - everything directly related to user input (keyboard, mouse, some basic processing, all low-level).

* **readwrite** - everything directly related to external files being loaded or containing methods used in loading files.

* **world** - locations and worlds generation and management units. Also basic 3D models operation goes here.

* **actor** - everything related to Actors (including AI and bodies)

* **generated** - all units automatically generated by some tools.

* **scripting** - everything related to scenario and scripting, including context feature.

* **gui** - all GUI elements implementations.

* **player** - high-level player input and management.

* **text** - everything related to translations and working with texts in game.

* **constructor** - Constructor is a WYSIWYG tool to manage game data.

* **constructor/components** - not directly related to Constructor, but extensions to Lazarus LCL.

Temporary units/projects, used for testing purposes are locaed in `test` folder.

All game data is stored in `data` folder

* **GUI** - contains images, fonts, etc., used in GUI
