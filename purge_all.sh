# Do a basic clean-up

bash purge.sh

# Remove compiled executables

rm Decoherence
rm Decoherence.exe
rm ConstructorTool

# Remove test executables

rm test/Context/ContextTest
rm test/Context/ContextTest.exe

# Remove screenshots

rm *.png

# Remove saved games and config

rm Configuration -rf
rm SavedGames -rf
