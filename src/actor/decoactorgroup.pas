{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* A group of actors that can control its members *)

{$INCLUDE compilerconfig.inc}

unit DecoActorGroup;

interface

uses
  Generics.Collections,
  DecoBaseActor,
  DecoGlobal;

type
  { A list of Actors }
  DActorList = specialize TObjectList<DBaseActor>;

type
  { A group of actors, with coordinated actions and managed together }
  DActorGroup = class(DObject)
  public
    { Actors in this group }
    FActorList: DActorList;
  public
    constructor Create; virtual; //override;
    destructor Destroy; override;
  end;

{............................................................................}
implementation
uses
  DecoLog {%Profiler%};

constructor DActorGroup.Create;
begin
  //inherited Create; <------ Parent is empty
  FActorList := DActorList.Create(false);
end;

{----------------------------------------------------------------------------}

destructor DActorGroup.Destroy;
begin
  FActorList.Free;
  inherited Destroy;
end;

end.

