{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Base actor - posessing stats and location, but no body or AI *)

{$INCLUDE compilerconfig.inc}

unit DecoBaseActor;

interface

uses
  DecoDuoRandom,
  DecoActorStats, DecoNavigation, DecoInfluence, DecoPerks,
  DecoGlobal;

type
  { Type, determining the type of the creature
    then it is linked to its body and stats }
  DCreatureType = String;

type
  { The most simple actor type.
    This actor doesn't have any stats except location and creature type. }
  DSimpleActor = class(DObject)
  strict protected
    { Current location of the Actor over discrete NAV }
    Nav: TNav;
  public
    { What kind of creature is this }
    CreatureType: DCreatureType;
    { Teleport this Actor to aNav }
    procedure TeleportTo(const aNav: TNav); virtual;
    { Procedures preformed on this actor every frame
      ?This should not be abstract? }
    procedure Manage; virtual; abstract;
  public
    (* Some macros for this Actor affinity with other factions
       Required to be read by some management routines. *)
    { Is this actor a player character? }
    function isPlayer: Boolean;
    { Is this actor ally of player's faction? }
    function isPlayerAlly: Boolean;
    { Is this actor hostile to player's faction? }
    function isHostile: Boolean;
    { Is this actor neutral to player's faction? }
    function isNeutral: Boolean;
  public
    constructor Create; virtual; //override;
  end;

type
  { Current alert state of the Actor }
  TAlertState = (
    { Actor does not suspect any hostilities and
      is idle relaxed, maybe doing his chores }
    asRelaxed,
    { Actor anticipates danger, but doesn't see enemy yet,
      or does not want to initiate combat }
    asAlert,
    { Actor stands ready to fight }
    asCombat);

type
  { Basic actor, with stats }
  DBaseActor = class(DSimpleActor)
  strict protected
    { Offence/Defence random for this Actor }
    FRandom: DDuoRandom;
    { What happens if this actor is influenced by some effect }
    onAffect: TInfluenceEvent;
    { What target is this Actor "aiming" at? }
    FTarget: DBaseActor;
    { Automatically choose target for this Actor,
      Note: It only selects any "valid" target, suitable for current Action
            (i.e. if current target has died this is responsible for choosing another)
            Priroritizing of targets and changing Actions should be done in AI }
    function AutoTarget: DBaseActor;
    {}
    function GetTarget: DBaseActor;
  public
    { Basic stats of the Actor;
      some of these are player-character only? }
    Hp, Sta, Cnc, Mph: DStat;
    { Alert state of the Actor}
    AlertState: TAlertState;
    { is this Actor capable of magic?
      this is player-character only? }
    function isMage: Boolean;
    { Affect this Actor with some Influence,
      e.g. a successful sword hit would be Affect("itPhysicalDamage,10,1") }
    { Is this actor alive? }
    function isAlive: Boolean;
    { Call-back for this Actor to react for Influence }
    procedure Affect(const Influence: DInfluence);
    { Completely heal the Actor and eliminate all active effects. }
    procedure ResetStats;
    { Return current target of this Actor }
    property Target: DBaseActor read GetTarget;
  strict private
    { Cached action of this actor }
    FAction: DAction;
    { Read action of current actor, if none or not valid, choose an appropriate }
    function GetAction: DAction;
    {}
    function DefaultAction: DAction;
    { Assign action to this actor }
    procedure SetAction(aValue: DAction);
  public
    { Call-back for changed action (e.g. for GUI) }
    OnActionChanged: TSimpleProcedure;
    { Action of this Actor }
    property Action: DAction read GetAction write SetAction;
  public
    procedure Manage; override;
  public
    constructor Create; override;
    destructor Destroy; override;
  end;

type
  { This is a temporary character with some stats }
  DTemporaryCharacterHelper = class helper for DBaseActor
    procedure TempCharacter;
  end;

{............................................................................}
implementation
uses
  DecoArchitect,
  DecoPlayerCharacter, //I don't like the cyclic reference, but it's allowed; Using it only to check if the actor is Player or not.
  DecoLog {%Profiler%};

procedure DSimpleActor.TeleportTo(const aNav: TNav);
begin
  {StartProfiler}

  if Architect.CurrentWorld.BlockNav(aNav) then
  begin
    Architect.CurrentWorld.ReleaseNav(Nav);
    Nav := aNav;
  end;
  //Warning: actor may be actually located in another World, i.e. beyond the current NAV

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

function DSimpleActor.isPlayer: Boolean;
begin
  Result := Self is DPlayerCharacter;
end;

{-----------------------------------------------------------------------------}

function DSimpleActor.isPlayerAlly: Boolean;
begin
  Result := Self.IsPlayer and true; {todo}
end;

{-----------------------------------------------------------------------------}

function DSimpleActor.isHostile: Boolean;
begin
  Result := (not Self.IsPlayerAlly) and (not Self.IsNeutral); {todo}
end;

{-----------------------------------------------------------------------------}

function DSimpleActor.isNeutral: Boolean;
begin
  Result := false; {todo}
end;

{-----------------------------------------------------------------------------}

constructor DSimpleActor.Create;
begin
  //nothing?
end;

{=============================================================================}

constructor DBaseActor.Create;
begin
  inherited Create;
  FRandom := DDuoRandom.Create;
  FRandom.Init(0, 0); //should be loaded from hdd
end;

{-----------------------------------------------------------------------------}

destructor DBaseActor.Destroy;
begin
  FRandom.Free;
  inherited Destroy;
end;

{-----------------------------------------------------------------------------}

function DBaseActor.isMage: Boolean;
begin
  Result := Mph.Value[2] > 0;
end;

{-----------------------------------------------------------------------------}

function DBaseActor.isAlive: Boolean;
begin
  Result := HP.Value[0] > 0;
end;

{-----------------------------------------------------------------------------}

function DBaseActor.GetAction: DAction;
begin
  if FAction <> nil then
    Result := FAction
  else
    Result := DefaultAction;
end;

{-----------------------------------------------------------------------------}

function DBaseActor.DefaultAction: DAction;
begin
  Result := GetActionByName('MeleeAttack');
end;

{-----------------------------------------------------------------------------}

procedure DBaseActor.SetAction(aValue: DAction);
begin
  if (aValue <> FAction) then
  begin
    FAction := aValue;
    if Assigned(OnActionChanged) then
      OnActionChanged;
  end;
end;

{-----------------------------------------------------------------------------}

function DBaseActor.AutoTarget: DBaseActor;
begin
  Result := nil;
end;

{-----------------------------------------------------------------------------}

function DBaseActor.GetTarget: DBaseActor;
begin
  { This procedure only returns a cached target!
    Looking for a "more suitable" target is done in AI }
  if (FTarget = nil) or (not FTarget.isAlive) then
    Result := AutoTarget
  else
    Result := FTarget;
end;

{-----------------------------------------------------------------------------}

procedure DBaseActor.ResetStats;
begin
  Hp.ResetToMaxMax;
  Sta.ResetToMaxMax;
  Cnc.ResetToMaxMax;
  Mph.ResetToMaxMax;
end;

{-----------------------------------------------------------------------------}

procedure DBaseActor.Manage;
begin
  //inherited Manage; <------- Parent is abstract
  //updates Actor's state here
end;

{-----------------------------------------------------------------------------}

procedure DBaseActor.Affect(const Influence: DInfluence);
begin
  {StartProfiler}

  //use a call-back for the Actor to react (somehow) to influence
  { we're using this before actual effects take place, so that they will be
    able to overwrite the current effect, not vice versa
    e.g. death animation will overwrite hurt animation. }
  if Assigned(onAffect) then
    onAffect(Influence);

  case Influence.InfluenceType of
    itPhysicalDamage: begin
        Self.Hp.ChangeCurrent(-Influence.Value, Influence.Skill);
        if Self.Hp.Value[0] < 0 then
          Self.Affect(KnockDown(Influence.Delay));
      end;
    itHealing: begin
        if (Self.Hp.Value[0] < 0) and (Influence.Value > -Self.Hp.Value[0]) then
          Self.Affect(HealingRecover(Influence.Delay));
        Self.Hp.ChangeCurrent(Influence.Value, Influence.Skill);
      end;
    itKnockDown: {add KnockDown state};
    itHealingRecover: {Remove "disabled" state};
    itDeath: {Kill the Actor};
    else
      Log(CurrentRoutine, 'Warning: invalid Influence ' + InfluenceTypeToString(Influence.InfluenceType));
  end;

  {StopProfiler}
end;

{============================================================================}

procedure DTemporaryCharacterHelper.TempCharacter;
begin
  Hp.SetMaxMax(100);
  Sta.SetMaxMax(100);
  Cnc.SetMaxMax(100);
  Mph.SetMaxMax(100);
  ResetStats;
  Cnc.ChangeCurrent(-DRND.Random(80), DRND.Random);
  Sta.ChangeCurrent(-DRND.Random(80), DRND.Random);
  Hp.ChangeCurrent(-DRND.Random(80), DRND.Random);
  Mph.ChangeCurrent(-DRND.Random(80), DRND.Random);
end;

end.

