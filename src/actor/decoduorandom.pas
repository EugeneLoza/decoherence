{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Three randoms, for Actors to provide for deterministic
   offence/defence rolls *)

{$INCLUDE compilerconfig.inc}

unit DecoDuoRandom;

interface

uses
  CastleRandom,
  DecoGlobal;

type
  { Random providing for three distinctive rolls:
    one for offence, one for defence and one for anything else
    This may be optimized}
  DDuoRandom = class(DObject)
  public
    { Offence and Defence random number generators }
    OffenceRandom: TCastleRandom;
    DefenceRandom: TCastleRandom;
    { Init random with predefined seeds }
    procedure Init(const OffenceSeed, DefenceSeed: LongWord);
    { Init random with random seeds }
    procedure Init;
  public
    { Pay attention: random numbers generators are created }
    constructor Create; //override; <----- non-virtual constructor
    destructor Destroy; override;
  end;

{............................................................................}
implementation
uses
  DecoLog {%Profiler%};

procedure DDuoRandom.Init(const OffenceSeed, DefenceSeed: LongWord);
begin
  OffenceRandom.Initialize(OffenceSeed);
  DefenceRandom.Initialize(DefenceSeed);
end;

{----------------------------------------------------------------------------}

procedure DDuoRandom.Init;
begin
  OffenceRandom.Initialize(0);
  DefenceRandom.Initialize(0);
end;

{----------------------------------------------------------------------------}

constructor DDuoRandom.Create;
begin
  //inherited Create <---------- Parent is empty
  { Randoms are created uninitialized }
  OffenceRandom := TCastleRandom.Create(1);
  DefenceRandom := TCastleRandom.Create(1);
end;

{----------------------------------------------------------------------------}

destructor DDuoRandom.Destroy;
begin
  OffenceRandom.Free;
  DefenceRandom.Free;
  inherited Destroy;
end;


end.

