{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Actor with a body *)

{$INCLUDE compilerconfig.inc}

unit DecoEmbodiedActor;

interface

uses
  CastleVectors,
  DecoBaseActor, DecoBody, DecoInfluence,
  DecoGlobal;

type
  { Actor with a body and relative functions }
  DEmbodiedActor = class(DBaseActor)  //abstract?
  public
    { Body of this actor. May be nil. }
    FBody: DBody;
  strict protected
    { Initializes this actor with a random direction }
    procedure GetRandomDirection;
    { Causes actor to react to external influences
      e.g. being hit or killed, or healed }
    procedure doAffect(const Influence: DInfluence);
  strict private
    procedure SetTranslation(const aTranslation: TVector3);
    function GetTranslation: TVector3;
    procedure SetDirection(const aDirection: TVector3);
    function GetDirection: TVector3;
    procedure SetRotation(const aRotation: TVector4);
    function GetRotation: TVector4;
  public
    { Exact current of this Actor in 3D world }
    property Translation: TVector3 read GetTranslation write SetTranslation;
    { Exact current of this Actor in 3D world }
    property Direction: TVector3 read GetDirection write SetDirection;
    { Exact rotation of this Actor in 3D world }
    property Rotation: TVector4 read GetRotation write SetRotation;
    { Returns desired minimal FPS of the animation in fractions of a second
      based on current playing animation. }
    function GetAnimationFPS: DFloat;
    procedure Manage; override;
  public
    constructor Create; override;
    destructor Destroy; override;
  end;

{............................................................................}
implementation
uses
  SysUtils,
  CastleCameras,
  DecoAnimations,
  DecoLog {%Profiler%};

procedure DEmbodiedActor.SetTranslation(const aTranslation: TVector3);
begin
  if FBody <> nil then
    FBody.Translation := aTranslation
  else
    Log(CurrentRoutine, 'Warning: Actor has no body!');
end;
function DEmbodiedActor.GetTranslation: TVector3;
begin
  if FBody <> nil then
    Result := FBody.Translation
  else
    Log(CurrentRoutine, 'Warning: Actor has no body!');
end;

{-----------------------------------------------------------------------------}

procedure DEmbodiedActor.SetDirection(const aDirection: TVector3);
begin
  if FBody <> nil then
    FBody.Direction := aDirection
  else
    Log(CurrentRoutine, 'Warning: Actor has no body!');
end;
function DEmbodiedActor.GetDirection: TVector3;
begin
  if FBody <> nil then
    Result := RotatePointAroundAxis(GetRotation, Vector3(0, 1, 0))//FBody.Direction
  else
    Log(CurrentRoutine, 'Warning: Actor has no body!');
end;

{-----------------------------------------------------------------------------}

procedure DEmbodiedActor.SetRotation(const aRotation: TVector4);
begin
  if FBody <> nil then
    FBody.Rotation := aRotation
  else
    Log(CurrentRoutine, 'Warning: Actor has no body!');
end;
function DEmbodiedActor.GetRotation: TVector4;
begin
  if FBody <> nil then
    Result := FBody.Rotation
  else
    Log(CurrentRoutine, 'Warning: Actor has no body!');
end;

{-----------------------------------------------------------------------------}

function DEmbodiedActor.GetAnimationFPS: DFloat;
begin
  case FBody.CurrentAnimationName of
    atIdle: Result := 1/5;
    atAttack, atHurt, atDie: Result := 1/40;
    //atSpeak: Result := 1/25;
    else
      Result := 1/15;
  end;
end;

{-----------------------------------------------------------------------------}

procedure DEmbodiedActor.GetRandomDirection;
var
  Phi: single;
begin
  Phi := 2 * Pi * DRND.Random;
  Rotation := Vector4(0, 0, 1, Phi);
end;

{-----------------------------------------------------------------------------}

procedure DEmbodiedActor.doAffect(const Influence: DInfluence);
begin
  {StartProfiler}

  if (FBody <> nil) then
  begin
    case Influence.InfluenceType of
      itPhysicalDamage: begin
          if Abs(Influence.Value) > Hp.Value[0] / 3 then
            FBody.DelayAnimation(atHurtCritical, Influence.Delay) //if damage taken > 1/3 hp then atHurtCritical
          else
          if Abs(Influence.Value) < Hp.Value[0] / 20 then
            FBody.DelayAnimation(atHurtMinor, Influence.Delay) //if damage taken < 5% hp then atMinorCritical
          else
            FBody.DelayAnimation(atHurt, Influence.Delay);
          //if Hp < 0 then another influence with death request will be sent, we don't process it here
        end;
      itHealing: FBody.DelayAnimation(atHealing, Influence.Delay);
      itKnockDown: FBody.DelayAnimation(atKnockDown, Influence.Delay);
      itHealingRecover: FBody.DelayAnimation(atStandUp, Influence.Delay);
      itDeath: FBody.DelayAnimation(atDie, Influence.Delay);

      else
        Log(CurrentRoutine, 'Warning: invalid Influence ' + InfluenceTypeToString(Influence.InfluenceType));    end;
  end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DEmbodiedActor.Manage;
begin
  {StartProfiler}

  inherited Manage;
  if FBody <> nil then
    FBody.Manage;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

constructor DEmbodiedActor.Create;
begin
  inherited Create;
  onAffect := @doAffect;
end;

{-----------------------------------------------------------------------------}

destructor DEmbodiedActor.Destroy;
begin
  FreeAndNil(FBody);
  inherited Destroy;
end;

end.

