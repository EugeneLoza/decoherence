{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* A information carrier to transfer an influence quant to an Actor (affect an Actor)
   In other words, this is an API for most effects applied to actors.
   Some influences may directly change Actor's stats (like damage health)
   Others may apply statuses.
   If an attack (or any other influence) changes more than one quant, then it should
   send multiple quants sequentially.

   This is EXPERIMENTAL, I strongly believe it will change significantly
   Now just building a proper architecture. *)

{$INCLUDE compilerconfig.inc}

unit DecoInfluence;

interface

uses
  DecoGlobal, DecoTime;

type
  { How this influence affects the character
    Warning: adding one here needs also add it in:
      * DecoBaseActor.Affect
      * DecoEmbodiedActor.doAffect
      * StringToInfluenceType / InfluenceTypeToString }
  TInfluenceType = (
    //this one should never happen in normal flow

    { caused by ERROR of some sort }
    itUndefined,

    //basic effects

    { Target receives direct physical damage to HP }
    itPhysicalDamage,
    { Target receives direct healing to HP }
    itHealing,

    //state change effects

    { Target HP < 0, issued by Target itself }
    itKnockDown,
    { Target recovers (from knock-down or similar state) due to healing and stands up }
    itHealingRecover,
    { Target MaxHP < 0, issued by Target itself }
    itDeath);

type
  { Influence quant - affects Actor in a single way
    Pay attention that Influence is raw - all other factors
    are determined when the Influence is created (and if it is created at all:
    including target armor, state, perks and everything)
    and the Influence itself directly affects the Actor. }
  DInfluence = record
    { Type of influence }
    InfluenceType: TInfluenceType;
    { Strength of influence, sometimes this is irrelevant, e.g. itDeath has no strength }
    Value: DFloat;
    { Additional "power" of this influence, in most cases this is "quality" of Value,
      However, it may also carry some arbitrary second parameter }
    Skill: DFloat;
    { A reaction to influence may be delayed (in seconds)
      e.g. in case "attack" animation does not land "blow" on the very first frame,
      but there is some non-zero swing time }
    Delay: DTime;
  end;

type
  { Event for reacting to influence received }
  TInfluenceEvent = procedure(const Influence: DInfluence) of object;

{ Converting InfluenceType to String }
function InfluenceTypeToString(const aInfluenceType: TInfluenceType): String;
{ Converting String to InfluenceType }
function StringToInfluenceType(const aString: String): TInfluenceType;

{ Some "macros" to easily construct DInfluence instance in often-occurring situations }
function Hit(const aValue, aSkill: DFloat; const aDelay: DTime): DInfluence;
function Heal(const aValue, aSkill: DFloat; const aDelay: DTime): DInfluence;
function KnockDown(const aDelay: DTime): DInfluence;
function HealingRecover(const aDelay: DTime): DInfluence;
{.......................................................................}
implementation
uses
  SysUtils, TypInfo,
  DecoLog {%Profiler%};

function InfluenceTypeToString(const aInfluenceType: TInfluenceType): String;
begin
  Result := GetEnumName(TypeInfo(TInfluenceType), Ord(aInfluenceType));
end;
function StringToInfluenceType(const aString: String): TInfluenceType;
begin
  case aString of
    'itPhysicalDamage': Result := itPhysicalDamage;
    'itHealing':        Result := itHealing;
    'itKnockDown':      Result := itKnockDown;
    'itHealingRecover': Result := itHealingRecover;
    'itDeath':          Result := itDeath;
    else
    begin
      Result := itUndefined;
      Log(CurrentRoutine, 'Unable to parse influence type: ' + aString);
    end;
  end;
end;

{=============================================================================}

function Hit(const aValue, aSkill: DFloat; const aDelay: DTime): DInfluence;
begin
  Result.InfluenceType := itPhysicalDamage;
  Result.Value := aValue;
  Result.Skill := aSkill;
  Result.Delay := aDelay;
end;

{-----------------------------------------------------------------------------}

function Heal(const aValue, aSkill: DFloat; const aDelay: DTime): DInfluence;
begin
  Result.InfluenceType := itHealing;
  Result.Value := aValue;
  Result.Skill := aSkill;
  Result.Delay := aDelay;
end;

{-----------------------------------------------------------------------------}

function KnockDown(const aDelay: DTime): DInfluence;
begin
  Result.InfluenceType := itKnockDown;
  Result.Value := -1;
  Result.Skill := -1;
  Result.Delay := aDelay;
end;

function HealingRecover(const aDelay: DTime): DInfluence;
begin
  Result.InfluenceType := itHealingRecover;
  Result.Value := -1;
  Result.Skill := -1;
  Result.Delay := aDelay;
end;

end.
