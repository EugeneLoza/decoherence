{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Descriptions of perks and actions *)

{$INCLUDE compilerconfig.inc}

unit DecoPerks;

interface

uses
  Generics.Collections,
  DecoInfluence, DecoImages,
  DecoGlobal;

type
  { Is this perk passive or active
    PASSIVE perks are used automatically
            (such as passive damage modifiers or poison effect)
    ACTIVE  perks should be selected manually to be effective
            (always "costing" something,
             e.g. increase attack damage and stamina consumption) }
  DPerkType = (ptPassive, ptActive);
  { What action does this perk perform
    RULE: one perk = one action }
  DPerkAction = TInfluenceType; //temporary

type
  { type of Perk.Value }
  DPerkValue = DFloat;

type
  { Perk is a basic action quant, that can influence some target
    or mutliple targets. Perk can also just modify the current Action. }
  DPerk = class(DObject)
  public
    { Is this perk passive or active? }
    PerkType: DPerkType;
    { What action does this perk perform? }
    PerkAction: DPerkAction;
    { How strong is the performed action? }
    Value: DPerkValue;
    {{}
      Each perk has a unique image }
    Image: DImage;
  public
    destructor Destroy; override;
  end;

type
  { A non-exclusive list of several perks }
  DPerkList = specialize TObjectList<DPerk>;

type
  { At which target is the perk used }
  DTargetType = (
    { No target (?) }
    ttNone,
    { Targeted at active enemy target }
    ttEnemy,
    { Targeted at Actor performing the action }
    ttSelf,
    { Targeted at active ally target }
    ttAlly
  );

type
  { A basic object in game, used for many purposes.
    Most often - as Action or Status. }
  DMultiPerk = class(DObject)
  public
    { What type of target this Action influences }
    TargetType: DTargetType;
    { Perks in this multiperk }
    Perks: DPerkList;
    {{}
      Each multiperk has a unique image }
    Image: DImage;
    { Apply all perks in this Action to Target }
    procedure Affect(const Target: DObject);  //ugly fix to avoid circular reference
  public
    constructor Create; //override;
    destructor Destroy; override;
  end;

type
  {}
  DAction = class(DObject)
  public
    Multiperk: DMultiperk; //not owned
  end;

type
  { A dictionary of loaded perks to be referenced by Alias }
  DPerkDictionary = specialize TObjectDictionary<String, DPerk>;
  { A dictionary of loaded multiperks to be referenced by Alias }
  DMultiperkDictionary = specialize TObjectDictionary<String, DMultiPerk>;
  {}
  DActionDictionary = specialize TObjectDictionary<String, DAction>;

{ Get a perk by name }
function GetPerkByName(const aName: String): DPerk;
{ Get a multiperk by name }
function GetMultiperkByName(const aName: String): DMultiPerk;
{}
function GetActionByName(const aName: String): DAction;
{ Initialize and load perks }
procedure InitPerks;
{ Free variables used by perks }
procedure FreePerks;
{............................................................................}
implementation
uses
  SysUtils,
  DecoBaseActor, DecoPerksLoader,
  DecoLog {%Profiler%};

function GetPerkByName(const aName: String): DPerk;
begin
  if not PerkDictionary.TryGetValue(aName, Result) then
  begin
    Log(CurrentRoutine, 'ERROR: Unknown Perk: ' + aName);
    //Result := nil;
  end;
end;

{-----------------------------------------------------------------------------}

function GetMultiperkByName(const aName: String): DMultiPerk;
begin
  {StartProfiler}

  if not MultiperkDictionary.TryGetValue(aName, Result) then
  begin
    Log(CurrentRoutine, 'ERROR: Unknown Multiperk: ' + aName);
    Result := nil;
  end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

function GetActionByName(const aName: String): DAction;
begin
  {StartProfiler}

  if not ActionDictionary.TryGetValue(aName, Result) then
  begin
    Log(CurrentRoutine, 'ERROR: Unknown Action: ' + aName);
    Result := nil;
  end;

  {StopProfiler}
end;


{=============================================================================}

destructor DPerk.Destroy;
begin
  //FreeAndNil(Image);
  inherited Destroy;
end;

{=============================================================================}

procedure DMultiPerk.Affect(const Target: DObject);
var
  P: DPerk;
  Influence: DInfluence;
begin
  if not (Target is DBaseActor) then
    raise Exception.Create('FATAL: MultiPerk target is not a BaseActor');
  for P in Perks do
  begin
    Influence.InfluenceType := P.PerkAction;
    Influence.Value := P.Value;
    Influence.Skill := 0;
    Influence.Delay := 0; //Parent.AnimationDelay(Self.Animation)
    DBaseActor(Target).Affect(Influence);
  end;
end;

{-----------------------------------------------------------------------------}

constructor DMultiPerk.Create;
begin
  //inherited Create; <---------------- parent is empty
  Perks := DPerkList.Create(false); //doesn't own children
end;

{-----------------------------------------------------------------------------}

destructor DMultiPerk.Destroy;
begin
  Perks.Free;
  inherited Destroy;
end;

{............................................................................}

procedure InitPerks;
begin
  LoadPerks;
  LoadMultiperks;
  LoadActions;
end;

{-----------------------------------------------------------------------------}

procedure FreePerks;
begin
  PerkDictionary.Free;
  MultiperkDictionary.Free;
  ActionDictionary.Free;
end;

end.

