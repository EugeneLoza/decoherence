{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Characters that may be controlled by the Player
   They have the most enchanced characteristics and API *)

{$INCLUDE compilerconfig.inc}

unit DecoPlayerCharacter;

interface

uses
  DecoEmbodiedActor, DecoInfluence,
  DecoGlobal;

type
  { Character that can be a part of Player's party,
    can accept input commands and has all the abilities 
    Recruitable characters are here too (at least for now) }
  DPlayerCharacter = class(DEmbodiedActor)
  strict private
    { extended EmbodiedActor.doAffect sending "signal" to GUI
      if this Character is part of CurrentParty }
    procedure PlayerAffect(const Influence: DInfluence);
  public
    { Which DCharacterSpace is responsible for controlling this character
      if nil, then this chracter is not in CurrentParty }
    MyCharSpace: DObject;
    { Loads a temporary body for this character }
    procedure LoadTempBody;
  public
    constructor Create; override;
  end;

{............................................................................}
implementation
uses
  //CastleResources,
  CastleScene, CastleVectors,
  DecoBody,
  DecoFolders,
  DecoTransformWorld,
  DecoCharacterSpace,
  DecoLog {%Profiler%};

procedure DPlayerCharacter.LoadTempBody;
begin
  FBody := DBody.Create(nil);
  FBody.BodyName := 'suzanne';
  FBody.RequestBody;
  FBody.MirrorRender := mrMirror;//mrAll
  FBody.isRenderSafe := false;
  FBody.BodyOwner := Self;
  FBody.Translation := Vector3(DRND.Random * 5, DRND.Random * 5, DRND.Random * 5);
  GetRandomDirection;
end;

{-----------------------------------------------------------------------------}

procedure DPlayerCharacter.PlayerAffect(const Influence: DInfluence);
begin
  //let the Actor.Body react normally
  doAffect(Influence); //Caution! We don't need Character display Billboard in GUI, if https://gitlab.com/EugeneLoza/decoherence/issues/740 works fine then there is no issue here and we may easily use this call-back to EmbodiedActor

  //And cast the effect info to GUI
  if MyCharSpace <> nil then
    (MyCharSpace as DCharacterSpace).onAffect(Influence);
end;

{-----------------------------------------------------------------------------}

constructor DPlayerCharacter.Create;
begin
  inherited Create;
  onAffect := @PlayerAffect;
end;

end.

