{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* A group of player characters that can be controlled *)

{$INCLUDE compilerconfig.inc}

unit DecoPlayerParty;

interface

uses
  {Generics.Lists,}
  DecoActorGroup,
  DecoPlayerCharacter,
  DecoGlobal;

{type
  DPlayerCharacterList = specialize TObjectList<DPlayerCharacter>;}

type
  { An ActorGroup containing a number of player characters to manage them together.
    With some additional functions relative to player characters management }
  DPlayerParty = class(DActorGroup)
  public
    { Wrapper for FActorList to provide for easier typecasting }
    function GetPlayerCharacter(const Index: DInt8): DPlayerCharacter;
    { Free all actors in this party (not sure if this is right) }
    procedure FreeActors;
    { Add characters in this party to the CurrentWorld (experimental) }
    procedure AddToWorld;
    { Remove characters in this party from the CurrentWorld (experimental) }
    procedure RemoveFromWorld;
  public
  end;

{............................................................................}
implementation
uses
  DecoArchitect,
  DecoLog {%Profiler%};

function DPlayerParty.GetPlayerCharacter(const Index: DInt8): DPlayerCharacter;
begin
  if Index < FActorList.Count then
    Result := FActorList[Index] as DPlayerCharacter
  else
    Result := nil; //or log error?
end;

{----------------------------------------------------------------------------}

procedure DPlayerParty.FreeActors;
var
  i: Integer;
begin
  for i := 0 to Pred(FActorList.Count) do
    FActorList[i].Free;
  //why while FActorList.Count>0 do FActorList.Delete[0] is not working???
end;

{----------------------------------------------------------------------------}

procedure DPlayerParty.AddToWorld;
var
  i: Integer;
begin
  for i := 0 to Pred(FActorList.Count) do
    Architect.CurrentWorld.AddActor(GetPlayerCharacter(i));
end;

{----------------------------------------------------------------------------}

procedure DPlayerParty.RemoveFromWorld;
var
  i: Integer;
begin
  for i := 0 to Pred(FActorList.Count) do
    Architect.CurrentWorld.RemoveActor(GetPlayerCharacter(i));
end;

end.

