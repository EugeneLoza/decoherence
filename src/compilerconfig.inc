{Copyright (C) 2012-2019 Yevhen Loza

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.}

{---------------------------------------------------------------------------}

(* Compiler directives/options to include in every unit *)

{====================== PASCAL-SPECIFIC DIRECTIVES =========================}

{$SMARTLINK ON} // Enable smart-linking

{$MODE objfpc} // FreePascal style code
{$H+} // AnsiStrings
{$J-} // non-writeable constants
{$COPERATORS ON} // allow += style operators, I like them
{$GOTO OFF} // Disallow goto and label
{$EXTENDEDSYNTAX ON} //allow discarding function result

{ Write heap trace to file or to log? }
//{$DEFINE HEAP_FILE}

{ Use simple corba interfaces }
{$INTERFACES corba}

{ Can use definition of `procedure is nested` }
{$MODESWITCH nestedprocvars}

{ I should try using cmem some day. They say in some cases it may +30% speed and -50% RAM usage. However, I didn't test it yet. }
//{$DEFINE useCMEM}

{============================ LOGGING OPTIONS ================================}

{ Write log to file }
{$IFDEF RELEASE}
{$DEFINE WriteLog}
{$ENDIF}

{ Write Shaders log, it's messy, so used only occasionally for heavy-debugging }
//{$DEFINE LogShaders}

{======================== ENGINE-SPECIFIC DIRECTIVES =========================}

{ Inline code? Just in case of bugs...
  e.g. freepascal debugger doesn't show line numbers in inlined code }
{$DEFINE SUPPORTS_INLINE}

{ Gzip or not gzip, that is the answer. }
//{$DEFINE gzipdata} //it's not used right now.

{ Fix a windows encoding bug }
//{$IFDEF Windows}{$DEFINE UTF8Encode}{$ENDIF} //it's not used right now

{================================ MOBILE-OS =================================}

{ Determines whether we're running a Desktop or Mobile OS
  used to optimize code concerning input and memory usage }
{$IFDEF ANDROID}{$DEFINE MOBILE}{$ENDIF}
{$IFDEF IOS}{$DEFINE MOBILE}{$ENDIF}
{$IFNDEF MOBILE}{$DEFINE DESKTOP}{$ENDIF}

{============================== ENABLE MACRO ================================}

{ Activate useful macros }
{$INCLUDE macro.inc}

{======================== GAME-SPECIFIC DIRECTIVES =========================}

{ Are frames burned with a "burner image" after scaling? }
{$DEFINE BurnerImage}

{ Definitions needed for Profiler to work }
{$INCLUDE profiler.inc}

{ Try to ignore irrelevant hints, notes and warnings }
{$INCLUDE ignorewarning.inc}

{ This checks mouse ray collision not only with body bounding box, but also with
  its triangles. This way we don't get the body selected when the mouse isn't over
  the exact mesh. This is slower, but more beautiful. Maybe, I'll disable it
  for efficiency some time later }
{$DEFINE ExactBodyCollisions}

{ If this defined, the Tasks will execute immediately in case they have CriticalImportance
  It has both pros and cons.
  (+) There is no 1 frame lag behind the tasks.
  (+) There is no case when interface elements aren't initialized in the first frame
  (-) It sometimes requires reinitialization of World, i.e. dropping performance
  (-) It can't be optimized by PerformAllTasks }
//{$DEFINE ImmediateCriticalTasks}

