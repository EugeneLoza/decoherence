{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ --------------------------------------------------------------------------- }

(* Initializes and frees the application *)

{$INCLUDE compilerconfig.inc}

unit DecoInit;

interface

uses
  DecoGlobal;

{ Initializes all generic stuff }
procedure InitDecoherence;
{ Frees everything initialized before }
procedure FreeDecoherence;
{............................................................................}
implementation

uses
  SysUtils, CastleWindow, CastleControls,
  CastleApplicationProperties,
  CastleGLUtils,
  DecoLoadEmbedded, DecoHDD, DecoWindowCloseConfirmation,
  DecoTranslation,
  DecoArchitect,
  DecoInput, DecoMouseRecorder, DecoPlayer, DecoGUI, DecoInterfaceLoader,
  DecoSceneLoader, DecoAnimations,
  DecoTrash, DecoTasks,
  DecoMain,
  DecoTempGame,
  DecoPerks,
  DecoTime, DecoLog, DecoWindow, DecoConfig, DecoProfiler;

{ Displays a "Loading..." image for the language
  thanks to Michalis, it's simple :) see https://github.com/eugeneloza/decoherence/issues/22 }
procedure SetLoadingImage;
begin
  case CurrentLanguage of
    Language_English: Theme.Images[tiLoading] := Loading_eng;
    Language_Russian: Theme.Images[tiLoading] := Loading_rus;
    else
      begin
        Theme.Images[tiLoading] := Loading_eng;
        Log(CurrentRoutine, 'WARNING: Unknown Language in DecoInit! Falling back to English.');
      end;
  end;

  Theme.OwnsImages[tiLoading] := false;
end;

{-----------------------------------------------------------------------------}

function GetApplicationName: String;
begin
  Result := 'Decoherence 1';
end;

{-----------------------------------------------------------------------------}

procedure ApplicationInitialize;
begin
  { Be careful with init sequence and do not change the init order
    unless you know what you are doing
    as some units require others being already initialized }
  Log(CurrentRoutine, 'Init sequence started.');

  //GLFeatures.EnableFixedFunction := false;

  InitTrash;
  InitAnimations;
  InitGlobal;
  InitTaskQueue;
  InitPerks;
  InitArchitect;
  InitSceneLoader;
  LoadInterface;
  InitPlayer;
  InitMouseRecorder;
  InitWindowCloseConfirmation; //we might want to assign Window.OnCloseQuery event in the end of init

  InitGUI;
  InitInput;
  InitMain;

  //<Temporary>
  StartTempGame;
  //</Temporary>

  LogTextureProfiler; //displays all loaded images and textures total size

  Log(CurrentRoutine, 'Init sequence finished.');
end;

{-----------------------------------------------------------------------------}

procedure InitDecoherence;
begin

  InitTime;
  InitProfiler; //init profiler as soon as possible, but after "time"

  InitLog;
  Log(CurrentRoutine, 'Initializing Application and Window.');

  InitHDDLock;
  ReadConfiguration;

  InitTranslation;
  SetLoadingImage;
  InitWindow;

  //init CastleWindow.Application properties
  Application.MainWindow := Window;
  Application.OnInitialize := @ApplicationInitialize;

  //set application name (tree copies)
  SysUtils.OnGetApplicationName := @GetApplicationName;
  ApplicationProperties(true).ApplicationName := GetApplicationName;
  Window.Caption := GetApplicationName;
end;

{-----------------------------------------------------------------------------}

procedure FreeDecoherence;
begin
  StopTempGame;

  { Be careful with free sequence and do not change the init order
    unless you know what you are doing
    as some units might accidentally (thou unlikely) send a call to already-freed instance }
  Log(CurrentRoutine, 'Game over. Freeing all data.');

  WriteConfiguration;
  FreeMain; //disable Window events as soon as possible to avoid accidental render (paranoia)
  FreeWindowCloseConfirmation; //disable Window.OnCloseQuery event (paranoia)
  FreeInput; //free input before player
  FreePlayer;
  FreeMouseRecorder;
  FreeArchitect;
  FreeGUI;
  FreeInterface;
  FreeSceneLoader;
  FreePerks;
  FreeTrash;
  FreeGlobal;
  FreeAnimations;
  FreeHDDLock;
  FreeTaskQueue;
  Log(CurrentRoutine, 'Finished.');

  FreeProfiler; //profiler should be freed after last StopProfiler and before freeing the log and timer
  FreeTime;
  FreeLog;
end;

initialization
  InitDecoherence;

finalization
  FreeDecoherence;

end.

