{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Core file of the game. Processes Window events and initializes the game *)

{$INCLUDE compilerconfig.inc}

unit DecoMain;

interface

uses
  CastleWindow;

{ Assign Window.onUpdate event }
procedure InitMain;
{ Release Window.onUpdate event }
procedure FreeMain;
{............................................................................}
implementation

uses
  DecoInit, DecoPlayer, DecoMouseRecorder, DecoGUI, DecoArchitect,
  DecoTime, DecoTasks, DecoWindow, DecoConfig {%Profiler%};

{$PUSH}{$WARN 5024 off : Parameter "$1" not used}  // It's ok to have unused variables here as these are CGE routines and we're not using Container inside at the moment

procedure doUpdate(Container: TUIContainer);
begin
  {StartProfiler}

  //Don't render anything if a Stop-Screen / LoadScreen is requested
  if not RenderOnlyGUI then
  begin
    {if (FrameStart < 0) then
      doTime; // this is the first initialization of time
      FrameStart += SecondEquivalent; //first frame is fine to wait a mintue for management
    }

    Architect.Manage;
    //Actors.Manage;
    //Music.Manage;
    Player.Manage;
    if UseMouseRecorder then
      MouseRecorder.Manage;

    ManageTasks;
  end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure doRender(Container: TUIContainer);
begin
  {StartProfiler}

  doTime; //will init FrameStart = ForceThreadedTime

  GUI.Draw;

  {StopProfiler}
end;

{$POP}

{............................................................................}

procedure InitMain;
begin
  Window.OnUpdate := @doUpdate;
  Window.OnRender := @doRender;
end;

{-----------------------------------------------------------------------------}

procedure FreeMain;
begin
  Window.OnUpdate := nil;
  Window.OnRender := nil;
end;

end.

