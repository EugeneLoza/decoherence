{ Copyright (C) 2018-2020 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Task Queue is a critically important manager for all possible in-game
   tasks trying to balance FPS.
   Only real-time (main thread) tasks are supported at the moment, maybe forever.
   Tasks must be managed by their own Parents *)

{$INCLUDE compilerconfig.inc}

unit DecoTasks;

interface

uses
  DecoGlobal, DecoTime;

{controls whether CriticalTask are executed immediately on in the next frame}
//{$DEFINE ImmediateCriticalTasks}

type
  { Resolution of importance. Can be relatively low. }
  DImportance = DInt32;

const
  { Maximal importance of the task (max value that can be assigned to DImportance) }
  MaxImportance = High(DImportance);

const
  { The max importance for the task
    "div 10" is used to reduce harm of "overflows" during summing something up }
  CriticalTask = MaxImportance div 10;
  { This is an important task }
  NormalTask = MaxImportance div 20;
  { This is an unimportant task that may be skipped for now }
  SmallTask = MaxImportance div 40;

type
  { A procedure that is executed when the task is executed
    Warning: tasks should not spawn other tasks. While it actually should work without any bugs
    (except a bit added inefficiency due to reordering the TaskQueue on Delete(i<Pred(TaskQueue.Count))
    and the fact that the new task will not be preformed this frame even if the TaskQueue is empty).
    But still it is an issue that needs thorough testing. }
  TTaskProcedure = procedure of object;
  { Procedure that accepts progress state }
  TProgressProcedure = procedure(const aProgress: DFloat) of object;

type
  { Some procedure wrapper that will be preformed no sooner than the NEXT frame.
    The Tasks are sorted based on Importance
    DTask is usually a non-FPS critical task, which may experience frameskips }
  DTask = class(DObject)
  strict protected
    { Careful, fisWorking = true guarantees that the task is in the TaskQueue
      and therefore should be handled only automatically }
    fisWorking: Boolean;
  public
    { Does this task needs deactivating the World to perform?
      Normally only tasks that require rendering some of World objects require this
      It is optimized, Deactivation/reactivation happens only once per Frame
      Unless the task requires immediate performance,
      then it will call both Deactivate/Activate }
    DeactivateWorld: Boolean;
    { The actual task to be preformed }
    Task: TTaskProcedure;
    { Importance of the task which determines its position among other tasks
      "CriticalTask" means task is critical and must be preformed despite FPS
      (this task will bypass the TaskQueue if ImmediateCriticalTasks is defined)
      Warning: if Importance is small the task may actually never be preformed
      if there are other, more important tasks pending. So, importance should be
      updated by Parent based on last performance time of the task and it's accumulated importance }
    Importance: DImportance;
    { Starts the task (adds it to TaskQueue list and initializes.
      It is safe to call this routine repeatedly even if the task is already in the TaskQueue list.
      Overloaded procedure also sets or updates Importance of the task. }
    procedure StartTask; virtual;
    procedure StartTask(const aImportance: DImportance); virtual;
    { Stop this task and remove it from TaskQueue.
      This procedure is safe and may be called even if the task is not in the TaskQueue. }
    procedure CancelTask; virtual;
    { Stop this task after it has been performed. }
    procedure StopTask; virtual;
    { Is this task in the TaskQueue at the moment? }
    property isWorking: Boolean read fisWorking default false;
  public
    destructor Destroy; override;
  end;

type
  { This task is expected to require more time than 1 frame duration
    Therefore such tasks are accumulated and as they together reach "CriticalTask" importance 
    all the accumulated tasks are fired }
  DDelayedTask = class(DTask)
  strict protected
    { Last value of Importance to subtract from DelayedTasksTotalImportance
      in case the task is canceled }
    LastImportance: DImportance;
  public
    procedure StartTask; override;
    procedure StartTask(const aImportance: DImportance); override;
    procedure CancelTask; override;
    procedure StopTask; override;
  end;

{ Initialize the TaskQueue }
procedure InitTaskQueue;
{ Free the TaskQueue }
procedure FreeTaskQueue;
{ Stops and removes all tasks in TaskQueue and DelayedTaskQueue
  is expected to run before some major change, e.g. changing of World location }
procedure ClearAllTasks;
{ This will perform all tasks in TaskQueue and DelayedTaskQueue
  In normal situation it will also use some progress bar to display progress
  as the process might take from several seconds (updating the overworld)
  to several minutes (loading/generating a new location) }
procedure PerformAllTasks(const aProgressProcedure: TProgressProcedure);
{ This should be called in Window.Update every frame
  Manages TaskQueue and performs its tasks, balancing the FPS }
procedure ManageTasks;
{............................................................................}
implementation
uses
  SysUtils,
  Generics.Collections, Generics.Defaults,
  DecoGUI, DecoArchitect,
  DecoConfig, DecoLog {%Profiler%};

type
  { List of tasks to be managed }
  DTaskList = specialize TObjectList<DTask>;

type
  { Comparer of the Tasks importance for sorting }
  TTasksComparer = specialize TComparer<DTask>;

var
  { Normal tasks that should be performed during the game. }
  TaskQueue: DTaskList;
  { Delayed tasks that should be performed during game pauses. }
  DelayedTaskQueue: DTaskList;
  { This is total importance of the tasks in DelayedTaskQueue
    When this value is > CriticalTask then the whole DelayedTaskQueue is executed
    int64 not to care about possible overflows
    if several critical tasks are assigned simultaneously}
  DelayedTasksTotalImportance: int64;
  { Cached TTasksComparer.Construct(@CompareTasks) }
  TasksComparer: specialize IComparer<DTask>;

function CompareTasks(constref t1, t2: DTask): Integer;
begin
  Result := t2.Importance - t1.Importance;
end;

{-----------------------------------------------------------------------------}

procedure ManageTasks;
var
  TaskCount: SizeInt;
begin
  {StartProfiler}

  if DelayedTasksTotalImportance >= CriticalTask then
    PerformAllTasks(GUI.RequestStopScreen) //if the amount/quality of DelayedTasks has reached a critical point, then stop game and execute everything immediately
  else
  if (GetNowThread - FrameStart < FPSGoal) then
  begin
    //cache TaskQueue.Count;
    TaskCount := Pred(TaskQueue.Count);

    {if TaskCount > 0 then
      TaskQueue.Sort(TasksComparer); }

    //to render all tasks set FrameStart to something ridiculously large (or rather to GetNowThread + SomeReasonableTimeEquivalent)

    while (TaskCount >= 0) and ((GetNowThread - FrameStart < FPSGoal)
      {$IFNDEF ImmediateCriticalTasks}
      or (TaskQueue[TaskCount].Importance >= CriticalTask)
      {$ENDIF}
      ) do
    begin
      if (Architect.CurrentWorld <> nil) and (TaskQueue.Items[TaskCount].DeactivateWorld) then
        Architect.CurrentWorld.DeactivateWorld;

      TaskQueue.Items[TaskCount].Task;
      TaskQueue.Items[TaskCount].StopTask; //maybe something more efficient may be here?
      TaskQueue.Delete(TaskCount);
      dec(TaskCount);
    end;


    if Architect.CurrentWorld <> nil then
      Architect.CurrentWorld.ActivateWorld;
  end;

  {StopProfiler}
end;

{============================================================================}

procedure DTask.StartTask;
begin
  {$IFDEF ImmediateCriticalTasks}
  if not fisWorking then
  begin
    if Importance < CriticalTask then
    begin
      //assign a new task into the queue
      TaskQueue.Add(Self);
      fisWorking := true;
    end else
      //if the task.Importance = CriticalTask then bypass the queue and execute it immediately
      Task;
  end else
    //if the task is already in the queue, but the updated Importance = CriticalTask
    if Importance = CriticalTask then
    begin
      //execute the task immediately
      Task;
      //and remove it from the queue
      CancelTask;
    end;
  {$ELSE}
  if not fisWorking then
  begin
    //assign a new task into the queue
    TaskQueue.Add(Self);
    fisWorking := true;
  end;
  {$ENDIF}
end;
procedure DTask.StartTask(const aImportance: DImportance);
begin
  Importance := aImportance;
  StartTask;
end;

{-----------------------------------------------------------------------------}

procedure DTask.StopTask;
begin
  fisWorking := false;
end;

{-----------------------------------------------------------------------------}

procedure DTask.CancelTask;
begin
  if fisWorking then
  begin
    TaskQueue.Extract(Self);
    StopTask;
  end;
end;

{-----------------------------------------------------------------------------}

destructor DTask.Destroy;
begin
  //prevent a freed task from accidentally remaining in TaskQueue
  CancelTask;
  inherited Destroy;
end;

{============================================================================}

procedure DDelayedTask.StartTask;
begin
  if not fisWorking then
  begin
    DelayedTaskQueue.Add(Self);
    fisWorking := true;
  end else
    //subtract "last importance" add "new importance"
    DelayedTasksTotalImportance -= LastImportance;

  LastImportance := Importance;
  DelayedTasksTotalImportance += Importance;
end;
procedure DDelayedTask.StartTask(const aImportance: DImportance);
begin
  Importance := aImportance;
  StartTask;
end;

{-----------------------------------------------------------------------------}

procedure DDelayedTask.CancelTask;
begin
  if fisWorking then
  begin
    DelayedTaskQueue.Extract(Self);
    StopTask;
  end;
end;

{-----------------------------------------------------------------------------}

procedure DDelayedTask.StopTask;
begin
  if fisWorking then
    DelayedTasksTotalImportance -= LastImportance;
  fisWorking := false;
end;

{============================================================================}

procedure PerformAllTasks(const aProgressProcedure: TProgressProcedure);
const
  { How smaller the TaskQueue tasks are considered in comparison to
    DelayedTasks tasks. Ugly, but I don't care for now :) }
  TaskQueueCoefficient = 0.1;
var
  MaxProgress: DFloat;
  CurrentProgress: DFloat;
  i: Integer;
begin
  Log(CurrentRoutine, 'Running DelayedTaskQueue.Count = '
    + IntToStr(DelayedTaskQueue.Count) + '; TaskQueue.Count = ' + IntToStr(TaskQueue.Count));
  MaxProgress := DelayedTaskQueue.Count + TaskQueue.Count * TaskQueueCoefficient;
  CurrentProgress := 0;

  if Assigned(aProgressProcedure) then
    aProgressProcedure(0);

  if Architect.CurrentWorld <> nil then
    Architect.CurrentWorld.DeactivateWorld;

  //perform the delayed tasks first, as they may spawn normal tasks
  for i := 0 to Pred(DelayedTaskQueue.Count) do
  begin
    DelayedTaskQueue.Items[i].Task;
    DelayedTaskQueue.Items[i].StopTask;
    CurrentProgress += 1;
    if Assigned(aProgressProcedure) then
      aProgressProcedure(CurrentProgress / MaxProgress);
  end;
  DelayedTaskQueue.Clear;
  DelayedTasksTotalImportance := 0;

  //and perform all the normal tasks, as we've made a pause anyway
  for i := 0 to Pred(TaskQueue.Count) do
  begin
    TaskQueue.Items[i].Task;
    TaskQueue.Items[i].StopTask;
    CurrentProgress += TaskQueueCoefficient;
    if Assigned(aProgressProcedure) then
      aProgressProcedure(CurrentProgress / MaxProgress);
  end;
  TaskQueue.Clear;

  if Architect.CurrentWorld <> nil then
    Architect.CurrentWorld.ActivateWorld;

  //sending a 1 value to ask GUI to hide StopScreen now
  if Assigned(aProgressProcedure) then
    aProgressProcedure(1);
end;


{-----------------------------------------------------------------------------}

procedure ClearAllTasks;
var
  i: Integer;
begin
  for i := 0 to Pred(TaskQueue.Count) do
    TaskQueue[i].StopTask;
  TaskQueue.Clear;
  for i := 0 to Pred(DelayedTaskQueue.Count) do
    DelayedTaskQueue[i].StopTask;
  DelayedTaskQueue.Clear;
  DelayedTasksTotalImportance := 0;
end;

{............................................................................}

procedure InitTaskQueue;
begin
  Log(CurrentRoutine, 'Initializing task queue...');
  TaskQueue := DTaskList.Create(false);
  DelayedTaskQueue := DTaskList.Create(false);
  DelayedTasksTotalImportance := 0;
  TasksComparer := TTasksComparer.Construct(@CompareTasks)
end;

{-----------------------------------------------------------------------------}

procedure FreeTaskQueue;
begin
  Log(CurrentRoutine, 'Freeing task queue...');
  //ClearAllTasks; <------- we don't seem to need this one
  TaskQueue.Free;
  DelayedTaskQueue.Free;
end;

end.

