{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ --------------------------------------------------------------------------- }

(* This is a temporary game loader unit
   to test different aspects of the game in real situations *)

{$INCLUDE compilerconfig.inc}

unit DecoTempGame;

interface

{uses
  Classes, SysUtils;}

{ Start the test game }
procedure StartTempGame;
{ Stop the test game }
procedure StopTempGame;
{............................................................................}
implementation
uses
  SysUtils, CastleVectors, CastleCameras,
  DecoWindow, DecoGUI, DecoTasks,
  DecoEmbodiedActor, DecoBody, DecoTransformWorld, DecoBaseActor,
  DecoFolders, DecoNavigationScene,
  DecoArchitect, DecoAbstractWorld, DecoPlayer,
  DecoTime, DecoLog {%Profiler%};

type
  TEnemyHelper = class helper for DEmbodiedActor
    procedure GetTempBody;
  end;

procedure TEnemyHelper.GetTempBody;
begin
  FBody := DBody.Create(nil);
  FBody.BodyName := 'spherecrawler';
  FBody.RequestBody;
  FBody.MirrorRender := mrAll;
  FBody.isRenderSafe := true;
  FBody.BodyOwner := Self;
  FBody.Translation := Vector3(1, 0, 0);
  GetRandomDirection;
end;

var
  Enemy: DEmbodiedActor;

procedure StartTempGame;
var
  S: DNavigationScene;
begin
  GUI.LoadScreen;

  Log(CurrentRoutine, 'Starting a new game...');
  {$PUSH}{$WARNINGS OFF}{$HINTS OFF} //it's ok, we're doing a very wrong operation here deliberately and temporarily
  Architect.CurrentWorld := DAbstractWorld.Create;
  {$POP}
  Player.MakeTestParty(8);
  Player.AddToWorld;

  Enemy := DEmbodiedActor.Create;
  with Enemy do
  begin
    GetTempBody;
    TempCharacter;
  end;
  Architect.CurrentWorld.AddActor(Enemy);

  Window.SceneManager.Camera := TExamineCamera.Create(Window.SceneManager);
  Window.SceneManager.Camera.SetView(Vector3(-4, 0, 1), Vector3(1, 0, 0), Vector3(0, 0, 1), true);
  S := DNavigationScene.Create(Window.SceneManager);
  S.Location := Vector3(-3, 0, 3);
  S.Radius := 10;
  Window.SceneManager.Items.Add(S);
  Window.SceneManager.MainScene := S;

  Architect.CurrentWorld.ActivateWorld;

  PerformAllTasks(GUI.LoadScreenProgress);
  //Sleep(1000);

  ResetUnsavedGameTime;

  GUI.TestInterface;
end;

{-----------------------------------------------------------------------------}

procedure StopTempGame;
begin
  Architect.CurrentWorld.DeactivateWorld;
  Player.CurrentParty.RemoveFromWorld;
  Enemy.Free;
  FreeAndNil(Architect.CurrentWorld);
  Log(CurrentRoutine, 'Game over...');
end;


end.

