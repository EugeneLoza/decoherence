{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* A timer to be used in Interface and World for timeout events *)

{$INCLUDE compilerconfig.inc}

unit DecoTimer;

interface

uses
  DecoTime, DecoGlobal;

type
  { A simple time-out mechanisms to preform some timed events on interface
    elements }
  DAbstractTimer = class abstract(DObject)
  private
    { Set automatically, date of the timer count start }
    StartTime: DTime;
  public
    { If the timer is running }
    Enabled: Boolean;
    { How long (in seconds) will it take the timer to fire }
    Interval: DTime;
    { Action to preform }
    onTimer: TSimpleProcedure;
    { A simple way to set and run timer }
    procedure SetTimeOut(const Seconds: DTime);
    { Check if the timer finished and run onTimer if true }
    procedure Update; virtual; abstract;
  public
    constructor Create; //override;
  end;

type
  { Timer for interface elements, uses Player time (OS time) to count }
  DInterfaceTimer = class(DAbstractTimer)
  public
     procedure Update; override;
  end;

type
  { Timer for World objects, uses World time (local time) to count }
  DWorldTimer = class(DAbstractTimer)
  public
     procedure Update; override;
  end;

type
  { Timer for animated objects,
    Unlike DWorldTimer is unaffected by SoftPause }
  DAnimationTimer = class(DAbstractTimer)
  public
     procedure Update; override;
  end;

{............................................................................}
implementation
uses
  DecoLog {%Profiler%};

constructor DAbstractTimer.Create;
begin
  //inherited Create; <----------- Parent is empty
  Enabled := false;
  StartTime := -1;
end;

{-----------------------------------------------------------------------------}

procedure DAbstractTimer.SetTimeout(const Seconds: DTime);
begin
  if Seconds > 0 then
  begin
    StartTime := -1;
    Enabled := true;
    Interval := Seconds;
  end else
  if Assigned(onTimer) then //if delay is zero, then just do it!
    onTimer;
end;


{============================================================================}

procedure DInterfaceTimer.Update;
begin
  {StartProfiler}

  if StartTime < 0 then
    StartTime := DecoNow
  else
    if (DecoNow - StartTime) >= Interval then
    begin
      Enabled := false;
      if Assigned(onTimer) then
        onTimer;
    end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DWorldTimer.Update;
begin
  {StartProfiler}

  if StartTime < 0 then
    StartTime := DecoNowWorld
  else
    if (DecoNowWorld - StartTime) >= Interval then
    begin
      Enabled := false;
      if Assigned(onTimer) then
        onTimer;
    end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DAnimationTimer.Update;
begin
  {StartProfiler}

  if StartTime < 0 then
    StartTime := DecoNowAnimation
  else
    if (DecoNowAnimation - StartTime) >= Interval then
    begin
      Enabled := false;
      if Assigned(onTimer) then
        onTimer;
    end;

  {StopProfiler}
end;

end.

