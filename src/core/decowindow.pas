{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ --------------------------------------------------------------------------- }

(* Handles window size and scale
   !!! NO RESCALES ALLOWED DURING THE GAME !!!
   Changing FullScreen mode or Resolution only by rebooting! *)

{$INCLUDE compilerconfig.inc}

unit DecoWindow;

interface

uses
  CastleWindow,
  DecoGlobal;

var
  { Main window of the game }
  Window: TCastleWindow;
  { If the screenshot is pending at the moment?
    Some routines may use it to render a bit different picture
    in screenshots, like hiding mouse cursor. }
  ScreenShotPending: Boolean;
  { false: everything is rendered
    true: only GUI is rendered }
  RenderOnlyGUI: Boolean = false;

{ Force Window update to render GUI
  i.e. due to loadscreen or stopscreen }
procedure ForceRenderGUI;
{ Creates and initializes the Window }
procedure InitWindow;
{ Make a screenshot of current screen and save it to a file }
procedure MakeScreenShot;
{ Clears screen with black color }
procedure ClearScreen;
{............................................................................}
implementation
uses
  SysUtils,
  CastleApplicationProperties,
  CastleVectors, CastleGLUtils,
  DecoTasks, DecoGui,
  DecoGUIScale, DecoColors,
  DecoConfig, DecoLog, DecoTime {%Profiler%};

const
  { Color for clearing the screen }
  ScreenClearColor: DColor = (Data: ( 0.0 , 0.0 , 0.0 , 1.0)); // = clBlack

procedure ForceRenderWindow;
begin
  Window.Invalidate;
  Application.ProcessAllMessages;
end;

{-----------------------------------------------------------------------------}

procedure ClearScreen;
begin
  RenderContext.Clear([cbColor], ScreenClearColor);
end;

{-----------------------------------------------------------------------------}

procedure ForceRenderGUI;
var
  LastSceneManagerState: Boolean;
  LastWorldTimeSpeed: DFloat;
begin
  LastWorldTimeSpeed := WorldTimeFlowSpeed;
  WorldTimeFlowSpeed := 0;
  LastSceneManagerState := Window.SceneManager.Exists;
  Window.SceneManager.Exists := false;
  RenderOnlyGUI := true;

  ForceRenderWindow;

  RenderOnlyGUI := false;
  Window.SceneManager.Exists := LastSceneManagerState;
  WorldTimeFlowSpeed := LastWorldTimeSpeed;
end;

{-----------------------------------------------------------------------------}

procedure MakeScreenShot;
begin
  ScreenShotPending := true;
  //careful: onBeforeRender and onRender are called here.
  if PreformAllTasksBeforeScreenshot then
    PerformAllTasks(GUI.RequestStopScreen);
  Window.SaveScreen('Deco_' + NiceDate + '.png');
  ScreenShotPending := false;
end;

{............................................................................}

procedure InitWindow;
begin
  {StartProfiler}

  Window := TCastleWindow.Create(Application);
  Window.DoubleBuffer := true;
  Window.ResizeAllowed := raOnlyAtOpen;

  Window.FullScreen := ConfigFullScreen;
  if ConfigFullScreen then
  begin
    Log(CurrentRoutine, 'FullScreen mode ON');
  end else
  begin
    Log(CurrentRoutine, 'FullScreen mode OFF, Window resolution = ' +
      ConfigWindowWidth.ToString + 'x' + ConfigWindowHeight.ToString);
    Window.Width := ConfigWindowWidth;
    Window.Height := ConfigWindowHeight;
  end;

  ApplicationProperties.LimitFPS := 0;

  ResetGUIScale;

  ScreenShotPending := false;

  {StopProfiler}
end;

end.

