{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ --------------------------------------------------------------------------- }

(* Buttons representing actions for player characters *)

{$INCLUDE compilerconfig.inc}

unit DecoActionButton;

interface

uses
  //Generics.Collections, DecoLinearArrangers, <----- later
  DecoBaseActor, DecoPerks,
  DecoButtons,
  DecoGlobal;

type
  { Button that represents an action }
  DActionButton = class(DAbstractImageFramedImageButton)
  strict protected
    { Reference to the actor }
    FTarget: DBaseActor;
    { Reference to the action }
    FAction: DAction;
    { Set target Actor for this button }
    procedure SetTarget(const AValue: DBaseActor);
    { Update this button content }
    procedure UpdateButton;
  public
    { Set/read target of this button }
    property Target: DBaseActor read FTarget write SetTarget;
  public
    constructor Create; override;
  end;

{............................................................................}
implementation
uses
  DecoImages, DecoInterfaceImages,
  DecoLog {%Profiler%};

procedure DActionButton.SetTarget(const aValue: DBaseActor);
begin
  if FTarget <> aValue then
  begin
    if FTarget <> nil then
      FTarget.OnActionChanged := nil;
    FAction := nil;
    FTarget := aValue;
    if FTarget <> nil then
      FTarget.OnActionChanged := @UpdateButton;
    UpdateButton;
  end;
end;

{-----------------------------------------------------------------------------}

procedure DActionButton.UpdateButton;
begin
  if (FTarget <> nil) then
  begin
    if FAction <> FTarget.Action then
    begin
      FAction := FTarget.Action;
      if FAction.Multiperk.Image <> nil then
        FImage.Load(FAction.Multiperk.Image)
      else
        FImage.Load(GetImageByName('ActionImagePlaceholder'));
    end;
  end else
    Log(CurrentRoutine, 'ERROR: Target is nil');
end;

{-----------------------------------------------------------------------------}

constructor DActionButton.Create;
begin
  inherited Create;
  LoadFrame(GetImageByName('ActionButtonImageFrame'));
  FImage := DSimpleImage.Create;
  InsertFront(FImage);
end;

end.

