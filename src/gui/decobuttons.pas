{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ --------------------------------------------------------------------------- }

(* Basic routines for Buttons *)

{$INCLUDE compilerconfig.inc}

unit DecoButtons;

interface

uses
  Generics.Collections, DecoLinearArrangers,
  DecoFramedImages, DecoLabels,
  DecoGlobal;

type
  { A simple button with text(caption)
    already contains onMouseLeftButton event as child of DSingleInterfaceElement}
  DTextButton = class(DFramedLabel)
  strict private
    procedure doMouseEnter;
    procedure doMouseLeave;
  public
    constructor Create; override;
  end;

type
  { Core routines for rectagonal buttons with images }
  DAbstractRectagonalImageButton = class(DRectagonalFramedImage)
  strict private
    procedure doMouseEnter;
    procedure doMouseLeave;
  public
    constructor Create; override;
  end;

type
  { Image button with rectagonal frame }
  DRectagonalImageButton = class(DAbstractRectagonalImageButton)
  public
    constructor Create; override;
  end;

type
  { Core routines for buttons with image frame (non-rectagonal) }
  DAbstractImageFramedImageButton = class(DImageFramedImage)
  strict private
    procedure doMouseEnter;
    procedure doMouseLeave;
  public
    constructor Create; override;
  end;

type
  { Image button with image frame }
  DImageFramedImageButton = class(DAbstractImageFramedImageButton)
  public
    constructor Create; override;
  end;

type
  { A set of TextButtons }
  DTextButtonsList = specialize TObjectList<DTextButton>;

type
  { A simple vertical/horizontal arranger of TextButtons }
  DTextButtonArranger = class(DLinearArranger)
  public
    { Add a button }
    procedure AddButton(const aButtonName: String; const onClick: TSimpleProcedure);
  end;

{............................................................................}
implementation
uses
  DecoFrames, DecoFont, DecoImages,
  DecoLog {%Profiler%};

procedure DTextButton.doMouseEnter;
begin
  FFrame.Highlight := true;
  FLabel.Highlight := true;
end;

{-----------------------------------------------------------------------------}

procedure DTextButton.doMouseLeave;
begin
  FFrame.Highlight := false;
  FLabel.Highlight := false;
end;

{-----------------------------------------------------------------------------}

constructor DTextButton.Create;
begin
  inherited Create;
  LoadFrame(GetFrameByName('ButtonFrame'));

  CanMouseOver := true;
  OnMouseEnter := @doMouseEnter;
  OnMouseLeave := @doMouseLeave;

  FLabel.Font := GetFontByName('NormalButtonFont');
  FLabel.LabelType := atOneLine;
end;

{============================================================================}

procedure DAbstractRectagonalImageButton.doMouseEnter;
begin
  FFrame.Highlight := true;
  FImage.Highlight := true;
end;

{-----------------------------------------------------------------------------}

procedure DAbstractRectagonalImageButton.doMouseLeave;
begin
  FFrame.Highlight := false;
  FImage.Highlight := false;
end;

{-----------------------------------------------------------------------------}

constructor DAbstractRectagonalImageButton.Create;
begin
  inherited Create;
  CanMouseOver := true;
  OnMouseEnter := @doMouseEnter;
  OnMouseLeave := @doMouseLeave;
end;

constructor DRectagonalImageButton.Create;
begin
  inherited Create;
  LoadFrame(GetFrameByName('ButtonFrame'));
end;

{============================================================================}

procedure DAbstractImageFramedImageButton.doMouseEnter;
begin
  FFrame.Highlight := true;
  FImage.Highlight := true;
end;

{-----------------------------------------------------------------------------}

procedure DAbstractImageFramedImageButton.doMouseLeave;
begin
  FFrame.Highlight := false;
  FImage.Highlight := false;
end;

{-----------------------------------------------------------------------------}

constructor DAbstractImageFramedImageButton.Create;
begin
  inherited Create;
  CanMouseOver := true;
  OnMouseEnter := @doMouseEnter;
  OnMouseLeave := @doMouseLeave;
end;

constructor DImageFramedImageButton.Create;
begin
  inherited Create;
  LoadFrame(GetImageByName('ButtonImageFrame'));
end;

{============================================================================}

procedure DTextButtonArranger.AddButton(const aButtonName: String; const onClick: TSimpleProcedure);
var
  aButton: DTextButton;
begin
  aButton := DTextButton.Create;
  aButton.Text := aButtonName;
  aButton.OnMouseLeftButton := onClick;
  InsertFront(aButton);
end;

end.

