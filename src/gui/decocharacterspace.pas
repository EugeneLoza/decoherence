{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* The most important control, which enables display and manipulate
   the player character, including everything that may be related:
   statbars, clicks, rollovers, portraits, commands, statuses, etc.
   It all sums up here in this place. *)

{$INCLUDE compilerconfig.inc}

unit DecoCharacterSpace;

interface

uses
  DecoInterfaceCore, DecoInterfaceContainer,
  DecoInterfaceArrangers, DecoStatBars, DecoLabels,
  DecoActionButton,
  DecoPlayerCharacter, DecoPortrait, DecoInfluence,
  DecoTime, DecoGlobal;

type
  { In which mode is the current Character Space }
  TCharacterSpaceState = (
    { Only health bars and statuses are displayed }
    csMinimal,
    { Healthbars, statuses and portraits are displayed (onMouseOver) }
    csPortrait,
    { All control elements are displayed }
    csControl);

type
  { Character space, that contains all his/her interface elements
    including portrait, statbars, current actions/statuses, controls, etc. }
  DCharacterSpace = class(DAbstractArranger)
  strict private
    FStatBars: DPlayerBars;
    FHealthLabel: DHealthLabel;
    FTarget: DPlayerCharacter;
    FPortrait: DPortrait;
    FAlignment: TInterfaceElementAlignment;
    procedure SetAlignment(const aAlign: TInterfaceElementAlignment);
    procedure SetTarget(const aTarget: DPlayerCharacter);
    { determine X of the interface element considering its left/right placement
      LeftX is "as if this element is located to the left" }
    function GetX(const LeftX, Width: DInt16): DInt16;
    { Returns a container which is correctly right/left aligned
      It is assumed that aContainer is left-aligned }
    function AlignContainer(const aContainer: DInterfaceContainer): DInterfaceContainer;
  strict protected
    procedure ArrangeChildren(const Animate: TAnimationStyle; const Duration: DTime); override;
  strict protected
    FSelected: Boolean;
    { Set this value selected/deselected }
    procedure SetSelected(const aValue: Boolean);
  public
    property isSelected: Boolean read FSelected write SetSelected;
    { Is this character space right or left?}
    property Alignment: TInterfaceElementAlignment read FAlignment write SetAlignment;
    { Character being monitored and controlled }
    property Target: DPlayerCharacter read FTarget write SetTarget;
    { Called when current Target has been affected }
    procedure onAffect(const aInfluence: DInfluence);
  strict private type
    TContainerProcedure = procedure (var Cont: DInterfaceContainer) of object;
  strict private
    (* container macro *)
    procedure StatBarsContainer(var Cont: DInterfaceContainer);
    procedure HealthLabelContainer(var Cont: DInterfaceContainer);
    procedure PortraitContainer(var Cont: DInterfaceContainer);
    procedure HideContainer(var Cont: DInterfaceContainer);
    procedure ShowContainer(var Cont: DInterfaceContainer);
    procedure RearrangeMe;
    { Arrange a child element container with a corresponding macro and other parameters }
    procedure ArrangeChild(const aChild: DAbstractElement; const aContainerProcedure: TContainerProcedure; const Animate: TAnimationStyle; const aDuration: DTime; const aShow: Boolean = true);
    (* state change events *)
  strict private
    { is the CharacterSpace currently hiding? }
    isHiding: Boolean;
    procedure doMouseLeaveTimeout;
    procedure doMouseEnter;
  strict private
    fCurrentState: TCharacterSpaceState;
    procedure SetCurrentState(const aState: TCharacterSpaceState);
  public
    { In which state is the current Character Space }
    property CurrentState: TCharacterSpaceState read fCurrentState write SetCurrentState;
    procedure Update; override;
  public
    constructor Create; override;
  end;

var
  { Setting this to "true" will force portraits always displayed }
  ShowAllPortraits: Boolean = true;

{............................................................................}
implementation
uses
  SysUtils,
  DecoTimer,
  DecoPartyView,
  DecoLog, DecoGUIScale {%Profiler%};

const
  { How long will it take since the CharacterSpace looses focus
    to collapse to csMinimal state }
  PortraitTimeOut = 1.0; {seconds}

constructor DCharacterSpace.Create;
begin
  inherited Create;
  FPortrait := DPortrait.Create;
  FPortrait.CanMouseOver := true;
  //FPortrait.Hint := 'Right-click to enter the character menu';
  InsertFront(FPortrait);
  SetTarget(nil);

  FStatBars := DPlayerBars.Create;
  FStatBars.CanMouseOver := true;
  InsertFront(FStatBars);
  FHealthLabel := DHealthLabel.Create;
  FHealthLabel.CanMouseOver := true;
  InsertFront(FHealthLabel);

  Timer := DInterfaceTimer.Create;
  Timer.onTimer := @doMouseLeaveTimeout;

  CurrentState := csMinimal;

  isHiding := true;
end;

{-----------------------------------------------------------------------------}

procedure DCharacterSpace.SetTarget(const aTarget: DPlayerCharacter);
begin
  if FTarget <> aTarget then
  begin
    FTarget := aTarget;
    FStatBars.Target := FTarget;
    FHealthLabel.Target := FTarget;
    FPortrait.Target := FTarget;
    //ArrangeChildren?
  end;
end;
procedure DCharacterSpace.SetAlignment(const aAlign: TInterfaceElementAlignment);
begin
  if FAlignment <> aAlign then
  begin
    FAlignment := aAlign;
    FPortrait.Alignment := FAlignment;
  end;
end;


{-----------------------------------------------------------------------------}

function DCharacterSpace.GetX(const LeftX, Width: DInt16): DInt16;
begin
  if FAlignment = iaLeft then
    Result := LeftX
  else
    Result := GUIWidth - (LeftX + Width);
end;

{-----------------------------------------------------------------------------}

function DCharacterSpace.AlignContainer(const aContainer: DInterfaceContainer): DInterfaceContainer;
begin
  Result.AssignFrom(aContainer);
  Result.x := GetX(Result.x, Result.w);
end;

{-----------------------------------------------------------------------------}

procedure DCharacterSpace.StatBarsContainer(var Cont: DInterfaceContainer);
begin
  Cont.x += GUILargeGap;
  Cont.y += GUIUnit4;
  Cont.h -= GUIUnit2;
  Cont.w := GUIUnit23;
end;
procedure DCharacterSpace.HealthLabelContainer(var Cont: DInterfaceContainer);
begin
  Cont.x += GUILargeGap;
  Cont.h := GUIUnit4;
  Cont.w := GUIUnit23;
end;
procedure DCharacterSpace.PortraitContainer(var Cont: DInterfaceContainer);
begin
  Cont.x += GUIUnit23 + 2 * GUILargeGap;
  Cont.h -= GUIUnit4;
  Cont.w := GUIUnit * 2;
end;
procedure DCharacterSpace.HideContainer(var Cont: DInterfaceContainer);
begin
  Cont.x := 0;
  Cont.a := 0;
end;
procedure DCharacterSpace.ShowContainer(var Cont: DInterfaceContainer);
begin
  //Cont.x := 0; <-------- we just leave x as-is
  Cont.a := 1 * Cont.a;
end;

procedure DCharacterSpace.ArrangeChild(const aChild: DAbstractElement;
  const aContainerProcedure: TContainerProcedure; const Animate: TAnimationStyle;
  const aDuration: DTime; const aShow: Boolean = true);
var
  CNext: DInterfaceContainer;
begin
  {StartProfiler}

  CNext.AssignFrom(Self.BaseState);
  aContainerProcedure(CNext);

  if aShow then
    ShowContainer(CNext)
  else
    HideContainer(CNext);

  //aChild.FromToAnimate(AlignContainer(CLast), AlignContainer(CNext), asDefault, aDuration);
  aChild.SetBaseSize(AlignContainer(CNext), asDefault, aDuration);

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DCharacterSpace.ArrangeChildren(const Animate: TAnimationStyle; const Duration: DTime);
var
  ShowPortrait, ShowControls: Boolean;
begin
  {StartProfiler}

  //inherited ArrangeChildren(Animate, Duration); <------- Parent is abstract
  //Log(CurrentRoutine, 'Arranging ChraracterSpace...');

  ShowPortrait := ShowAllPortraits or (CurrentState <> csMinimal); //portrait is always shown unless in csMinimal state
  ShowControls := CurrentState = csControl; //controls are only shown in csControl state

  //show statbars
  ArrangeChild(FStatBars, @StatBarsContainer, Animate, Duration, true);
  ArrangeChild(FHealthLabel, @HealthLabelContainer, Animate, Duration, true);

  //show current statuses
  //...

  //show current action
  //...

  //show portrait
  ArrangeChild(FPortrait, @PortraitContainer, Animate, Duration, ShowPortrait);

  //show controls
  //...

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DCharacterSpace.RearrangeMe;
begin
  ArrangeChildren(asDefault, DefaultAnimationDuration);
end;

{-----------------------------------------------------------------------------}

procedure DCharacterSpace.SetCurrentState(const aState: TCharacterSpaceState);
begin
  //Log(CurrentRoutine, 'Set state ' + IntToStr(Ord(aState)) + ' former = ' + IntToStr(Ord(fCurrentState)));
  if fCurrentState <> aState then
  begin
    fCurrentState := aState;
    RearrangeMe;
  end;
end;

{-----------------------------------------------------------------------------}

procedure DCharacterSpace.doMouseLeaveTimeout;
begin
  if isHiding then
    SetCurrentState(csMinimal);
end;

{-----------------------------------------------------------------------------}

procedure DCharacterSpace.doMouseEnter;
begin
  if isHiding then
  begin
    isHiding := false;
    SetCurrentState(csPortrait);
  end;
end;

{-----------------------------------------------------------------------------}

procedure DCharacterSpace.Update;
begin
  inherited Update;

  if Self.isMouseOverTree then
    doMouseEnter
  else
  begin
    if (not isHiding) and (not FSelected) then
    begin
      isHiding := true;
      Timer.SetTimeOut(PortraitTimeOut);
    end;
  end;
end;

{-----------------------------------------------------------------------------}

procedure DCharacterSpace.SetSelected(const aValue: Boolean);
begin
  if aValue <> FSelected then
  begin
    FSelected := aValue;

    if FSelected = true then
    begin
      if Parent is DPartyView then
        DPartyView(Parent).Select(Self)
      else
        Log(CurrentRoutine, 'ERROR: Parent is not DPartyView!');

      SetCurrentState(csControl);
    end;

    if FSelected = false then
    begin
      SetCurrentState(csMinimal);
      {if Self.isMouseOverTree then
        doMouseEnter
      else
      begin
        isHiding := true;
        Timer.SetTimeOut(PortraitTimeOut);
      end;}
    end;

    //FCharacterControl.Selected := aValue;
  end;
end;

{-----------------------------------------------------------------------------}

procedure DCharacterSpace.onAffect(const aInfluence: DInfluence);
begin
  //roll-in the portrait
  //and show portrait overlay with affect info
end;

end.

