{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Definitions of colors throughout the game -
   at this moment it's a wrapper for CastleColors
   but with less indefenite colors names *)

{$INCLUDE compilerconfig.inc}

unit DecoColors;

interface

uses
  CastleVectors, CastleColors;

type
  DColor = TCastleColor;

const
  { Transparent color to clear transparent images }
  clClear  : DColor = (Data: ( 0.0, 0.0, 0.0, 0.0));
  { a copy of CastleColors colors definition, but with cl prefix
    maybe, they'll be changed some time in future }
  clWhite  : DColor = (Data: ( 1.0, 1.0, 1.0, 1.0));
  clMaroon : DColor = (Data: ( 0.5, 0.0, 0.0, 1.0));
  clRed    : DColor = (Data: ( 1.0, 0.0, 0.0, 1.0));
  clOrange : DColor = (Data: ( 1.0, 0.65, 0.0, 1.0));
  clYellow : DColor = (Data: ( 1.0, 1.0, 0.0, 1.0));
  clOlive  : DColor = (Data: ( 0.5, 0.5, 0.0, 1.0));
  clPurple : DColor = (Data: ( 0.5, 0.0, 0.5, 1.0));
  clFuchsia: DColor = (Data: ( 1.0, 0.0, 1.0, 1.0));
  clLime   : DColor = (Data: ( 0.0, 1.0, 0.0, 1.0));
  clGreen  : DColor = (Data: ( 0.0, 0.5, 0.0, 1.0));
  clNavy   : DColor = (Data: ( 0.0, 0.0, 0.5, 1.0));
  clBlue   : DColor = (Data: ( 0.0, 0.0, 1.0, 1.0));
  clAqua   : DColor = (Data: ( 0.0, 1.0, 1.0, 1.0));
  clTeal   : DColor = (Data: ( 0.0, 0.5, 0.5, 1.0));
  clBlack  : DColor = (Data: ( 0.0, 0.0, 0.0, 1.0));
  clSilver : DColor = (Data: ( 0.75, 0.75, 0.75, 1.0));
  clGray   : DColor = (Data: ( 0.5, 0.5, 0.5, 1.0));
  { RBG Colors (for convenience) copy of the above with alpha discarded }
  rgbWhite  : TVector3 = (Data: ( 1.0, 1.0, 1.0));
  rgbMaroon : TVector3 = (Data: ( 0.5, 0.0, 0.0));
  rgbRed    : TVector3 = (Data: ( 1.0, 0.0, 0.0));
  rgbOrange : TVector3 = (Data: ( 1.0, 0.65, 0.0));
  rgbYellow : TVector3 = (Data: ( 1.0, 1.0, 0.0));
  rgbOlive  : TVector3 = (Data: ( 0.5, 0.5, 0.0));
  rgbPurple : TVector3 = (Data: ( 0.5, 0.0, 0.5));
  rgbFuchsia: TVector3 = (Data: ( 1.0, 0.0, 1.0));
  rgbLime   : TVector3 = (Data: ( 0.0, 1.0, 0.0));
  rgbGreen  : TVector3 = (Data: ( 0.0, 0.5, 0.0));
  rgbNavy   : TVector3 = (Data: ( 0.0, 0.0, 0.5));
  rgbBlue   : TVector3 = (Data: ( 0.0, 0.0, 1.0));
  rgbAqua   : TVector3 = (Data: ( 0.0, 1.0, 1.0));
  rgbTeal   : TVector3 = (Data: ( 0.0, 0.5, 0.5));
  rgbBlack  : TVector3 = (Data: ( 0.0, 0.0, 0.0));
  rgbSilver : TVector3 = (Data: ( 0.75, 0.75, 0.75));
  rgbGray   : TVector3 = (Data: ( 0.5, 0.5, 0.5));

{ Find a supplementary color to a given one
  (i.e. a high-contrast color, with inverse HUE) }
function SupplementaryColor(const aColor: DColor): DColor;
{............................................................................}
implementation
uses
  DecoLog {%Profiler%};

function RGBtoRGBA(const aRGBColor: TVector3; const Alpha: Single): DColor;
begin
  Result.Data[0] := aRGBColor.Data[0];
  Result.Data[1] := aRGBColor.Data[1];
  Result.Data[2] := aRGBColor.Data[2];
  Result.Data[3] := Alpha;
end;

{-----------------------------------------------------------------------------}

function RGBAtoRGB(const aRGBAColor: DColor): TVector3;
begin
  Result.Data[0] := aRGBAColor.Data[0];
  Result.Data[1] := aRGBAColor.Data[1];
  Result.Data[2] := aRGBAColor.Data[2];
end;

{-----------------------------------------------------------------------------}

function SupplementaryColor(const aColor: DColor): DColor;
var
  HSVColor: TVector3;
begin
  HSVColor := RGBtoHSV(RGBAtoRGB(aColor));
  HSVColor.Data[0] += 0.5 * 6; //cycle Hue by 180 deg. (i.e. 0.5*6)
  if HSVColor.Data[0] > 6 then
    HSVColor.Data[0] -= 6;
  Result := RGBtoRGBA(HSVtoRGB(HSVColor), aColor.Data[3]);
end;

end.

