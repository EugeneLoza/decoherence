{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Image withing a frame *)

{$INCLUDE compilerconfig.inc}

unit DecoFramedImages;

interface

uses
  DecoImages,
  DecoInterfaceImages, DecoFramedElement;

type
  { Image within a rectagonal frame }
  DRectagonalFramedImage = class(DRectagonalFramedElement)
  strict protected
    FImage: DSimpleImage;
  public
    { Wrapper for Image.Load }
    procedure Load(const aImage: DImage);
  public
    constructor Create; override;
  end;

type
  { Image within an image frame }
  DImageFramedImage = class(DImageFramedElement)
  strict protected
    FImage: DSimpleImage;
  public
    { Wrapper for Image.Load }
    procedure Load(const aImage: DImage);
  public
    constructor Create; override;
  end;

{............................................................................}
implementation
uses
  DecoLog {%Profiler%};

procedure DRectagonalFramedImage.Load(const aImage: DImage);
begin
  FImage.Load(aImage);
end;

{-----------------------------------------------------------------------------}

constructor DRectagonalFramedImage.Create;
begin
  inherited Create;
  FImage := DSimpleImage.Create;
  InsertFront(FImage);
end;

{============================================================================}

procedure DImageFramedImage.Load(const aImage: DImage);
begin
  FImage.Load(aImage);
end;

{-----------------------------------------------------------------------------}

constructor DImageFramedImage.Create;
begin
  inherited Create;
  FImage := DSimpleImage.Create;
  InsertFront(FImage);
end;

end.

