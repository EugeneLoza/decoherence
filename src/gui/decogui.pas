{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* A highets-level container for the interface and its routines *)

{$INCLUDE compilerconfig.inc}

unit DecoGUI;

interface

uses
  {<temporary included for debugging>}
  DecoPerks,
  DecoActionButton,
  {</temporary>}
  DecoGUICore;

type
  { GUI container, manages all other GUI elements }
  DGUI = class(DGUICore)
  public
    { A pop-up window, showing a message }
    procedure ShowMessage(const aMessage: String);
    { Show MainMenu (during gameplay) }
    procedure ShowMainMenu;
  public
    { A test interface to play with }
    procedure TestInterface;
    { Display LoadScreen
      should be called before the loading/generation starts }
    procedure LoadScreen;
  end;

var
  { All the game interface }
  GUI: DGUI;

{ Initialize GUI instance }
procedure InitGUI;
{ Free GUI instance }
procedure FreeGUI;
{............................................................................}
implementation
uses
  DecoInterfaceCore,
  DecoGameMode, DecoGUIScale, DecoLogLabel,
  DecoMessageBox, DecoPartyView, DecoWind,
  DecoLoadScreen, DecoMainMenu,
  DecoLog {%Profiler%};

procedure DGUI.ShowMessage(const aMessage: String);
var
  aMessageBox: DSimpleMessageBox;
begin
  Log(CurrentRoutine, aMessage);
  aMessageBox := DSimpleMessageBox.Create;
  aMessageBox.Text := aMessage;
  aMessageBox.SetBaseSize(GUICenterX - GUIUnit * 3, GUICenterY - GUIUnit * 1, GUIUnit * 6, GUIUnit * 2, 1, asFadeIn);

  InsertFrontModal(aMessageBox);
end;

{-----------------------------------------------------------------------------}

procedure DGUI.ShowMainMenu;
var
  aMainMenu: DMainMenu;
begin
  if isModal then
  begin
    if CurrentModalElement is DMainMenu then
      DMainMenu(CurrentModalElement).CloseMenu;
    Exit;  //don't show main menu in modal mode
  end;

  Log(CurrentRoutine, 'Entering main menu...');
  aMainMenu := DMainMenu.Create;
  aMainMenu.SetBaseSize(GUICenterX - GUIUnit * 2, GUICenterY - GUIUnit * 4, GUIUnit * 4, GUIUnit * 8, 1, asFadeIn);

  InsertFrontModal(aMainMenu);
end;

{===================== SPECIFIC INTERFACE KINDS ============================}

procedure DGUI.LoadScreen;
begin
  ResetGUI;

  FLoadScreen := DLoadScreen.Create;
  InsertFront(FLoadScreen);

  //ForceRenderGUI; <------- SIGSEGV here as SceneManager isn't active yet
end;

{-----------------------------------------------------------------------------}

procedure DGUI.TestInterface;
var
  PartyView: DPartyView;
  MainMenuButton: DMainMenuButton;
  { Displays in-game log messages, such as player action result }
  FLogLabel: DLogLabel;
  FActionButton: DActionButton;
begin
  ResetGUI;

  //InsertFront(DWind.Create);

  FLogLabel := DLogLabel.Create;
  FLogLabel.SetBaseSize(Round(GUICenter[0]) - 3 * GUIUnit, GUILargeGap + GUIUnit8, 6 * GUIUnit, 4 * GUIUnit, 1);
  FLogLabel.doRender;
  InsertFront(FLogLabel);

  PartyView := DPartyView.Create;
  PartyView.Reset;
  InsertFront(PartyView);

  MainMenuButton := DMainMenuButton.Create;
  MainMenuButton.SetBaseSize(GUILargeGap, GUILargeGap + GUIUnit8, GUIUnit23, GUIUnit23);
  InsertFront(MainMenuButton);


  FActionButton := DActionButton.Create;
  FActionButton.SetBaseSize(GUIUnit * 5, GUIUnit * 2, GUIUnit, GUIUnit, 1, asNone);
  FActionButton.Target := nil;
  InsertFront(FActionButton);
end;

{............................................................................}

procedure InitGUI;
begin
  GUI := DGUI.Create;
end;

{-----------------------------------------------------------------------------}

procedure FreeGUI;
begin
  GUI.Free;
end;

end.

