{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Core features of the GUI
   handles basic features such as stop/load screens, modal elements,
   mouse, logging, etc. *)

{$INCLUDE compilerconfig.inc}

unit DecoGUICore;

interface

uses
  DecoInterfaceCore, DecoMouseCursor,
  DecoStopScreen, DecoLoadScreen,
  DecoLabels, DecoLogMessages,
  DecoTasks, DecoTime, DecoGlobal;

type
  { A set of modal elements }
  DModalElementsList = DInterfaceElementsList;

type
  { Core features of GUI }
  DGUICore = class abstract (DInterfaceElement)
  strict protected
    { A Stop-Screen to display when the game needs to make some calculation-heavy task }
    FStopScreen: DStopScreen;
    { A load screen during game loading/generation }
    FLoadScreen: DLoadScreen;
    { Call-back to render StopScreen }
    procedure RenderStopScreen(const aProgress: DFloat);
    { Call-back to render LoadScreen }
    procedure RenderLoadScreen(const aProgress: DFloat);
  public
    { Is current GUI state modal? }
    isModal: Boolean;
    { Message container to store and use GUI log messages }
    LogMessages: DMessageContainer;
    { Reference to StopScreen progress call-back }
    function RequestStopScreen: TProgressProcedure;
    { Reference to LoadScreen progress call-back }
    function LoadScreenProgress: TProgressProcedure;
  strict private
    { A label that counts and displays real FPS }
    FPSLabel: DFPSLabel;
    { Is this render the first one?
      Then needs some init tasks to be done. }
    FFirstRender: Boolean;
    { Some post-initialization routines, that require graphics context fully available }
    procedure FirstRender;
  strict protected
    { Completely clear all GUI children and set all references to nil }
    procedure ResetGUI;
  public
    { Mouse cursor and corresponding operations
      Also used to render dragged image }
    Cursor: DCursor;
    { Updates cursor position and image }
    procedure UpdateCursor(const CursorX, CursorY: Single;
      const DragElement: DSingleInterfaceElement = nil);
    { Does current GUI mode allows World mouse events? }
    function AllowWorldMouseRay: Boolean;
  strict protected
    { Stored value of DecoTime.SoftPause
      uninitialized value is -99 (and checked against -10)
      to comply with "zero soft pause" = -1}
    StoredSoftPause: DTime;
    { Lists of modal elements and their corresponding shades/winds }
    ModalElements, ModalWinds, ModalShades: DModalElementsList;
    { Topmost modal element }
    function CurrentModalElement: DSingleInterfaceElement;
    { Start GUI modal mode }
    procedure StartModal;
    { Stop GUI modal mode
      Returns True if something has been stopped and False otherwise }
    function StopModal: Boolean;
    procedure doStopModal;
    { Insert an element in front and make it modal }
    procedure InsertFrontModal(const aChild: DSingleInterfaceElement);
  public
    { (Re)sets the tint of the whole interface }
    procedure SetTint; override;
    { Draw the GUI and all its child elements }
    procedure Draw; override;
    function ifMouseOver(const xx, yy: DInt16): DAbstractElement; override;
  strict protected
    { Ask GUI to reset cursor (if GUI element has been created/destroyed) }
    procedure HardResetCursor;
  public
    constructor Create; override;
    destructor Destroy; override;
  end;

{............................................................................}
implementation
uses
  DecoMouse, DecoGUIScale, DecoGameMode, DecoColors,
  DecoShadeImage, DecoWind,
  DecoConfig, DecoWindow, DecoLog {%Profiler%};

procedure DGUICore.RenderStopScreen(const aProgress: DFloat);
begin
  if ScreenShotPending then
    Exit; //if this event fired during prepraration to a screenshot, then we don't need to display stop-screen

  if FStopScreen = nil then
  begin
    FStopScreen := DStopScreen.Create;
    InsertFront(FStopScreen);
  end;
  FStopScreen.Position := aProgress;

  ForceRenderGUI;
end;

{-----------------------------------------------------------------------------}

procedure DGUICore.RenderLoadScreen(const aProgress: DFloat);
begin
  FLoadScreen.UpdateProgress(aProgress);
  ForceRenderGUI;
end;

{-----------------------------------------------------------------------------}

function DGUICore.RequestStopScreen: TProgressProcedure;
begin
  Result := @Self.RenderStopScreen;
end;

{-----------------------------------------------------------------------------}

function DGUICore.LoadScreenProgress: TProgressProcedure;
begin
  Result := @Self.RenderLoadScreen;
end;

{-----------------------------------------------------------------------------}

procedure DGUICore.FirstRender;
begin
  {StartProfiler}

  FFirstRender := false;
  Cursor.HideOSCursor;
  SetTint;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DGUICore.ResetGUI;
begin
  ResetTime;
  Clear;
  FStopScreen := nil;
  FLoadScreen := nil;
end;

{-----------------------------------------------------------------------------}

procedure DGUICore.UpdateCursor(const CursorX, CursorY: Single;
  const DragElement: DSingleInterfaceElement = nil);
begin
  {StartProfiler}

  Cursor.DragElement := DragElement;

  if Cursor.DragElement = nil then
  begin
    if MouseLook then
    begin
      { it's important to avoid mouse shimmer during MouseLook }
      Cursor.x := GUICenter[0];
      Cursor.y := GUICenter[1];
    end else
    begin
      Cursor.x := CursorX;
      Cursor.y := CursorY;
    end;
  end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

function DGUICore.AllowWorldMouseRay: Boolean;
begin
  Result := not isModal;
end;

{-----------------------------------------------------------------------------}

function DGUICore.CurrentModalElement: DSingleInterfaceElement;
begin
  if ModalElements.Count > 0 then
    Result := ModalElements[Pred(ModalElements.Count)]
  else
    Log(CurrentRoutine, 'ERROR! ModalElements.Count is Zero!');
end;

{-----------------------------------------------------------------------------}

procedure DGUICore.StartModal;
var
  Wind, Shade: DSingleInterfaceElement;
begin
  isModal := true;
  if StoredSoftPause < -10 then
    StoredSoftPause := SoftPause;
  SoftPause := InfiniteSoftPause;

  //add shade and wind
  Shade := DShadeImage.Create;
  ModalShades.Add(Shade);
  InsertFront(Shade);
  Wind := DWind.Create;
  ModalWinds.Add(Wind);
  InsertFront(Wind);
end;

{-----------------------------------------------------------------------------}

function DGUICore.StopModal: Boolean;
begin
  Result := false;

  if ModalElements.Count > 0 then
  begin
    Result := true;
    //delete the "topmost" modal element
    ModalShades[Pred(ModalShades.Count)].AnimateMe(asFadeOut); // suicide animation
    ModalShades.ExtractIndex(Pred(ModalShades.Count));
    ModalWinds[Pred(ModalWinds.Count)].AnimateMe(asFadeOut); // suicide animation
    ModalWinds.ExtractIndex(Pred(ModalWinds.Count));

    ModalElements[Pred(ModalElements.Count)].AnimateMe(asFadeOut); // suicide animation
    ModalElements.ExtractIndex(Pred(ModalElements.Count));
    // if the deleted element was the last one, stop modal mode
    if ModalElements.Count = 0 then
    begin
      isModal := false;
      SoftPause := StoredSoftPause;
      StoredSoftPause := -99;
      Result := false;
    end;
  end;
end;
procedure DGUICore.doStopModal;
begin
  StopModal; //just discard the function result
end;

{-----------------------------------------------------------------------------}

procedure DGUICore.InsertFrontModal(const aChild: DSingleInterfaceElement);
begin
  {StartProfiler}

  StartModal;
  ModalElements.Add(aChild);
  Children.Add(aChild);
  aChild.Parent := Self;
  aChild.OnClose := @doStopModal; // child must take care of suicide itself on button press

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DGUICore.SetTint;
begin
  {StartProfiler}

  inherited SetTint;
  Cursor.SetTint;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DGUICore.Draw;
begin
  {StartProfiler}

  if FFirstRender then
    FirstRender;

  { clear the screen depending on the game mode
    in case SceneManager doesn't clear it }
  if GameModeNeedsClearingScreen then
    ClearScreen;

  { draw children elements }
  inherited Draw;

  { draw special elements }
  if (ShowFPS) and (not RenderOnlyGUI) and (not ScreenShotPending) then
    FPSLabel.CountFPS;

  { Draw mouse cursor }
  if Self.FLoadScreen = nil then
  begin
    if HardResetCursorRequired then
      HardResetCursor;
    Cursor.Draw;
  end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DGUICore.HardResetCursor;
begin
  {StartProfiler}

  ifMouseOver(Round(Cursor.x), Round(Cursor.y));
  HardResetCursorRequired := false;
  //Log(CurrentRoutine, 'GUI changed, hard resetting cursor...');

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

function DGUICore.ifMouseOver(const xx, yy: DInt16): DAbstractElement;
begin
  {StartProfiler}

  if isModal then
    //if there is a modal dialog pending, only it accepts mouse events
    Result := CurrentModalElement.ifMouseOver(xx, yy)
  else
    //otherwise just use "Regular" ifMouseOver
    Result := inherited ifMouseOver(xx, yy);

  //also let Cursor know what element is below to display hint if needed
  if Result is DSingleInterfaceElement then
    Cursor.OverElement := DSingleInterfaceElement(Result)
  else
    Cursor.OverElement := nil;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

constructor DGUICore.Create;
begin
  inherited Create;
  FullScreen;

  isModal := false;
  ModalElements := DModalElementsList.Create(false);
  ModalWinds := DModalElementsList.Create(false);
  ModalShades :=  DModalElementsList.Create(false);
  StoredSoftPause := -99;

  FFirstRender := true;
  Cursor := DCursor.Create;

  FPSLabel := DFPSLabel.Create;
  //FPSLabel.Parent := Self;

  HardResetCursorRequired := false;

  LogMessages := DMessageContainer.Create;
  LogMessages.AddMessage('Interface initialized.', clRed);
end;

{-----------------------------------------------------------------------------}

destructor DGUICore.Destroy;
begin
  Cursor.Free;
  FPSLabel.Free;
  LogMessages.Free;

  while StopModal do ; //clear all pending modal elements (to avoid SIGSEGV?!)

  ModalElements.Free;
  ModalWinds.Free;
  ModalShades.Free;

  inherited Destroy;
end;

end.

