{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ --------------------------------------------------------------------------- }

(* Determines sacle and size of GUI elements based on current window size
   !!! NO RESCALES ALLOWED DURING THE GAME !!!
   Changing FullScreen mode or Resolution only by rebooting! *)

{$INCLUDE compilerconfig.inc}

unit DecoGUIScale;

interface

uses
  CastleVectors,
  DecoGlobal;

var
  { Width and height of GUI }
  GUIWidth, GUIHeight: DInt16;
  { =1/GUIHeight }
  GUIScale: DFloat;
  { What is considered a 1 pixel gap between some dense GUI elements
    It can become larger if resolution (window size) is large
    ~1 pixel }
  GUISmallGap: DInt16;
  { A bit larger than GUISmallGap
    ~3 pixel }
  GUILargeGap: DInt16;
  { Reference size of a clickable action button }
  GUIUnit: DInt16;
  { Fractions of GUIUnit (for convenience) }
  GUIUnit2, GUIUnit3, GUIUnit4, GUIUnit8, GUIUnit23: DInt16;
  { Center of the GUI/Window }
  GUICenter: TVector2;
  GUICenterX, GUICenterY: DInt16;

  {------------ specialized ------------ }

  { Height of Character Space }
  GUICharSpaceHeight: DInt16;

{ Resets all scale variables to match Window.Width, Window.Height }
procedure ResetGUIScale;
{............................................................................}
implementation
uses
  DecoWindow, DecoLog {%Profiler%};

var GuiScaleInitialized: Boolean = false;

procedure ResetGUIScale;
  function RoundOne(a: DFloat): DInt16;
  begin
    Result := Round(a);
    if Result < 1 then Result := 1;
  end;
begin
  {StartProfiler}

  if GuiScaleInitialized then
    Log(CurrentRoutine, 'WARNING: Reinitializing the GUI Scale!');
  GuiScaleInitialized := true;

  GUIWidth := Window.Width;
  GUIHeight := Window.Height;
  //GUIScaleW := 1/GUIWidth;
  GUIScale := 1/GUIHeight;

  GUISmallGap := RoundOne(1/768 * GUIHeight);
  GUILargeGap := RoundOne(3/768 * GUIHeight);

  GUIUnit := Round(GUIHeight / 13);

  GUIUnit2 := GUIUnit div 2;
  GUIUnit3 := GUIUnit div 3;
  GUIUnit4 := GUIUnit div 4;
  GUIUnit8 := GUIUnit div 8;
  GUIUnit23 := GUIUnit * 2 div 3;

  GUICenter[0] := GUIWidth / 2;
  GUICenter[1] := GUIHeight / 2;
  GUICenterX := GUIWidth div 2;
  GUICenterY := GUIHeight div 2;

  GUICharSpaceHeight := 3 * GUIUnit;

  {StopProfiler}
end;


end.

