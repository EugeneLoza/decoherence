{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Contains most of GUI basics and grouping *)

{$INCLUDE compilerconfig.inc}

unit DecoInterfaceCore;

interface

uses
  Generics.Collections,
  DecoTimer, DecoInterfaceContainer,
  DecoGlobal, DecoTime;

const
  { Default duration of all animations (in seconds),
    unless otherwise specified }
  DefaultAnimationDuration = 0.3;

type
  { Style of the animation.
    asLinear is just linear interpolation,
    asSquare is faster in the beginning and slower in the end }
  TAnimationCurve = (acLinear, acSquare);

type
  { Animation style of the object. asDefault means "animate from previous state",
    presuming that there was some "previous state" }
  TAnimationStyle = (asNone, asDefault,
    asFadeIn, asFadeOut,
    asZoomIn, asZoomOut
    {,
    asFlyInRadial, asFlyOutRadial,
    asFlyInRandom, asFlyOutRandom});

type
  { Is this character's interface located on the right or left part of the screen? }
  TInterfaceElementAlignment = (iaRight, iaLeft);

type
  { Most abstract container for interface elements
    Defines size, scaling and animation state }
  DAbstractElement = class abstract(DObject)
  strict private
    AnimationStart: DTime;
    AnimationDuration: DTime;
    AnimationCurve: TAnimationCurve;
    CurrentAnimation: TAnimationStyle;
    function GetAlpha: DFloat;
    function GetVisible: Boolean;
  public
  //strict protected
    { Base state is "normal" state of the element, issued by SetBaseSize }
    BaseState: DInterfaceContainer;
  strict protected
    { Parent interface element }
    Parent: DAbstractElement;
    { Container of this inhterface element determined based on
      Last and Next state of the animation}
    Current: DInterfaceContainer;
    { Helper is any other state of the element (initial or final during some animation) }
    HelperState: DInterfaceContainer;
    { Last and Next state of the container during the animation
      in case the animation is over, Next container is used }
    Last, Next: ^DInterfaceContainer;
    { Stops any animation, and sets Last = Next }
    procedure ResetAnimation;
  strict protected
    { If this element is full-screen? }
    isFullScreen: Boolean;
    { Enables "suicide" of this element in case it has vanished
      managed by Parent }
    KillMePlease: Boolean;
    { updates the class each frame,
      (e.g. gets the current animation state) }
    procedure Update; virtual;
    { Updates/caches Current container }
    procedure GetAnimationState; TryInline
    { GetAnimationState with additional safety checks }
    function GetAnimationStateSafe: Boolean;
    { If CurrentAnimation is a suicide animation }
    function AnimationSuicide: Boolean;
    { If CurrentAnimatino is suitable for full-screen elements? }
    function AnimationFullScreen: Boolean;
    { This procedure alerts Parent that this element has changed its size }
    procedure SizeChanged(const Animate: TAnimationStyle; const Duration: DTime); virtual;
  public
    function isContainerInitialized: Boolean;
    { Transparency of current element, stacking all parents' transparation }
    property Alpha: Single read GetAlpha;
    { Is this Element visible on screen? }
    property isVisible: Boolean read GetVisible;
    { Draw the element / as abstract as it might be :) }
    procedure Draw; virtual; abstract;
    { Set tint of the element to match GUI color }
    procedure SetTint; virtual;
    { Initialize this element with specific coordinates/size/alpha
      optionally animation may be specified }
    procedure SetBaseSize(const ax, ay, aw, ah: DInt16; const aAlpha: DFloat = 1.0;
      const Animate: TAnimationStyle = asDefault; const Duration: DTime = DefaultAnimationDuration);
    procedure SetBaseSize(const aContainer: DInterfaceContainer;
      const Animate: TAnimationStyle = asDefault; const Duration: DTime = DefaultAnimationDuration);
    { Init the animation of this element }
    procedure AnimateMe(const Animate: TAnimationStyle = asDefault;
      const Duration: DTime = DefaultAnimationDuration);
    { Scale this element to full screen (no animation) }
    procedure FullScreen(const aAlpha: Single = 1);
  public
    constructor Create; virtual; //override;
    destructor Destroy; override;
  end;

type
  { A simple procedure which reports Sender and clicked x,y }
  TMouseEvent = procedure of object;

type
  { Fully-featured Interface Element with Mouse/Touch support
    It lacks only "Children" or specific "Draw" to be used }
  DSingleInterfaceElement = class abstract(DAbstractElement)
  strict protected
    { A simple timer to fire some event on time-out
      Warning: Timer is not created by default! You have to create it manually in Constructor}
    Timer: DInterfaceTimer;
    procedure Update; override;
    (* Mouse routines *)
  public
    { Some text hint to display for this Element }
    Hint: String;
    { If this element is active (clickable) }
    CanMouseOver: Boolean;
    { Can this element be dragged? }
    CanDrag: Boolean;
    { Are these coordinates in this element's box? }
    function IAmHere(const xx, yy: DInt16): Boolean;
    { Returns self if IAmHere and runs all possible events }
    function ifMouseOver(const xx, yy: DInt16): DAbstractElement; virtual;
  strict private
    DragX, DragY: DInt16;
    { To support for Drag-n-drop return item to its origin in case it
      cannot be dropped here }
    SavedContainerState: DInterfaceContainer;
  public
    { Dragg-n-drop routines }
    procedure Drag(const xx, yy: DInt16);
    procedure StartDrag(const xx, yy: DInt16);
    procedure Drop;
  public
    { If mouse is over this element }
    isMouseOver: Boolean;
    { Mouse/touch Events }
    OnMouseEnter: TMouseEvent;
    OnMouseLeave: TMouseEvent;
    OnMouseLeftButton: TMouseEvent;
    OnMouseRightButton: TMouseEvent;
    { Event fired on element close, in Destructor }
    OnClose: TSimpleProcedure;
  public
    constructor Create; override;
    destructor Destroy; override;
  end;

  { List of DSingleInterfaceElement instances }
  DInterfaceElementsList = specialize TObjectList<DSingleInterfaceElement>;

type
  { An interface element, that can contain "Children" }
  DInterfaceElement = class(DSingleInterfaceElement)
  strict protected
    { List of the children of this interface element }
    Children: DInterfaceElementsList;
    procedure Update; override;
  public
    procedure SetTint; override;
    procedure Draw; override;
    { Assign given element as a child and inserts it as the last child }
    procedure InsertFront(const aChild: DSingleInterfaceElement);
    { Assign given element as a child and inserts it as the first child }
    procedure InsertBack(const aChild: DSingleInterfaceElement);
    { Assign given element as a child and inserts it at given position
      Careful: use this only when you exactly know the position to insert }
    procedure Insert(const aPosition: Integer; const aChild: DSingleInterfaceElement);
    procedure Clear(const Animate: TAnimationStyle = asNone;
      const Duration: DTime = DefaultAnimationDuration);

    (* Mouse routines *)
  public
    { Returns last (cached) call to MouseOverTree result, may be invalid! }
    isMouseOverTree: Boolean;
    { Returns Self if IAmHere and runs all possible events + scans all children }
    function ifMouseOver(const xx, yy: DInt16): DAbstractElement; override;
  public
    constructor Create; override;
    destructor Destroy; override;
  end;

var
  { Does the cursor needs a hard reset? }
  HardResetCursorRequired: Boolean;

{............................................................................}
implementation
uses
  SysUtils,
  DecoGUIScale, DecoLog, DecoMath {%Profiler%};

{============================================================================}
{======================== D ABSTRACT ELEMENT ================================}
{============================================================================}

constructor DAbstractElement.Create;
begin
  //inherited <------- nothing to inherit
  KillMePlease := false;
  BaseState.Create;
  HelperState.Create;
  Current.Create;
  CurrentAnimation := asNone;
  isFullScreen := false;

  HardResetCursorRequired := true;
end;

{-----------------------------------------------------------------------------}

destructor DAbstractElement.Destroy;
begin
  HardResetCursorRequired := true;

  inherited Destroy;
end;

{-----------------------------------------------------------------------------}

function DAbstractElement.AnimationSuicide: Boolean;
begin
  if (CurrentAnimation in [asFadeOut, asZoomOut{, asFlyOutRadial, asFlyOutRandom}]) then
    Result := true
  else
    Result := false;
end;

{-----------------------------------------------------------------------------}

function DAbstractElement.AnimationFullScreen: Boolean;
begin
  if (CurrentAnimation in [asNone, asDefault, asFadeIn, asFadeOut]) then
    Result := true
  else
    Result := false;
end;

{-----------------------------------------------------------------------------}

function DAbstractElement.GetAnimationStateSafe: Boolean;
begin
  Result := not ((Last = nil) or (Next = nil));
  if Result then
  begin
    GetAnimationState;
    Result := (Last^.isInitialized) and (Current.isInitialized)
  end;
end;

{-----------------------------------------------------------------------------}

procedure DAbstractElement.GetAnimationState; TryInline
var
  Phase: DFloat;
begin
  {StartProfiler}

  //if this is start of the animation - init time
  if AnimationStart < 0 then
    AnimationStart := DecoNow;

  if not Next^.isInitialized then
    Log(CurrentRoutine, 'Warning: NEXT is not initialized!');

  if (DecoNow - AnimationStart < AnimationDuration) and (Last^.isInitialized) then
  begin
    //determine the animation time passed relative to AnimationDuration
    Phase := (DecoNow - AnimationStart) / AnimationDuration;
    //determine the animation phase
    case AnimationCurve of
      //acLinear: ; //<- change nothing.
      acSquare: Phase := Sqrt(Phase);
    end;
    Current.AssignMix(Last^, Next^, Phase);
  end else
  begin
    Current.AssignFrom(Next^);
    {if this was a suicide animation then kill this element}
    if AnimationSuicide then
      Self.KillMePlease := true;
  end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

{$PUSH}{$WARN 5024 off}  // It's ok to have unused variables here as because it should be overriden in children (just not yet sure if I should make it completely abastract)
procedure DAbstractElement.SizeChanged(const Animate: TAnimationStyle; const Duration: DTime);
begin
  //...
  {if Parent <> nil then
    Parent.SizeChanged(Animate, Duration);}
end;
{$POP}

{-----------------------------------------------------------------------------}

procedure DAbstractElement.SetTint;
begin
  //just does nothing
end;

{-----------------------------------------------------------------------------}

function DAbstractElement.GetAlpha: DFloat;
begin
  if Parent <> nil then
    Result := Minimum(Current.a, Parent.Alpha)
  else
    Result := Current.a;
end;

{-----------------------------------------------------------------------------}

function DAbstractElement.isContainerInitialized: Boolean;
begin
  Result := Last^.isInitialized and Next^.isInitialized;
end;

{-----------------------------------------------------------------------------}

function DAbstractElement.GetVisible: Boolean;
const
  { Which transparent elements are considered invisible }
  AlphaThreshold = 0.3;
begin
  if Current.a < AlphaThreshold then
    Result := false
  else
    if Self.Alpha < AlphaThreshold then
      Result := false
    else
      Result := true;
end;

{-----------------------------------------------------------------------------}

procedure DAbstractElement.AnimateMe(const Animate: TAnimationStyle;
  const Duration: DTime = DefaultAnimationDuration);
{var
  mx, my: DInt16;
  dx, dy, ddx, ddy: DFloat;}
begin
  {StartProfiler}

  AnimationStart := -1;
  AnimationDuration := Duration;
  CurrentAnimation := Animate;

  case Animate of
    {Default animation: from current state to BaseState}
    asDefault:
      begin
        AnimationCurve := acSquare;
        if Current.isInitialized then
          //!!!GetAnimationState must be called before calling AnimateMe and before assigning BaseState
          HelperState.AssignFrom(Current)
        else
        begin
          HelperState.AssignFrom(BaseState);
          Log(CurrentRoutine, 'WARNING: Calling asDefault animation with Current.isInitialized = false. Assuming asNone.');
          AnimationDuration := -1;
        end;
        Next := @BaseState;
        Last := @HelperState;
      end;
    asNone:
      begin
        AnimationCurve := acLinear;
        HelperState.AssignFrom(BaseState);
        Next := @BaseState;
        Last := @HelperState;
        AnimationDuration := -1; //no animation will just assign Current = Next (=BaseState) on next frame
      end;
    {fades in/out element}
    asFadeIn:
      begin
        AnimationCurve := acLinear;
        HelperState.AssignFrom(BaseState);
        HelperState.a := 0;
        Last := @HelperState;
        Next := @BaseState;
      end;
    asFadeOut:
      begin
        AnimationCurve := acLinear;
        HelperState.AssignFrom(BaseState);
        HelperState.a := 0;
        Last := @BaseState;
        Next := @HelperState;
      end;
    {zooms in/out element}
    asZoomIn:
      begin
        AnimationCurve := acSquare;
        HelperState.AssignFrom(BaseState);
        HelperState.SetIntWidthHeight(1, 1, 0);
        Next := @BaseState;
        Last := @HelperState;
      end;
    asZoomOut:
      begin
        AnimationCurve := acSquare;
        HelperState.AssignFrom(BaseState);
        HelperState.SetIntWidthHeight(1, 1, 0);
        Last := @BaseState;
        Next := @HelperState;
      end;
    {fly in/out}
    {asFlyInRadial,asFlyOutRadial,asFlyInRandom,asFlyOutRandom:
      begin
        if Animate in [asFlyInRadial, asFlyOutRadial] then
        begin
          dx := Current.x - GUICenter[0];
          dy := Current.y - GUICenter[1];
          ddy := dy / GUIHeight;
          ddx := dx / GUIWidth;
          if abs(ddy) > abs(ddx) then
          begin
            if dy < 0 then
              my := 0
            else
              my := GUIHeight;
            mx := Round(GUIWidth * (1 + ddx / abs(ddy)) / 2);
          end else
          begin
            if dx < 0 then
              mx := 0
            else
              mx := GUIWidth;
            my := Round(GUIHeight * (1 + ddy / abs(ddx)) / 2);
          end;
        end else
          case DRND.Random(4) of
            0: begin
                 mx := 0;
                 my := DRND.Random(GUIHeight);
               end;
            1: begin
                 mx := GUIWidth;
                 my := DRND.Random(GUIHeight);
               end;
            2: begin
                 mx := DRND.Random(GUIWidth);
                 my := 0;
               end;
            3: begin
                 mx := DRND.Random(GUIWidth);
                 my := GUIHeight;
               end;
          end;
        case Animate of
          asFlyInRadial, asFlyInRandom: Last.SetIntSize(mx, my, 1, 1, 0);
          asFlyOutRadial, asFlyOutRandom: Next.SetIntSize(mx, my, 1, 1, 0);
        end;
      end;   }
  end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DAbstractElement.ResetAnimation;
begin
  {StartProfiler}

  if (Last = nil) or (Next = nil) then
  begin
    Log(CurrentRoutine, 'WARNING: Last/Next is nil!');
    Last := @HelperState;
    Next := @BaseState;
  end;
  if not Next^.isInitialized then
    Log(CurrentRoutine, 'WARNING: Next state is not initialized!');

  Last^.AssignFrom(Next^);
  AnimationDuration := -1;
  GetAnimationStateSafe;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DAbstractElement.Update;
begin
  {StartProfiler}

  if not GetAnimationStateSafe then
    Log(CurrentRoutine, 'Note: Can''t get animation state; Last or Next is Nil.');

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DAbstractElement.SetBaseSize(const ax, ay, aw, ah: DInt16; const aAlpha: DFloat = 1.0;
  const Animate: TAnimationStyle = asDefault; const Duration: DTime = DefaultAnimationDuration);
var
  CurrentInitialized: Boolean;
begin
  {StartProfiler}

  CurrentInitialized := GetAnimationStateSafe;

  BaseState.SetIntSize(ax, ay, aw, ah, aAlpha);

  if not CurrentInitialized then
    Current.AssignFrom(BaseState);

  AnimateMe(Animate, Duration);
  SizeChanged(Animate, Duration);

  {StopProfiler}
end;

procedure DAbstractElement.SetBaseSize(const aContainer: DInterfaceContainer;
  const Animate: TAnimationStyle = asDefault; const Duration: DTime = DefaultAnimationDuration);
begin
  SetBaseSize(aContainer.x, aContainer.y, aContainer.w, aContainer.h, aContainer.a,
    Animate, Duration);
end;

{-----------------------------------------------------------------------------}

procedure DAbstractElement.FullScreen(const aAlpha: Single = 1);
begin
  {StartProfiler}

  BaseState.SetIntSize(0, 0, GUIWidth, GUIHeight, aAlpha);
  HelperState.AssignFrom(BaseState);
  Next := @BaseState;
  Last := @HelperState;
  ResetAnimation;
  isFullScreen := true;
  //SizeChanged <----- we don't call it, as it's very unlikely that a FullScreen element is a Child of some Arranger

  {StopProfiler}
end;

{============================================================================}
{===================== D SINGLE INTERFACE ELEMENT ===========================}
{============================================================================}

constructor DSingleInterfaceElement.Create;
begin
  inherited Create;
  isMouseOver := false;
  CanMouseOver := false;
  Hint := '';
end;

{-----------------------------------------------------------------------------}

destructor DSingleInterfaceElement.Destroy;
begin
  if Assigned(OnClose) then
    OnClose;
  FreeAndNil(Timer);
  inherited Destroy;
end;

{-----------------------------------------------------------------------------}

procedure DSingleInterfaceElement.Update;
begin
  {StartProfiler}

  inherited Update;
  if (Timer <> nil) and (Timer.Enabled) then
    Timer.Update;

  {StopProfiler}
end;

{==============================  Mouse handling =============================}

function DSingleInterfaceElement.IAmHere(const xx, yy: DInt16): Boolean; TryInline
begin
  {StartProfiler}

  if (xx >= Current.x) and (xx <= Current.x2) and
    (yy >= Current.y) and (yy <= Current.y2) and
    (Self.isVisible) then
    Result := true
  else
    Result := false;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

function DSingleInterfaceElement.ifMouseOver(const xx, yy: DInt16): DAbstractElement;
begin
  {StartProfiler}

  Result := nil;
  if IAmHere(xx, yy) then
  begin
    if isMouseOver = false then
    begin
      if Assigned(onMouseEnter) then
        onMouseEnter;
      isMouseOver := true;
    end;

    if (CanMouseOver) or (CanDrag) then
      Result := Self;
  end else
  begin
    if isMouseOver then
    begin
      isMouseOver := false;
      if Assigned(onMouseLeave) then
        onMouseLeave;
    end;
  end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DSingleInterfaceElement.StartDrag(const xx, yy: DInt16);
begin
  {StartProfiler}

  ResetAnimation;
  SavedContainerState.AssignFrom(BaseState);
  DragX := BaseState.x - xx;
  DragY := BaseState.y - yy;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DSingleInterfaceElement.Drag(const xx, yy: DInt16);
begin
  {StartProfiler}

  BaseState.x := DragX + xx;
  BaseState.y := DragY + yy;
  //this is ugly!
  ResetAnimation;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DSingleInterfaceElement.Drop;
begin
  {StartProfiler}

  //if CanDropHere
  AnimateMe(asDefault);
  BaseState.AssignFrom(SavedContainerState);
  AnimateMe(asDefault);

  {StopProfiler}
end;

{============================================================================}
{======================= D INTERFACE ELEMENT ================================}
{============================================================================}

constructor DInterfaceElement.Create;
begin
  inherited Create;
  Children := DInterfaceElementsList.Create(true);
end;

{----------------------------------------------------------------------------}

destructor DInterfaceElement.Destroy;
begin
  //this should fire as recoursive because children owns elements, which in turn will fire their destructors onfree
  Children.Free;
  inherited Destroy;
end;

{----------------------------------------------------------------------------}

procedure DInterfaceElement.Clear(const Animate: TAnimationStyle = asNone;
      const Duration: DTime = DefaultAnimationDuration);
var
  c: DSingleInterfaceElement;
begin
  {StartProfiler}

  if Animate = asNone then
    Children.Clear
  else
    for c in Children do
    begin
      c.GetAnimationState;
      c.AnimateMe(Animate, Duration);
      if c is DInterfaceElement then
        DInterfaceElement(c).Clear(Animate, Duration);
    end;

  {StopProfiler}
end;

{----------------------------------------------------------------------------}

procedure DInterfaceElement.Update;
var
  i: Integer;
begin
  {StartProfiler}

  { does not call update on children as they will call their own update on draw }
  if Children.Count > 0 then
  begin
    i := 0;
    repeat
      if Children[i].KillMePlease then
        Children.Delete(i) //will also free child
      else
        inc(i);
    until i >= Children.Count;
  end;

  inherited Update;

  {StopProfiler}
end;

{----------------------------------------------------------------------------}

procedure DInterfaceElement.SetTint;
var
  c: DSingleInterfaceElement;
begin
  {StartProfiler}

  //inherited SetTint; <---------- parent is an "empty" virtual procedure
  for c in Children do
    c.SetTint;

  {StopProfiler}
end;

{----------------------------------------------------------------------------}

procedure DInterfaceElement.Draw;
var
  c: DSingleInterfaceElement;
begin
  {StartProfiler}

  //inherited Draw; <---------- parent is abstract
  Update;
  for c in Children do
    c.Draw;

  {StopProfiler}
end;

{----------------------------------------------------------------------------}

procedure DInterfaceElement.InsertFront(const aChild: DSingleInterfaceElement);
begin
  {StartProfiler}

  Children.Add(aChild);
  aChild.Parent := Self;

  {StopProfiler}
end;

{----------------------------------------------------------------------------}

procedure DInterfaceElement.Insert(const aPosition: Integer; const aChild: DSingleInterfaceElement);
begin
  {StartProfiler}

  Children.Insert(aPosition, aChild);
  aChild.Parent := Self;

  {StopProfiler}
end;

{----------------------------------------------------------------------------}

procedure DInterfaceElement.InsertBack(const aChild: DSingleInterfaceElement);
begin
  {StartProfiler}

  Children.Insert(0, aChild);
  aChild.Parent := Self;

  {StopProfiler}
end;

{----------------------------------------------------------------------------}

function DInterfaceElement.ifMouseOver(const xx, yy: DInt16): DAbstractElement;
var
  i: Integer;
  tmpLink: DAbstractElement;
begin
  {StartProfiler}

  Result := inherited ifMouseOver(xx, yy);
  //if rsult<>nil ... *or drag-n-drop should get the lowest child?

  // recoursively scan all children
  for i := 0 to Pred(Children.Count) do
  begin
    tmpLink := Children[i].ifMouseOver(xx, yy);
    if tmpLink <> nil then
    begin
      Result := tmpLink;
    end;
  end;

  isMouseOverTree := Result <> nil;

  {StopProfiler}
end;

end.

