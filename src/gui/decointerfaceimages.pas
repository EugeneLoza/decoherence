{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Basic types of interface images *)

{$INCLUDE compilerconfig.inc}

unit DecoInterfaceImages;

interface

uses
  CastleVectors,
  DecoInterfaceCore, DecoImages,
  DecoColors, DecoGlobal;

type
  { How the item is higlighted? }
  TImageHighlight = Boolean;//(ihNone, ihBright, ihDull);

type
  { General routines shared by images, frames and labels }
  DAbstractImage = class abstract(DSingleInterfaceElement)
  strict private
    const
      HighlightColor: DColor = (data:(0.2, 0.2, 0.2, 0.2));
  strict private
    isColorManual: Boolean;
    function GetWidth: DInt16;
    function GetHeight: DInt16;
    procedure SetColor(const aValue: DColor);
  strict protected
    { Color of this image }
    fColor: DColor;
    { Is the image higlighted? }
    fHighlight: TImageHighlight;
    { GL Image displayed by this interface element, may be animated }
    Image: DImage;
    { Is this image "owned" by parent? False for interface images
      and true for Labels (generated temporary images) }
    OwnsImage: Boolean;
  public
    { Real(unscaled) size of the image }
    property RealWidth: DInt16 read GetWidth;
    property RealHeight: DInt16 read GetHeight;
    { Set image size to unscaled }
    procedure ResetToRealSize(const ResetAnim: Boolean = false);
    { Color (tint) of this image
      if not set then GUITint will be used }
    property Color: DColor read fColor write SetColor;
    { Is the image higlighted? }
    property Highlight: TImageHighlight read fHighlight write fHighlight default false;
    procedure Draw; override;
    procedure SetTint; override;
  public
    constructor Create; override;
    destructor Destroy; override;
  end;

type
  { Most basic image type, ready to display the provided image
    Warning: for this image type "image" is only a reference
    and must not be freed (specified by OwnsImage = false) }
  DSimpleImage = class(DAbstractImage)
  public
    { Load the image. Doesn't claim ownership of the image! }
    procedure Load(const aImage: DImage);
  public
    constructor Create; override;
  end;

type
  { Image that is automatically loaded and assigned to FullScreen
    Used for background images }
  DFullScreenImage = class(DAbstractImage)
  public
    { Load the image. Doesn't claim ownership of the image! }
    procedure Load(const aImage: DImage);
  public
    constructor Create; override;
  end;

{............................................................................}
implementation
uses
  SysUtils, CastleGLImages,
  DecoConfig,
  DecoLog {%Profiler%};

{============================================================================}
{========================== D ABSTRACT IMAGE ================================}
{============================================================================}

destructor DAbstractImage.Destroy;
begin
  if OwnsImage then
    FreeAndNil(Image);
  inherited Destroy;
end;

{-----------------------------------------------------------------------------}

constructor DAbstractImage.Create;
begin
  inherited Create;
  isColorManual := false;
end;

{-----------------------------------------------------------------------------}

procedure DAbstractImage.SetTint;
begin
  //inherited SetTint; <---------- parent is an "empty" virtual procedure
  if not isColorManual then
    fColor := GUITint;
end;

{-----------------------------------------------------------------------------}

procedure DAbstractImage.SetColor(const aValue: DColor);
begin
  fColor := aValue;
  isColorManual := true;
end;

{-----------------------------------------------------------------------------}

procedure DAbstractImage.Draw;
begin
  {StartProfiler}

  //inherited Draw; <---------- parent is abstract
  if Image <> nil then
  begin
    Update;
    if Highlight then
      Image.Color := FColor + HighlightColor //draw the image a bit brighter and more achromatic
    else
      Image.Color := fColor;
    Image.SetAlpha(Self.Alpha);
    Image.Draw(Current.x, Current.y, Current.w, Current.h);
  end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

function DAbstractImage.GetWidth: DInt16;
begin
  if (Image <> nil) then
    Result := Image.Width
  else
    Result := -1;
end;
function DAbstractImage.GetHeight: DInt16;
begin
  if Image <> nil then
    Result := Image.Height
  else
    Result := -1;
end;

{-----------------------------------------------------------------------------}

procedure DAbstractImage.ResetToRealSize(const ResetAnim: Boolean = false);
begin
  {StartProfiler}

  BaseState.SetIntWidthHeight(GetWidth, GetHeight, BaseState.a);
  if ResetAnim then
    ResetAnimation
  else
    AnimateMe;

  {StopProfiler}
end;

{============================================================================}
{=========================== D SIMPLE IMAGE =================================}
{============================================================================}

constructor DSimpleImage.Create;
begin
  inherited Create;
  OwnsImage := false;
end;

{-----------------------------------------------------------------------------}

procedure DSimpleImage.Load(const aImage: DImage);
begin
  Image := aImage;
  SetTint;
end;

{============================================================================}

constructor DFullScreenImage.Create;
begin
  inherited Create;
  OwnsImage := false;
end;

{-----------------------------------------------------------------------------}

procedure DFullScreenImage.Load(const aImage: DImage);
begin
  Image := aImage;
  SetTint;
  FullScreen;
end;

end.

