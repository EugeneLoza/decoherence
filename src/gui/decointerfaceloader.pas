{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Load some content for the Interface *)

{$INCLUDE compilerconfig.inc}

unit DecoInterfaceLoader;

interface

{ Read all interface images, icons, cursor pointers, and so on }
procedure LoadInterface;
{ Free all non-automatically freed interface-related stuff }
procedure FreeInterface;
{............................................................................}
implementation
uses
  {$IFDEF BurnerImage}DecoBurner,{$ENDIF}
  DecoFont, DecoInterfaceText,
  DecoWind, DecoFrames, DecoImages,
  DecoImageLoader, DecoGUIScale,
  DecoLog {%Profiler%};

{............................................................................}

procedure LoadFrames;
var
  RoundedFrame: DFrameImage;
  StrictFrame: DFrameImage;
  StrictFrameWithGap: DFrameImage;
  LargeFrame: DFrameImage;
  PortraitFrame: DFrameImage;
  LogLabelFrame: DFrameImage;
  BrightFrameWithGap: DFrameImage;
  BrightFrame: DFrameImage;
begin
  RoundedFrame := LoadFrameImage('GUI/Frames/GradientFrame.png', 3, 3, 3, 3);
  StrictFrame := LoadFrameImage('GUI/Frames/StrictCornerFrame.png', 1, 1, 1, 1);
  StrictFrameWithGap := LoadFrameImage('GUI/Frames/StrictCornerFrame.png', 2, 2, 2, 2);
  PortraitFrame := LoadFrameImage('GUI/Frames/PortraitFrame.png', 1, 1, 1, 1);
  LargeFrame := LoadFrameImage('GUI/Frames/LoadScreenFrame.png', 16, 16, 16, 16);
  LogLabelFrame := LoadFrameImage('GUI/Frames/LogLabelFrame.png', 2, 5, 3, 5);
  BrightFrameWithGap := LoadFrameImage('GUI/Frames/BrightFrame.png', 5, 5, 5, 5);
  BrightFrame := LoadFrameImage('GUI/Frames/BrightFrame.png', 1, 1, 1, 1);

  FramesDictionary := TFramesDictionary.Create([]); //doesn't own children
  FramesDictionary.Add('RegularFrame', RoundedFrame);
  FramesDictionary.Add('PlayerStatBarFrame', StrictFrame);
  FramesDictionary.Add('PlayerBarsFrame', StrictFrameWithGap);
  FramesDictionary.Add('PlayerPortraitFrame', PortraitFrame);
  FramesDictionary.Add('ProgressbarFrame', StrictFrame);
  FramesDictionary.Add('StopScreenFrame', StrictFrameWithGap);
  FramesDictionary.Add('LoadScreenFrame', StrictFrameWithGap);
  FramesDictionary.Add('LoadScreenImageFrame', LargeFrame);
  FramesDictionary.Add('LogFrame', LogLabelFrame);
  FramesDictionary.Add('ButtonFrame', BrightFrameWithGap);
  //FramesDictionary.Add('ActionButtonFrame', BrightFrame);
  FramesDictionary.Add('MessageBoxFrame', RoundedFrame);
  FramesDictionary.Add('MainMenuFrame', RoundedFrame);
  FramesDictionary.Add('MainMenuButtonFrame', BrightFrame);
end;

{-----------------------------------------------------------------------------}

procedure LoadImages;
begin
  {StartProfiler}

  ImagesDictionary := TImagesDictionary.Create([]); //doesn't own children
  ImagesDictionary.Add('PlayerHealthBarImage',
    LoadDecoImage('GUI/StatBar/HealthBar.png', GUIUnit23 div 3, GUICharSpaceHeight));
  ImagesDictionary.Add('PlayerStaminaBarImage',
    LoadDecoImage('GUI/StatBar/StaminaBar.png', GUIUnit23 div 3, GUICharSpaceHeight));
  ImagesDictionary.Add('PlayerConcentrationBarImage',
    LoadDecoImage('GUI/StatBar/MentalBar.png', GUIUnit23 div 3, GUICharSpaceHeight));
  ImagesDictionary.Add('PlayerMetaphysicsBarImage',
    LoadDecoImage('GUI/StatBar/MetaphysicsBar.png', GUIUnit23 div 3, GUICharSpaceHeight));
  ImagesDictionary.Add('ProgressbarImage',
    LoadDecoImage('GUI/ProgressBar/ProgressBar.png', 32, 500));
  ImagesDictionary.Add('ButtonImageFrame',
    LoadDecoImage('GUI/Frames/CircleFrame.png', GUIUnit * 2, GUIUnit * 2));
  ImagesDictionary.Add('ActionButtonImageFrame',
    LoadDecoImage('GUI/Frames/CircleFrame.png', GUIUnit * 2, GUIUnit * 2));
  ImagesDictionary.Add('MainMenuImage',
    LoadDecoImage('GUI/Images/gears_CC-BY_by_Lorc.png', GUIUnit, GUIUnit));

  ImagesDictionary.Add('ActionImagePlaceholder',
    LoadDecoImage('GUI/Actions/uncertainty_icon_CC-BY_by_Lorc.png', GUIUnit, GUIUnit));

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure LoadInterface;
begin
  {StartProfiler}

  Log(CurrentRoutine, 'Loading interface files.');
  InitFonts;
  LoadInterfaceText;
  {$IFDEF BurnerImage}
  InitBurnerImage;
  {$ENDIF}
  InitWind;
  LoadFrames;
  LoadImages;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure FreeInterface;
begin
  Log(CurrentRoutine, 'Freeing interface objects.');
  FreeFonts;
  FreeInterfaceText;
  FramesDictionary.Free;
  ImagesDictionary.Free;
end;

end.

