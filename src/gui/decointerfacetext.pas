{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Generic GUI text strings *)

{$INCLUDE compilerconfig.inc}

unit DecoInterfaceText;

interface

uses
  DecoGenerics;

{ Get GUI text by Alias }
function GetInterfaceText(const aAlias: String): String; TryInline
{ Read Generic GUI text from a file }
procedure LoadInterfaceText;
{ Free Generic GUI text }
procedure FreeInterfaceText;
{............................................................................}
implementation
uses
  DecoLog {%Profiler%};

var
  InterfaceText: DStringDictionary;

procedure LoadInterfaceText;
begin
  InterfaceText := DStringDictionary.Create;
  //depending on current language
  InterfaceText.Add('Ok', 'Ok');
  InterfaceText.Add('ResumeGame', 'Resume game');
  InterfaceText.Add('ExitGame', 'Exit game');
  InterfaceText.Add('ExitConfirmation', 'Really quit? Current progress will be lost.');
  InterfaceText.Add('Cheater1', 'No! This is a different game!');
end;

{-----------------------------------------------------------------------------}

procedure FreeInterfaceText;
begin
  InterfaceText.Free;
end;

{-----------------------------------------------------------------------------}

function GetInterfaceText(const aAlias: String): String; TryInline
begin
  Result := GetStringByKey(InterfaceText, aAlias);
  if Result = '' then
  begin
    Result := '%' + aAlias;
    Log(CurrentRoutine, 'Warning: Unknown interface text requested: ' + aAlias);
  end;
end;

end.

