{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Different types of labels
   All labels are rendered as images to (significantly) boost performance. *)

{$INCLUDE compilerconfig.inc}

unit DecoLabels;

interface

uses
  DecoInterfaceCore, DecoInterfaceArrangers,
  DecoInterfaceImages, DecoFont, DecoFramedElement,
  DecoBaseActor,
  DecoTime, DecoGlobal;

type
  { A powerful text label, converted to GLImage to be extremely fast }
  DLabelImage = class(DAbstractImage)
  strict private
    { Text in this label }
    fText: String;
    { Width at which the text image was rendered }
    RecentLabelWidth: DInt16;
    { Change the current fText and call Prepare Text Image if needed }
    procedure SetText(const Value: String);
    { Get the label width to prepare text image }
    function GetLabelWidth: DInt16;
    { Converts String (Text) into an image }
    procedure PrepareTextImage;
  public
    { Font to print the label }
    Font: DFont;
    { Shadow intensity. Shadow=0 is no shadow (strictly), default 0 }
    ShadowIntensity: DFloat;
    { Shadow length in pixels, default 3 }
    ShadowLength: DInt16;
    { Label type }
    LabelType: TAlignType;
    { Text at the label }
    property Text: String read fText write SetText;
    { Width of container of this label
      ltOneLine has "extremely long width" }
    property LabelWidth: DInt16 read GetLabelWidth;
    procedure SizeChanged(const Animate: TAnimationStyle; const Duration: DTime); override;
  public
    constructor Create; override;
  end;

type
  { An aligned Label,
    Wrapper for DLabelImage }
  DLabel = class(DAbstractArranger)
  strict protected
    FLabel: DLabelImage;
    procedure SetText(const aValue: String);
    function GetText: String;
    procedure SetFont(const aValue: DFont);
    function GetFont: DFont;
    function GetWidth: DInt16;
    function GetHeight: DInt16;
    function GetLabelType: TAlignType;
    procedure SetLabelType(const aValue: TAlignType);
    function GetHighlight: Boolean;
    procedure SetHighlight(const aValue: Boolean);
    function GetShadowIntensity: DFloat;
    procedure SetShadowIntensity(const aValue: DFloat);
    procedure ArrangeChildren(const Animate: TAnimationStyle; const Duration: DTime); override;
  public
    { Text assigned to this label }
    property Text: String read GetText write SetText;
    { Font of the label }
    property Font: DFont read GetFont write SetFont;
    { Reset this label to real (crisp) size }
    procedure ResetToRealSize(const ResetAnim: Boolean = false);
    { Real width of the label }
    property RealWidth: DInt16 read GetWidth;
    { Real height of the label }
    property RealHeight: DInt16 read GetHeight;
    { Type of this label }
    property LabelType: TAlignType read GetLabelType write SetLabelType;
    { If the label is highlighted }
    property Highlight: Boolean read GetHighlight write SetHighlight;
    { Intensity of label shadow }
    property ShadowIntensity: DFloat read GetShadowIntensity write SetShadowIntensity;
  public
    constructor Create; override;
  end;

type
  { A simple FPS-counting label
    It's a separate GUI element and is used/managed directly by DGUI
    Practically it just increases FPScount by 1 each CountFPS call
    and changes displayed value approx once per second }
  DFPSLabel = class(DLabelImage)
  strict private
    FPScount: DInt16;
    LastRenderTime: DTime;
  public
    { Call this each frame instead of Draw,
         Draw is automatically called here }
    procedure CountFPS;
  public
    constructor Create; override;
  end;

type
  { A text label within a rectagonal frame }
  DFramedLabel = class(DRectagonalFramedElement)
  strict protected
    FLabel: DLabel;
    procedure SetText(const aValue: String);
    function GetText: String;
    procedure SetFont(const aValue: DFont);
    function GetFont: DFont;
  public
    { Text assigned to this label }
    property Text: String read GetText write SetText;
    { Font of this label }
    property Font: DFont read GetFont write SetFont;
    procedure ArrangeChildren(const Animate: TAnimationStyle; const Duration: DTime); override;
  public
    constructor Create; override;
  end;

type
  { Label to display amount of health of the Actor within a frame }
  DHealthLabel = class(DFramedLabel)
  strict private
    FTarget: DBaseActor;
    procedure SetTarget(const aActor: DBaseActor);
  public
    { Which actor's health is displayed }
    property Target: DBaseActor read FTarget write SetTarget;
    procedure Update; override;
  public
    constructor Create; override;
  end;

{............................................................................}
implementation
uses
  SysUtils,
  CastleImages,
  DecoImages, DecoFrames, DecoInterfaceContainer,
  DecoLog {%Profiler%};

constructor DLabelImage.Create;
begin
  inherited Create;
  RecentLabelWidth := -1;
  LabelType := atLeft;
  ShadowIntensity := 0;
  ShadowLength := 3;
  Font := DebugFont;
  OwnsImage := true;
end;

{-----------------------------------------------------------------------------}

procedure DLabelImage.SetText(const Value: String);
begin
  {StartProfiler}

  if fText <> Value then
  begin
    fText := Value;
    PrepareTextImage;
  end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

function DLabelImage.GetLabelWidth: DInt16;
begin
  if LabelType = atOneLine then
    Result := MaxInt16 div 2
  else
    Result := BaseState.w;
end;

{-----------------------------------------------------------------------------}

procedure DLabelImage.PrepareTextImage;
var
  TextImage: TGrayscaleAlphaImage;
begin
  {StartProfiler}

  FreeAndNil(Image);

  RecentLabelWidth := GetLabelWidth;

  if fText <> '' then
  begin
    if ShadowIntensity = 0 then
      TextImage := Font.StringToImage(fText, LabelWidth, LabelType)
    else
      TextImage := Font.StringToImageWithShadow(fText, LabelWidth,
        ShadowIntensity, ShadowLength, LabelType);

    Image := DImage.Create(TextImage, true, true);
    SetTint;
  end else
    Image := nil;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

{$PUSH}{$WARN 5024 off}
procedure DLabelImage.SizeChanged(const Animate: TAnimationStyle; const Duration: DTime);
begin
  {StartProfiler}

  if (FText <> '') and (LabelWidth <> RecentLabelWidth) then
  begin
    Log(CurrentRoutine, 'Warning: changing size of a non-empty label; content = ' + FText);
    PrepareTextImage;
  end;

  {StopProfiler}
end;
{$POP}

{=============================================================================}

procedure DLabel.SetText(const aValue: String);
begin
  if aValue <> FLabel.Text then
  begin
    FLabel.BaseState.AssignFrom(Self.BaseState);
    FLabel.Text := aValue;
  end;
end;
function DLabel.GetText: String;
begin
  Result := FLabel.Text;
end;

{---------------------------------------------------------------------------}

procedure DLabel.SetFont(const aValue: DFont);
begin
  FLabel.Font := aValue;
end;
function DLabel.GetFont: DFont;
begin
  Result := FLabel.Font;
end;

{---------------------------------------------------------------------------}

procedure DLabel.ResetToRealSize(const ResetAnim: Boolean = false);
begin
  FLabel.ResetToRealSize(ResetAnim);
end;

{---------------------------------------------------------------------------}

function DLabel.GetWidth: DInt16;
begin
  Result := FLabel.RealWidth;
end;
function DLabel.GetHeight: DInt16;
begin
  Result := FLabel.RealHeight;
end;

{---------------------------------------------------------------------------}

function DLabel.GetLabelType: TAlignType;
begin
  Result := FLabel.LabelType;
end;
procedure DLabel.SetLabelType(const aValue: TAlignType);
begin
  FLabel.LabelType := aValue;
end;

{---------------------------------------------------------------------------}

function DLabel.GetHighlight: Boolean;
begin
  Result := FLabel.Highlight;
end;
procedure DLabel.SetHighlight(const aValue: Boolean);
begin
  FLabel.Highlight := aValue;
end;

{---------------------------------------------------------------------------}

function DLabel.GetShadowIntensity: DFloat;
begin
  Result := FLabel.ShadowIntensity;
end;
procedure DLabel.SetShadowIntensity(const aValue: DFloat);
begin
  FLabel.ShadowIntensity := aValue;
end;

{---------------------------------------------------------------------------}

procedure DLabel.ArrangeChildren(const Animate: TAnimationStyle; const Duration: DTime);
var
  ToState: DInterfaceContainer;
begin
  {StartProfiler}

  //inherited ArrangeChildren(Animate, Duration); <------- ParentIsEmpty
  ToState.AssignFrom(Self.BaseState);
  ToState.a := 1;

  //no other children of DLabel
  ToState.SetIntSize(Self.BaseState.x, Self.BaseState.y, FLabel.RealWidth, FLabel.RealHeight); //align left
  FLabel.SetBaseSize(ToState, Animate, Duration);

  {StopProfiler}
end;

{---------------------------------------------------------------------------}

constructor DLabel.Create;
begin
  inherited Create;

  FLabel := DLabelImage.Create;
  InsertFront(FLabel);
end;

{=============================================================================}

constructor DFPSLabel.Create;
begin
  inherited Create;
  LabelType := atOneLine;
  FPSCount := 0;
  LastRenderTime := -1;

  SetBaseSize(0, 0, 1, 1);
  Font := FPSFont;
  Text := ' '; //initialize the label, so that it always has an image
end;

{---------------------------------------------------------------------------}

procedure DFPSLabel.CountFPS;
begin
  if LastRenderTime < 0 then
    LastRenderTime := DecoNow;

  if (DecoNow - LastRenderTime >= 1) then
  begin
    Text := IntToStr(FPSCount){+' '+IntToStr(Round(Window.Fps.RealTime))};
    Self.ResetToRealSize(true);
    Self.BaseState.x := 0;
    Self.BaseState.y := 0;
    FPSCount := 0;
    LastRenderTime := DecoNow;
  end else
    inc(FPSCount);

  Draw;
end;

{=============================================================================}

constructor DFramedLabel.Create;
begin
  inherited Create;
  FLabel := DLabel.Create;
  InsertFront(FLabel);
end;

{---------------------------------------------------------------------------}

procedure DFramedLabel.SetText(const aValue: String);
begin
  FLabel.Text := aValue;
end;
function DFramedLabel.GetText: String;
begin
  Result := FLabel.Text;
end;

{---------------------------------------------------------------------------}

procedure DFramedLabel.SetFont(const aValue: DFont);
begin
  FLabel.Font := aValue;
end;
function DFramedLabel.GetFont: DFont;
begin
  Result := FLabel.Font;
end;

{---------------------------------------------------------------------------}

procedure DFramedLabel.ArrangeChildren(const Animate: TAnimationStyle; const Duration: DTime);
begin
  {StartProfiler}

  //inherited ArrangeChildren(Animate, Duration); <------- Completely overwrite parent

  UpdateFrame(Animate, Duration);

  //no other children of DFramedLabel
  ToState.SetIntWidthHeight(FLabel.RealWidth, FLabel.RealHeight);
  FLabel.SetBaseSize(ToState, Animate, Duration);

  {StopProfiler}
end;

{=============================================================================}

procedure DHealthLabel.SetTarget(const aActor: DBaseActor);
begin
  if FTarget <> aActor then
  begin
    FTarget := aActor;
    Update;
  end;
end;

{---------------------------------------------------------------------------}

procedure DHealthLabel.Update;
begin
  inherited Update;
  if FTarget <> nil then
    Text := IntToStr(Round(FTarget.Hp.Value[0]));
end;

{---------------------------------------------------------------------------}

constructor DHealthLabel.Create;
begin
  inherited Create;
  LoadFrame(GetFrameByName('PlayerStatBarFrame'));
  FLabel.Font := GetFontByName('PlayerHealth');
  FLabel.LabelType := atOneLine;
end;

end.

