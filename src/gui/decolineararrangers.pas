{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Arranger that arranges the interface elements into
   vertical or horizontal stripe *)

{$INCLUDE compilerconfig.inc}

unit DecoLinearArrangers;

interface

uses
  DecoInterfaceCore, DecoInterfaceArrangers,
  DecoTime, DecoGlobal;

type
  { How to arrange the elements - vertically or horizontally }
  DArrangerStyle = (asVertical, asHorizontal);

type
  { Arranges the elements vertically or horizontally }
  DLinearArranger = class(DAbstractArranger)
  public
    { Vertical or horizontal? }
    ArrangerStyle: DArrangerStyle;
    procedure ArrangeChildren(const Animate: TAnimationStyle; const Duration: DTime); override;
  end;

{............................................................................}
implementation
uses
  DecoInterfaceContainer, DecoGUIScale,
  DecoLog {%Profiler%};

procedure DLinearArranger.ArrangeChildren(const Animate: TAnimationStyle; const Duration: DTime);
var
  i: Integer;
  Size: DInt16;
  SizeWithGap: Single;

  function ArrangeElement(const aContainer: DInterfaceContainer): DInterfaceContainer;
  begin
    Result.AssignFrom(aContainer);
    if ArrangerStyle = asHorizontal then
    begin
      Result.w := Size;
      Result.x += Round(i * SizeWithGap) + GUILargeGap;
      Result.h -= 2 * GUILargeGap;
      Result.y += GUILargeGap;
    end else
    begin
      Result.h := Size;
      Result.y += Round(i * SizeWithGap) + GUILargeGap;
      Result.w -= 2 * GUILargeGap;
      Result.x += GUILargeGap;
    end;
  end;
begin
  {StartProfiler}

  //inherited ArrangeChildren(Animate, Duration); <------- parent is abstract

  if ArrangerStyle = asHorizontal then
  begin
    SizeWithGap := BaseState.w / Children.Count;
    Size := Round((BaseState.w - 2 * GUILargeGap) / Children.Count) - GUILargeGap;
  end else
  begin
    SizeWithGap := BaseState.h / Children.Count;
    Size := Round((BaseState.h - 2 * GUILargeGap) / Children.Count) - GUILargeGap;
  end;

  for i := 0 to Pred(Children.Count) do
    Children[i].SetBaseSize(ArrangeElement(BaseState), Animate, Duration);

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}




end.

