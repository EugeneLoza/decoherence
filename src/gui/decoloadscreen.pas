{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* LoadScreen that is displayed during game loading/generation *)

{$INCLUDE compilerconfig.inc}

unit DecoLoadScreen;

interface

uses
  DecoStopScreen, DecoFramedElement,
  DecoLoadScreenImage,
  DecoTime, DecoGlobal;

const
  { How long a single fact/image is displayed }
  LoadScreenChangeTime = 10; {seconds}

type
  { A full-screen cover to display loading/generation progress }
  DLoadScreen = class(DRectagonalFramedElement)
  strict private
    { Last time the new fact/image was requested }
    FLastChanged: DTime;
    { Image to display during LoadScreen }
    FImage: DLoadScreenImage;
  public
    { A progress-bar with Loading... text
      for now it's just a copy of DStopScreen }
    FStopScreen: DStopScreen;
    { Updates progressbar state and requests new fact/image if necessary }
    procedure UpdateProgress(const aProgress: DFloat);
  public
    constructor Create; override;
    //destructor Destroy; override;
  end;

{............................................................................}
implementation
uses
  DecoFrames, DecoGUIScale,
  DecoLog {%Profiler%};

constructor DLoadScreen.Create;
begin
  inherited Create;
  FullScreen;
  FLastChanged := -1;

  LoadFrame(GetFrameByName('LoadScreenFrame'));

  FImage := DLoadScreenImage.Create;
  FImage.SetBaseSize(GUIUnit2, GUIUnit * 5 div 2, GUIWidth - GUIUnit, GUIHeight - GUIUnit * 3);
  InsertFront(FImage);

  FStopScreen := DStopScreen.Create;
  InsertFront(FStopScreen);
end;

{-----------------------------------------------------------------------------}

procedure DLoadScreen.UpdateProgress(const aProgress: DFloat);
begin
  FStopScreen.Position := aProgress;
  if (FLastChanged < 0) or (GetNow - FLastChanged > LoadScreenChangeTime) then
  begin
    FImage.LoadNewImage;
    FLastChanged := GetNow;
  end;
end;

end.

