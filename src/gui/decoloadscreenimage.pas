{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Image and Label that are displayed during LoadScreen *)

{$INCLUDE compilerconfig.inc}

unit DecoLoadScreenImage;

interface

uses
  DecoInterfaceImages, DecoFramedElement, DecoLabels,
  DecoGlobal;

type
  { The actual image to display }
  DLoadScreenImageContent = class(DAbstractImage)
  public
    { Reloads a new random image }
    procedure LoadNewImage(const ParentX, ParentWidth: DInt16);
  public
    constructor Create; override;
  end;

type
  { Label with a Fact text }
  DFactLabel = class(DLabelImage)
  public
    { Get a new fact and assign it to the label }
    procedure LoadNewFact(const ParentX, ParentY, ParentWidth: DInt16);
  public
    constructor Create; override;
  end;

type
  { Framed LoadScrrenImageContent }
  DLoadScreenImage = class(DRectagonalFramedElement)
  strict private
    { LoadScrrenImageContent to display}
    FLoadScreenImage: DLoadScreenImageContent;
    { FactLabel to display }
    FFactLabel: DFactLabel;
  public
    { Reloads a new random image }
    procedure LoadNewImage;
  public
    constructor Create; override;
  end;

{............................................................................}
implementation
uses
  SysUtils,
  DecoGUIScale,
  DecoImageLoader, DecoFolders, DecoFrames, DecoFont,
  DecoMath, DecoLog {%Profiler%};

function TemporaryGetNewImage: String;
begin
  case DRND.Random(4) of
    0: Result := 'abstract-background-purple-2_CC0_by_Tammy_Sue_[crop,softglow].jpg';
    1: Result := 'ce-abstract-golds-bg_CC0_by_Gale_Titus_[blur,noise].jpg';
    2: Result := 'colour-of-nature_CC0_by_Sharon_Apted_[GMIC,colorize].jpg';
    3: Result := 'colour-of-nature-fractal_CC0_by_Sharon_Apted_[colorize].jpg';
    else
      Log(CurrentRoutine, 'Wrong random number');
  end;
end;

function TemporaryFact: String;
begin
  case DRND.Random(4) of
    0: Result := 'While pointed weapon is better against armored targets, cutting weapon deals more damage.';
    1: Result := 'Swooping enemies cannot be attacked in melee. One should either use ranged weapon or wait for counter-attack chance.';
    2: Result := 'Paralyzed and knocked-out characters are easy target for enemy.';
    3: Result := 'The character can actively resist external influence, which in turn requires using concentration and endurance.';
    else
      Log(CurrentRoutine, 'Wrong random number');
  end;
end;

{-----------------------------------------------------------------------------}

constructor DLoadScreenImageContent.Create;
begin
  inherited Create;
  OwnsImage := true;
end;

{-----------------------------------------------------------------------------}

procedure DLoadScreenImageContent.LoadNewImage(const ParentX, ParentWidth: DInt16);
var
  ImageName: String;
begin
  FreeAndNil(Image);
  ImageName := TemporaryGetNewImage;
  Log(CurrentRoutine, 'Loading a new image');
  Image := LoadDecoImageUnmanaged('GUI/LoadScreens/' + ImageName, BaseState.w, BaseState.h, true);
  ResetToRealSize(true);
  SetTint;
  //now shift the image by golden ratio to the left
  BaseState.x := ParentX + Round((ParentWidth - BaseState.w) * (1 - GoldenRatio));
end;

{=============================================================================}

procedure DFactLabel.LoadNewFact(const ParentX, ParentY, ParentWidth: DInt16);
begin
  BaseState.w := ParentWidth div 3;
  Text := TemporaryFact;

  ResetToRealSize(true);
  //now shift the image by golden ratio to the left
  BaseState.x := ParentX + ParentWidth - Self.RealWidth - GUIUnit;
  BaseState.y := ParentY + GUIUnit;
end;

{-----------------------------------------------------------------------------}

constructor DFactLabel.Create;
begin
  inherited Create;
  Font := GetFontByName('LoadScreenFact');
  ShadowIntensity := 1;
  LabelType := atRight;
end;

{=============================================================================}

procedure DLoadScreenImage.LoadNewImage;
begin
  FLoadScreenImage.LoadNewImage(BaseState.x, BaseState.w);
  FFactLabel.LoadNewFact(BaseState.x, BaseState.y, BaseState.w);
end;

{-----------------------------------------------------------------------------}

constructor DLoadScreenImage.Create;
begin
  inherited Create;
  LoadFrame(GetFrameByName('LoadScreenImageFrame'));
  FLoadScreenImage := DLoadScreenImageContent.Create;
  InsertFront(FLoadScreenImage);
  FFactLabel := DFactLabel.Create;
  InsertFront(FFactLabel);
end;

end.

