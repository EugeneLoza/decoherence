{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* An array of labels to display game messages/logs
   such as actions / information, etc. *)

{$INCLUDE compilerconfig.inc}

unit DecoLogLabel;

interface

uses
  Generics.Collections,
  DecoFont, DecoInterfaceImages, DecoFramedElement,
  DecoGlobal;

const
  { How long does the LogLabel remains highlighted after a new message has been added
    Then fades out }
  NewMessageTimeOut = 3; {seconds}

type
  { Label, containing multiple messsages from GUI.LogMessages }
  DMultiLabel = class(DAbstractImage)
  public
    { Font to print the label }
    Font: DFont;
    { Renders text into image }
    procedure PrepareTextImage;
  public
    constructor Create; override;
  end;

type
  { A block with several labels }
  DLogLabel = class(DRectagonalFramedElement)
  strict private
    FLabel: DMultiLabel;
    { Mouse events }
    procedure doMouseEnter;
    procedure doMouseLeave;
  public
    { Call back to re-render the Label (when the content has been changed) }
    procedure DoRender;
  public
    constructor Create; override;
    destructor Destroy; override;
  end;

{............................................................................}
implementation
uses
  SysUtils,
  CastleImages, CastleVectors,
  DecoInterfaceCore, DecoGUI, DecoImages, DecoTimer,
  DecoColors, DecoFrames, DecoLogMessages,
  DecoLog {%Profiler%};

type
  { Extension function to TRGBAlphaImage }
  TRGBAlphaImageHelper = class helper for TRGBAlphaImage
    { Draw TGrayscaleAlphaImage over TRGBAlphaImage colorized with Color
      This is a altered version of TRGBAlphaImage.DrawFrom(dmOverwrite) }
    procedure DrawColored(const Source: TCastleImage;
        const X, Y: Integer; const Color: DColor);
  end;

procedure TRGBAlphaImageHelper.DrawColored(const Source: TCastleImage;
  const X, Y: Integer; const Color: DColor);
const
  SourceX = 0;
  SourceY = 0;
var
  PSource: PVector2Byte;
  PDest: PVector4Byte;
  DestX, DestY: Integer;
begin
  {StartProfiler}

  for DestY := Y to Y + Source.Height - 1 do
  begin
    PSource := Source.PixelPtr(SourceX, SourceY + DestY - Y);
    PDest := PixelPtr(X, DestY);
    for DestX := X to X + Source.Width - 1 do
    begin
      PDest^.Data[0] := Trunc(PSource^.Data[0] * Color[0]);
      PDest^.Data[1] := Trunc(PSource^.Data[0] * Color[1]);
      PDest^.Data[2] := Trunc(PSource^.Data[0] * Color[2]);
      PDest^.Data[3] := Trunc(PSource^.Data[1] * Color[3]);
      Inc(PSource);
      Inc(PDest);
    end;
  end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DLogLabel.DoMouseEnter;
begin
  Self.BaseState.a := 0.9;
  AnimateMe(asDefault);
end;

{-----------------------------------------------------------------------------}

procedure DLogLabel.DoMouseLeave;
begin
  if (not isMouseOver) and (not Timer.Enabled) then  //Timer.Enabled is set to false before calling MouseLeave
  begin
    Self.BaseState.a := 0.5;
    AnimateMe(asDefault);
  end;
end;

{-----------------------------------------------------------------------------}

procedure DLogLabel.doRender;
begin
  {StartProfiler}

  FLabel.PrepareTextImage;
  FLabel.ResetToRealSize(true);

  DoMouseEnter; //flash bright
  Timer.SetTimeOut(NewMessageTimeOut); //and dull in one second, if not mouse is over

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

constructor DLogLabel.Create;
begin
  inherited Create;
  Timer := DInterfaceTimer.Create;
  Timer.onTimer := @DoMouseLeave; //will become "dull" after timeout

  //LoadFrame(GetFrameByName('LogFrame'));
  onMouseEnter := @DoMouseEnter;
  onMouseLeave := @DoMouseLeave;

  FLabel := DMultiLabel.Create;
  FLabel.Color := clWhite; //prevent GUITint from messing up with label color
  InsertFront(FLabel);
  GUI.LogMessages.onChanged := @doRender;
  //doRender;
end;

{-----------------------------------------------------------------------------}

destructor DLogLabel.Destroy;
begin
  inherited Destroy;
end;

{=============================================================================}

constructor DMultiLabel.Create;
begin
  inherited Create;
  OwnsImage := true;
  Font := GetFontByName('LogLabel');
end;

{-----------------------------------------------------------------------------}

procedure DMultiLabel.PrepareTextImage;
var
  TextImage: TRGBAlphaImage;
  tmpImage: TGrayscaleAlphaImage;
  i: Integer;
  yy: DInt16;
begin
  {StartProfiler}

  FreeAndNil(Image);

  TextImage := TRGBAlphaImage.Create(BaseState.w, BaseState.h);
  TextImage.Clear(clClear);
  yy := 0;
  for i := Pred(GUI.LogMessages.MessageList.Count) downto 0 do
  begin
    tmpImage := Font.StringToImage(GUI.LogMessages.MessageList[i].Message, BaseState.w);
    TextImage.DrawColored(tmpImage, 0, yy, GUI.LogMessages.MessageList[i].Color);
    yy += tmpImage.Height;
    tmpImage.Free;
  end;

  Image := DImage.Create(TextImage, true, true);
  SetTint;

  {StopProfiler}
end;

end.

