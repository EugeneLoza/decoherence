{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ --------------------------------------------------------------------------- }

(* Main menu of the game (accessed during the game) *)

{$INCLUDE compilerconfig.inc}

unit DecoMainMenu;

interface

uses
  DecoFramedElement, DecoButtons;

type
  { Main menu container }
  DMainMenu = class(DRectagonalFramedElement)
  strict private
    FButtonArranger: DTextButtonArranger;
    { CallBack to exit game }
    procedure ExitGame;
  public
    { CallBack to close the menu }
    procedure CloseMenu;
  public
    constructor Create; override;
  end;

type
  { A on-screen button to show the MainMenu }
  DMainMenuButton = class(DAbstractRectagonalImageButton)  //DImageButton
  public
    constructor Create; override;
  end;

{............................................................................}
implementation
uses
  DecoInterfaceCore, DecoImages, DecoFrames, DecoInterfaceText,
  DecoLinearArrangers,
  DecoGUI, DecoWindowCloseConfirmation,
  DecoWindow, DecoLog {%Profiler%};

procedure DMainMenu.ExitGame;
begin
  TryWindowClose;
end;

{-----------------------------------------------------------------------------}

procedure DMainMenu.CloseMenu;
begin
  AnimateMe(asFadeOut);
end;

{-----------------------------------------------------------------------------}

constructor DMainMenu.Create;
begin
  inherited Create;
  LoadFrame(GetFrameByName('MainMenuFrame'));
  FButtonArranger := DTextButtonArranger.Create;
  FButtonArranger.AddButton(GetInterfaceText('ExitGame'), @ExitGame);
  FButtonArranger.AddButton(GetInterfaceText('ResumeGame'), @CloseMenu);
  FButtonArranger.ArrangerStyle := asVertical;
  InsertFront(FButtonArranger);
end;

{=============================================================================}

constructor DMainMenuButton.Create;
begin
  inherited Create;
  LoadFrame(GetFrameByName('MainMenuButtonFrame'));
  FImage.Load(GetImageByName('MainMenuImage'));
  OnMouseLeftButton := @GUI.ShowMainMenu;
end;

end.

