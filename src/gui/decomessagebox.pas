{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ --------------------------------------------------------------------------- }

(* A simple message box with buttons *)

{$INCLUDE compilerconfig.inc}

unit DecoMessageBox;

interface

uses
  DecoInterfaceCore,
  DecoLabels, DecoFramedElement, DecoButtons,
  DecoGlobal, DecoTime;

type
  { General methods to handle MessageBox }
  DAbstractMessageBox = class abstract(DRectagonalFramedElement)
  strict protected
    { Text to display in the MessageBox }
    FLabel: DLabelImage;
    { Set of buttons below the MessageBox }
    FButtonArranger: DTextButtonArranger;
  strict private
    procedure SetText(const aValue: String);
    function GetText: String;
  strict protected
    { Call-back to close the MessageBox }
    procedure CloseMessageBox;
  public
    { Current text in the MessageBox }
    property Text: String read GetText write SetText;
    procedure ArrangeChildren(const Animate: TAnimationStyle; const Duration: DTime); override;
  public
    constructor Create; override;
  end;

type
  { A simple message box with "Ok" button }
  DSimpleMessageBox = class(DAbstractMessageBox)
  public
    constructor Create; override;
  end;

{............................................................................}
implementation
uses
  DecoInterfaceContainer, DecoFont, DecoInterfaceText, DecoLinearArrangers,
  DecoFrames, DecoGUIScale,
  DecoLog {%Profiler%};

procedure DAbstractMessageBox.ArrangeChildren(const Animate: TAnimationStyle; const Duration: DTime);
  function LabelState(const aInitialState: DInterfaceContainer): DInterfaceContainer;
  begin
    Result.AssignFrom(aInitialState);
    Result.y += GUIUnit;
    Result.h -= GUIUnit;
  end;
  function ButtonState(const aInitialState: DInterfaceContainer): DInterfaceContainer;
  begin
    Result.AssignFrom(aInitialState);
    Result.h := GUIUnit;
  end;
begin
  {StartProfiler}

  //inherited ArrangeChildren(Animate, Duration); <------- overriding the parent completely

  UpdateFrame(Animate, Duration);

  FLabel.SetBaseSize(LabelState(ToState), Animate, Duration);
  FLabel.ResetToRealSize(true);

  FButtonArranger.SetBaseSize(ButtonState(ToState), Animate, Duration);

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DAbstractMessageBox.SetText(const aValue: String);
begin
  FLabel.Text := aValue;
end;
function DAbstractMessageBox.GetText: String;
begin
  Result := FLabel.Text;
end;

{-----------------------------------------------------------------------------}

procedure DAbstractMessageBox.CloseMessageBox;
begin
  AnimateMe(asFadeOut);
end;

{-----------------------------------------------------------------------------}

constructor DAbstractMessageBox.Create;
begin
  inherited Create;
  LoadFrame(GetFrameByName('MessageBoxFrame'));
  FLabel := DLabelImage.Create;
  FLabel.Font := GetFontByName('MessageBoxFont');
  InsertFront(FLabel);
  FButtonArranger := DTextButtonArranger.Create;
  InsertFront(FButtonArranger);
end;

{=============================================================================}

constructor DSimpleMessageBox.Create;
begin
  inherited Create;
  FButtonArranger.AddButton(GetInterfaceText('Ok'), @CloseMessageBox);
  FButtonArranger.ArrangerStyle := asHorizontal;
end;

end.

