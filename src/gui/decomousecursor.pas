{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Management of mouse cursor/pointer *)

{$INCLUDE compilerconfig.inc}

unit DecoMouseCursor;

interface

uses
  CastleVectors,
  DecoTransformWorld,
  DecoImages, DecoInterfaceCore, DecoLabels,
  DecoGlobal;

type
  TCursorType = (
    { No cursor. Normally, this shouldn't happen. }
    ctNone,
    { Default cursor }
    ctDefault,
    { Crosshair in the center of the screen }
    ctMouseLook,
    { Cursor pointing at a friendly Actor }
    ctAlly,
    { Cursor pointing at a hostile Actor }
    ctEnemy,
    { Magnification glass (inspect object) }
    ctInspect,
    { Grab something, e.g. an item }
    ctGrab,
    { Use/manipulate a world object }
    ctUse,
    { Question mark }
    ctUnknown);

type
  { Mouse/touch cursor }
  DCursor = class(TObject)
  strict private
    { Interface element under Cursor }
    FOverElement: DSingleInterfaceElement;
    { World transform under Cursor }
    FOverTransform: DTransformWorld;
    { Label that displays hint for the GUI element }
    HintLabel: DLabel;
    { Size of hint }
    HintWidth, HintHeight, HintGap: DInt16;
    { A set of images to be used to display cursor in different situations }
    CursorImg: array[TCursorType] of DCursorImage;
    { Last text of shown hint }
    LastHintText: String;
    { Update the displayed hint text }
    procedure UpdateHintLabel;
    { Update hint location on Screen }
    procedure UPdateHintLabelCoordinates;
    { Assign OverElement and set Hint if needed }
    procedure SetOverElement(const aValue: DSingleInterfaceElement);
    { Assign OverTransform }
    procedure SetOverTransform(const aValue: DTransformWorld);
    { Get current CursorType }
    function GetCurrentCursor: TCursorType; TryInline
  public
    { Coordinates of the cursor }
    x, y: Single;
    { Current cursor type (determines displayed cursor image) }
    CurrentCursor: TCursorType;
    { Element this cursor currently drags or clicks }
    DragElement, ClickElement: DSingleInterfaceElement;
    { What GUI element the cursor is over }
    property OverElement: DSingleInterfaceElement read FOverElement write SetOverElement;
    { Transform under Cursor }
    property OverTransform: DTransformWorld read FOverTransform write SetOverTransform;
    { Change the tint of the cursor }
    procedure SetTint;
    { Draw the cursor on screen }
    procedure Draw;
    { Hides the OS cursor }
    procedure HideOSCursor;
  public
    constructor Create; //override; <----- non-virtual constructor
    destructor Destroy; override;
  end;

{............................................................................}
implementation
uses
  SysUtils, CastleKeysMouse,
  DecoImageLoader, DecoGUIScale, DecoFont, DecoColors, DecoMouse,
  DecoBody,
  DecoWindow, DecoConfig, DecoLog {%Profiler%};

constructor DCursor.Create;
begin
  //inherited Create; <--------- nothing to inherit

  {todo: remake it into something useful}
  CursorImg[ctDefault] := LoadCursorImage('GUI/Cursors/default.png', -1, +1);
  CursorImg[ctMouseLook] := LoadCursorImage('GUI/Cursors/mouselook.png', -15, +15);
  CursorImg[ctAlly] := LoadCursorImage('GUI/Cursors/ally.png', -1, +1);
  CursorImg[ctEnemy] := LoadCursorImage('GUI/Cursors/enemy.png', -1, +1);
  CursorImg[ctInspect] := LoadCursorImage('GUI/Cursors/inspect.png', -11, +11);
  CursorImg[ctGrab] := LoadCursorImage('GUI/Cursors/grab.png', -15, +15);
  CursorImg[ctUse] := LoadCursorImage('GUI/Cursors/use.png', -13, +15);
  CursorImg[ctUnknown] := LoadCursorImage('GUI/Cursors/unknown.png', -1, +1);

  CurrentCursor := ctDefault;

  LastHintText := '';
  HintLabel := DLabel.Create;
  HintLabel.Font := GetFontByName('HintFont');
  HintWidth := GUIUnit * 3;
  HintHeight := GUIUnit * 2;
  HintGap := GUILargeGap;
end;

{-----------------------------------------------------------------------------}

destructor DCursor.Destroy;
begin
  Window.SceneManager.Camera.Cursor := mcStandard;
  HintLabel.Free;
  inherited Destroy;
end;

{-----------------------------------------------------------------------------}

procedure DCursor.HideOSCursor;
begin
  Window.SceneManager.Camera.Cursor := mcForceNone;
  Window.SceneManager.Camera.ExclusiveEvents := false;
  //InitInput will take care of providing cursor coordinates initialization to window center
end;

{-----------------------------------------------------------------------------}

procedure DCursor.UpdateHintLabel;
begin
  {StartProfiler}

  if FOverElement = nil then
  begin

    if FOverTransform = nil then
      LastHintText := ''
    else
    begin
      if FOverTransform is DBody then
        DBody(FOverTransform).UpdateHint;

      if LastHintText <> FOverTransform.Hint then
      begin
        HintLabel.Text := '';
        HintLabel.SetBaseSize(Round(x), Round(y), HintWidth, HintHeight, 1);
        HintLabel.Text := FOverTransform.Hint;
        LastHintText := HintLabel.Text;
      end;
      UpdateHintLabelCoordinates;
    end;
  end else
  begin
    if LastHintText <> FOverElement.Hint then
    begin
      HintLabel.Text := '';
      HintLabel.SetBaseSize(Round(x), Round(y), HintWidth, HintHeight, 1);
      HintLabel.Text := FOverElement.Hint;
      LastHintText := HintLabel.Text;
    end;
    UpdateHintLabelCoordinates;
  end;

  {StopProfiler}
end;

procedure DCursor.UpdateHintLabelCoordinates;
var
  Lx, Ly: DInt16;
begin
  Lx := Round(x);
  Ly := Round(y);

  {
  if Lx < HintGap then
    Lx := HintGap
  else
  }
  if Lx + HintLabel.RealWidth >= GUIWidth - HintGap then
    Lx := GUIWidth - HintLabel.RealWidth - HintGap;

  {
  if Ly < HintGap then
    Ly := HintGap
  else
  }
  if Ly + HintLabel.RealHeight >= GUIHeight - HintGap then
    Ly := GUIHeight - HintLabel.RealHeight - HintGap;

  HintLabel.SetBaseSize(Lx, Ly, HintWidth, HintHeight, 1, asNone);
end;

{-----------------------------------------------------------------------------}


procedure DCursor.SetOverElement(const aValue: DSingleInterfaceElement);
begin
  {StartProfiler}

  if FOverElement <> aValue then
  begin
    FOverElement := aValue;
    UpdateHintLabel;
  end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DCursor.SetOverTransform(const aValue: DTransformWorld);
begin
  {StartProfiler}

  if FOverTransform <> aValue then
  begin
    FOverTransform := aValue;
    UpdateHintLabel;
    if FOverTransform = nil then
      CurrentCursor := ctDefault
  end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DCursor.Draw;
begin
  {StartProfiler}

  if DragElement = nil then
  begin
    //Draw the cursor

    CurrentCursor := GetCurrentCursor;

    if (not ScreenShotPending) or (not HideMouseCursorInScreenshot) then //hide cursor for screenshots
      if (CurrentCursor <> ctNone) and (CursorImg[CurrentCursor].Image <> Nil) then
        CursorImg[CurrentCursor].Image.Draw(x + CursorImg[CurrentCursor].CursorShift.Data[0],
          y - CursorImg[CurrentCursor].Image.Height + CursorImg[CurrentCursor].CursorShift.Data[1]);

    // and draw hint
    if ((FOverElement <> nil) and (FOverElement.Hint <> '')) or
       ((FOverTransform <> nil) and (FOverTransform.Hint <> ''))then
    begin
      UpdateHintLabel;
      HintLabel.Draw;
    end;
  end else
    DragElement.Draw; //isn't that redundant? The element is drawn as child of GUI and here it's drawn again. Thou, on top of all other elements (which is good)

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

function DCursor.GetCurrentCursor: TCursorType; TryInline
begin
  {StartProfiler}

  if MouseLook then
    Result := ctMouseLook
  else
    Result := ctDefault;

  //Result will be overwritten by interface elements etc.

  if FOverElement <> nil then
    {dummy}
  else //actually FOverElement and FOverTransform should exclude each other, but still better safe, as Interface will be the top element to catch clicks
    if FOverTransform <> nil then
      begin
        if FOverTransform is DBody then
        begin
          if DBody(FOverTransform).BodyOwner.isPlayerAlly then
            Result := ctAlly
          else
            Result := ctEnemy;
        end else
        {if FOverTransform is DItem then
          CurrentCursor := ctGrab
        else
        if FOverTransform is DTrigger then
          case DTrigger(CurrentTransform).TriggerType of
            ttInvestiagate: CurrentCursor := ctInvestigate;
            ttUse: CurrentCursor := ctUse;
            ttUnknown: CurrentCursor := ctUnknown;
          end;
        };
      end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DCursor.SetTint;
var
  c: TCursorType;
begin
  for c in TCursorType do
    if CursorImg[c].Image <> nil then
      CursorImg[c].Image.Color := SupplementaryColor(GUITint);
  HintLabel.SetTint;
end;

end.

