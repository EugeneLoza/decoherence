{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Basically this is a sorter for DecoCharacterSpace elements
   Extracted from CurrentParty *)

{$INCLUDE compilerconfig.inc}

unit DecoPartyView;

interface

uses
  Generics.Collections,
  DecoInterfaceCore,
  DecoInterfaceArrangers, DecoCharacterSpace,
  DecoPlayer,
  DecoTime, DecoGlobal;

type
  { A set of controls to monitor and control the player characters }
  DCharacterSpaceList = specialize TObjectList<DCharacterSpace>;

type
  { Top-level control element, providing access to DCharacterSpace of every character
    in player's party. }
  DPartyView = class(DAbstractArranger)
  strict private
    { The character currently selected }
    FSelected: DCharacterSpace;
    { Set of CharacterSpace(s) }
    FCharacterSpaceList: DCharacterSpaceList;
    { Safely clear FCharacterSpaceList,
      also takes care that each DPlayerCharacter.MyCharSpace = nil }
    procedure ClearCharSpaceList;
  public
    { Get the current state of Player.CurrentParty and build PartyView to control it }
    procedure Reset;
    { Select a specific character in the current party }
    procedure Select(const aCharSpace: DcharacterSpace);
    { Deselect a character }
    procedure Deselect;
    procedure ArrangeChildren(const Animate: TAnimationStyle; const Duration: DTime); override;
  public
    constructor Create; override;
    destructor Destroy; override;
  end;

{............................................................................}
implementation
uses
  DecoGUIScale,
  DecoLog {%Profiler%};

procedure DPartyView.ClearCharSpaceList;
var
  CharSpace: DCharacterSpace;
begin
  if FCharacterSpaceList.Count > 0 then
    for CharSpace in FCharacterSpaceList do
      CharSpace.Target.MyCharSpace := nil; //clear target's reference to char space

  FCharacterSpaceList.Clear;
end;

{-----------------------------------------------------------------------------}

procedure DPartyView.Reset;
var
  i: Integer;
  YLine: DInt16;
  CharSpace: DCharacterSpace;
begin
  {StartProfiler}

  Deselect;

  ClearCharSpaceList;
  Clear(asFadeOut); //Maybe add Clear animations on switching parties?
  if (Player <> nil) and (Player.CurrentParty <> nil) then
    for i := 0 to Pred(Player.CurrentParty.FActorList.Count) do
    begin
      CharSpace := DCharacterSpace.Create;

      if Odd(i) then
        CharSpace.Alignment := iaRight
      else
        CharSpace.Alignment := iaLeft;

      CharSpace.Target := Player.CurrentParty.GetPlayerCharacter(i);
      CharSpace.Target.MyCharSpace := CharSpace;

      YLine := i div 2 + 1;
      CharSpace.SetBaseSize(0, GUIHeight - YLine * GUICharSpaceHeight, GUIUnit, GUICharSpaceHeight, 0.9, asFadeIn);

      InsertFront(CharSpace);
    end
  else
    Log(CurrentRoutine, 'ERROR: Player party is nil!');

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DPartyView.Select(const aCharSpace: DcharacterSpace);
begin
  if aCharSpace <> FSelected then
  begin
    if FSelected <> nil then
      FSelected.isSelected := false;
    FSelected := aCharSpace;
  end;
end;

{-----------------------------------------------------------------------------}

procedure DPartyView.Deselect;
begin
  if FSelected <> nil then
  begin
    FSelected.isSelected := false;
    FSelected := nil;
  end;
end;

{-----------------------------------------------------------------------------}

{$PUSH}{$WARN 5024 off} // It's ok to have unused variables here, this is a top-level element commanding others, not otherwise
procedure DPartyView.ArrangeChildren(const Animate: TAnimationStyle; const Duration: DTime);
begin
  {StartProfiler}

  //inherited ArrangeChildren(Animate, Duration); <------- Parent is abstract
  Reset;

  {StopProfiler}
end;
{$POP}

{-----------------------------------------------------------------------------}

constructor DPartyView.Create;
begin
  inherited Create;
  FullScreen;
  FCharacterSpaceList := DCharacterSpaceList.Create(False); //CharacterSpaces are owned by the DPartyView as Children
end;

{-----------------------------------------------------------------------------}

destructor DPartyView.Destroy;
begin
  ClearCharSpaceList;
  FCharacterSpaceList.Free;
  inherited Destroy;
end;


end.

