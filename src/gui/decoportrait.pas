{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* A Player character's portrait with all possible overlays and clicks *)

{$INCLUDE compilerconfig.inc}

unit DecoPortrait;

interface

uses
  DecoInterfaceCore,
  DecoFramedElement,
  DecoPortraitImage, DecoPlayerCharacter;

type
  { An intefrace element that manages everything related to the character portrait:
    the portrait image, overlays such as effects, damage labels, etc. }
  DPortrait = class(DRectagonalFramedElement)
  strict private
    FAlignment: TInterfaceElementAlignment;
    FPortraitImage: DPortraitImage;
    FTarget: DPlayerCharacter;
    procedure SetTarget(const aTarget: DPlayerCharacter);
    procedure SetAlignment(const aAlign: TInterfaceElementAlignment);
    { Select Actor }
    procedure doLeftClick;
  public
    { Is this portrait right or left?}
    property Alignment: TInterfaceElementAlignment read FAlignment write SetAlignment;
    { Character this portait is for }
    property Target: DPlayerCharacter read FTarget write SetTarget;
  public
    constructor Create; override;
  end;

{............................................................................}
implementation
uses
  DecoFrames,
  DecoCharacterSpace,
  DecoLog {%Profiler%};

procedure DPortrait.SetTarget(const aTarget: DPlayerCharacter);
begin
  if FTarget <> aTarget then
  begin
    FTarget := aTarget;
    FPortraitImage.Target := aTarget;
  end;
end;
procedure DPortrait.SetAlignment(const aAlign: TInterfaceElementAlignment);
begin
  if FAlignment <> aAlign then
  begin
    FAlignment := aAlign;
    FPortraitImage.Alignment := FAlignment;
  end;
end;

{-----------------------------------------------------------------------------}

procedure DPortrait.doLeftClick;
begin
  if Parent is DCharacterSpace then
    DCharacterSpace(Parent).isSelected := true
  else
    Log(CurrentRoutine, 'ERROR: Portrait''s Parent is not DCharacterSpace!');
end;

{-----------------------------------------------------------------------------}

constructor DPortrait.Create;
begin
  inherited Create;
  CanMouseOver := true;
  OnMouseLeftButton := @doLeftClick;

  FPortraitImage := DPortraitImage.Create;
  InsertFront(FPortraitImage);
  LoadFrame(GetFrameByName('PlayerPortraitFrame'));
end;

end.

