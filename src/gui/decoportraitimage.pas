{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Portrait and PaperDoll image as render of EmbodiedActor's body *)

{$INCLUDE compilerconfig.inc}

unit DecoPortraitImage;

interface

uses
  CastleGLImages, CastleScene, CastleSceneManager, CastleRectangles,
  DecoInterfaceCore, DecoNavigationScene,
  DecoEmbodiedActor,
  DecoTime, DecoTasks, DecoGlobal;

type
  { A generic routines required to render a DBody into a TGLImage }
  DBodyRenderer = class abstract(DSingleInterfaceElement)
  strict private
    { is this BodyRenderer ready to show image? }
    isReady: Boolean;
  strict protected
    { The image of this BodyRenderer }
    FImage: TGLImage;
    FTarget: DEmbodiedActor;
    { Last render time of this BodyRenderer }
    LastRender: DTime;
    { Cached classes that are used to render this BodyRenderer
      with set of predefined parameters for this specific render case }
    SceneManager: TCastleSceneManager;
    FLightScene: DNavigationScene;
    ViewportRect: TRectangle;
    { Requesting to render the Body }
    BodyRenderTask: DTask;
    { Procedure that actually renders the Body into FImage }
    procedure GetCameraAndLight; virtual; abstract;
    procedure RenderMe;
    procedure SetTarget(const aTarget: DEmbodiedActor); //virtual;
  public
    { Target of this BodyRenderer image
      Alignment should be set before Target if appliccable }
    property Target: DEmbodiedActor read FTarget write SetTarget;
    procedure Draw; override;
  public
    constructor Create; override;
    destructor Destroy; override;
  end;


type
  { Prtrait image automatically rendered from PlayerCharacter's body
    as DPortraitImage directly uses TGLImage it is not a child of DAbstractImage }
  DPortraitImage = class(DBodyRenderer)
  strict protected
    { if this portrait is located in the left or right of the screen }
    FAlignment: TInterfaceElementAlignment;
    { properly align camera and light relative to the Body }
    procedure GetCameraAndLight; override;
    procedure SetAlignment(const aAlign: TInterfaceElementAlignment);
  public
    { Is this portrait right or left?}
    property Alignment: TInterfaceElementAlignment read FAlignment write SetAlignment;
  end;

type
  { Paper doll to render character's outlook in inventory screen }
  DPaperDollImage = class(DBodyRenderer)
  strict protected
    procedure GetCameraAndLight; override;
  end;

{............................................................................}
implementation
uses
  CastleCameras, CastleImages, CastleVectors,
  DecoArchitect,
  DecoGUIScale,
  DecoWindow, DecoLog {%Profiler%};

procedure DBodyRenderer.RenderMe;
var
  ReactivateWorld: Boolean;
begin
  {StartProfiler}

  if (FTarget <> nil) and (FTarget.FBody.isTransformReady) then
  begin
    //Set up camera and lighting
    GetCameraAndLight;

    //remove body from the World and temporarily add it to BodyRenderer.SceneManager
    if (Architect.CurrentWorld <> nil) and (Architect.CurrentWorld.isActive) then
    begin
      ReactivateWorld := true;
      Architect.CurrentWorld.DeactivateWorld;
    end else
      ReactivateWorld := false;

    FTarget.FBody.PrepareForOfflineRender;
    SceneManager.Items.Add(FTarget.FBody);
    //Finally, render the Body image
    FImage.RenderToImageBegin(false);
    //Scenemanager.Camera.Position := Scenemanager.Camera.Position + Vector3(0,0,DRND.Random - 0.5);//test redrawing of screen
    Window.Container.RenderControl(SceneManager, ViewportRect);
    FImage.RenderToImageEnd;
    //Remove body from the BodyRenderer and return it to the World
    SceneManager.Items.Remove(FTarget.FBody);
    FTarget.FBody.ReleaseOfflineRender;

    if ReactivateWorld then
      Architect.CurrentWorld.ActivateWorld;

    isReady := true;
  end;

  LastRender := DecoNow;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DBodyRenderer.SetTarget(const aTarget: DEmbodiedActor);
begin
  {StartProfiler}

  if FTarget <> aTarget then
  begin
    FTarget := aTarget;
    SceneManager.Items.Clear;
    SceneManager.Items.Add(FLightScene);
    SceneManager.MainScene := FLightScene;
    SceneManager.Camera.Input := [];
    isReady := false;
    //SetAlignment(Self.FAlignment);
    //prevent displaying unrendered image
    RenderMe;
  end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DBodyRenderer.Draw;
begin
  {StartProfiler}

  //inherited Draw; <---------- parent is abstract

  if FTarget <> nil then
  begin
    Update;

    if Self.isVisible then
      if DecoNow - LastRender > FTarget.GetAnimationFPS then
      begin
        BodyRenderTask.StartTask(CriticalTask);
      end else
        BodyRenderTask.StartTask(Round(SmallTask * (DecoNow - LastRender) / FTarget.GetAnimationFPS));

    if isReady then
    begin
      FImage.Color[3] := Self.Alpha;
      FImage.Draw(Current.x, Current.y, Current.w, Current.h);
    end;
  end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

constructor DBodyRenderer.Create;
var
  xx: DInt16 = 0;
  yy: DInt16 = 0; //these do not matter?!
  ww: DInt16 = 1023;
  hh: DInt16 = 1023;
begin
  inherited Create;

  //physical size of portrait, taken from DecoCharacterSpace and DecoPartyView
  {ww := GUIUnit * 2;
  hh := GUICharSpaceHeight;
  //these do not work due to https://gitlab.com/EugeneLoza/decoherence/issues/1019}

  ViewportRect := Rectangle(xx, yy, xx+ww, yy+hh);
  FImage := TGlImage.Create(TRGBAlphaImage.Create(ww+1, hh+1), true, true);

  SceneManager := TCastleSceneManager.Create(nil);
  //SceneManager.FullSize := false;
  SceneManager.CustomRenderingPass := 1;
  SceneManager.Camera := TExamineCamera.Create(SceneManager);
  SceneManager.BackgroundColor := Vector4(0, 0, 0, 0);
  //SceneManager.Transparent := true; //this doesn't clear the image below (good but not for this usecase :))
  SceneManager.GLContextOpen;

  FLightScene := DNavigationScene.Create(SceneManager);
  FLightScene.Color := Vector3(1, 1, 1);
  FLightScene.Intensity := 0.7;
  FLightScene.Radius := 100;
  FLightScene.FOV := 0.5;

  BodyRenderTask := DTask.Create;
  BodyRenderTask.Task := @RenderMe;
  BodyRenderTask.DeactivateWorld := true;

  isReady := false;
end;

{-----------------------------------------------------------------------------}

destructor DBodyRenderer.Destroy;
begin
  BodyRenderTask.Free;
  FImage.RenderToImageFree;
  FImage.Free;
  SceneManager.Free;
  inherited Destroy;
end;

{============================================================================}

procedure DPortraitImage.GetCameraAndLight;
begin
  if FAlignment = iaLeft then
    FTarget.FBody.LeftFaceCamera(SceneManager.Camera)
  else
    FTarget.FBody.RightFaceCamera(SceneManager.Camera);
  FLightScene.Location := 18 * FTarget.Direction + WorldGravity;
end;

{-----------------------------------------------------------------------------}

procedure DPortraitImage.SetAlignment(const aAlign: TInterfaceElementAlignment);
begin
  FAlignment := aAlign;
end;

{============================================================================}

procedure DPaperDollImage.GetCameraAndLight;
begin
  FTarget.FBody.FullBodyCamera(SceneManager.Camera);
  FLightScene.Location := 18 * FTarget.Direction + WorldGravity;
end;

end.

