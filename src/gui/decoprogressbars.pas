{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Progressbars to display progress in game *)

{$INCLUDE compilerconfig.inc}

unit DecoProgressBars;

interface

uses
  DecoInterfaceCore, DecoInterfaceBars, DecoLabels, DecoFramedElement,
  DecoTime, DecoGlobal;

type
  { A generic progress bar in a frame with no specific usage }
  DProgressBar = class abstract(DFramedBar)
  strict private
    procedure SetMax(const aValue: DFloat);
    function GetMax: DFloat;
    procedure SetMin(const aValue: DFloat);
    function GetMin: DFloat;
    procedure SetPos(const aValue: DFloat);
    function GetPos: DFloat;
  public
    { Max value of this progressbar, default 1.0 }
    property Max: DFloat read GetMax write SetMax;
    { Min value of this progressbar, default 0.0 }
    property Min: DFloat read GetMin write SetMin;
    { Current value of this progressbar }
    property Position: DFloat read GetPos write SetPos;
  public
    constructor Create; override;
  end;

type
  { Progressbar with a percent label }
  DLabeledProgressBar = class(DProgressBar)
  strict private
    FLabel: DLabel;
  public
    procedure Update; override;
    procedure ArrangeChildren(const Animate: TAnimationStyle; const Duration: DTime); override;
  public
    constructor Create; override;
    destructor Destroy; override;
  end;

{............................................................................}
implementation
uses
  SysUtils,
  DecoImages, DecoFrames, DecoFont,
  DecoLog {%Profiler%};

constructor DProgressBar.Create;
begin
  inherited Create;
  FBar.Kind := bsHorizontal;
  LoadFrame(GetFrameByName('ProgressbarFrame'));
  FBar.Load(GetImageByName('ProgressbarImage'));
end;

{--------------------------------------------------------------------------}

procedure DProgressBar.SetMax(const aValue: DFloat);
begin
  FBar.Max := aValue;
end;
function DProgressBar.GetMax: DFloat;
begin
  Result := FBar.Max;
end;

{--------------------------------------------------------------------------}

procedure DProgressBar.SetMin(const aValue: DFloat);
begin
  FBar.Min := aValue
end;
function DProgressBar.GetMin: DFloat;
begin
  Result := FBar.Min;
end;

{--------------------------------------------------------------------------}

procedure DProgressBar.SetPos(const aValue: DFloat);
begin
  FBar.Position := aValue;
end;
function DProgressBar.GetPos: DFloat;
begin
  Result := FBar.Position;
end;

{==========================================================================}

procedure DLabeledProgressBar.Update;
begin
  inherited Update;

  FLabel.Text := IntToStr(Round(100 * FBar.Position / (FBar.Max - FBar.Min))) + '%';
  FLabel.ResetToRealSize(true); //THIS IS UGLY!
end;

{--------------------------------------------------------------------------}

procedure DLabeledProgressBar.ArrangeChildren(const Animate: TAnimationStyle; const Duration: DTime);
begin
  inherited ArrangeChildren(Animate, Duration);

  {if FLabel <> nil then
    FLabel.ResetToRealSize(true)
  else
    Log(CurrentRoutine, 'ERROR: FLabel is nil! (Means ArrangeChildren is called before Create)'); }
end;

{--------------------------------------------------------------------------}

constructor DLabeledProgressBar.Create;
begin
  inherited Create;
  FLabel := DLabel.Create;
  FLabel.Font := GetFontByName('ProgressBar');
  FLabel.ShadowIntensity := 1;
  InsertFront(FLabel);
end;

{--------------------------------------------------------------------------}

destructor DLabeledProgressBar.Destroy;
begin
  inherited Destroy;
end;

end.

