{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* An image that shades all interface elements below it
   to create a "darken" effect (for modal elements) *)

{$INCLUDE compilerconfig.inc}

unit DecoShadeImage;

interface

uses
  DecoInterfaceCore;

type
  { A simple dark rectangle to shade World and interface elements below }
  DShadeImage = class(DSingleInterfaceElement)
  public
    procedure Draw; override;
  public
    constructor Create; override;
  end;


implementation
uses
  CastleGLUtils, CastleRectangles, CastleVectors,
  DecoGUIScale, DecoLog {%Profiler%};

constructor DShadeImage.Create;
const
  ModalAlpha = 0.5;
begin
  inherited Create;
  FullScreen(ModalAlpha); //just to init Self.Alpha correctly for fade animation
end;

{-----------------------------------------------------------------------------}

procedure DShadeImage.Draw;
begin
  {StartProfiler}

  //inherited Draw; <---------- overriding parent completely

  Update;
  //using a low-level routine here
  DrawRectangle(Rectangle(0, 0, GUIWidth, GUIHeight), Vector4(0, 0, 0, Self.Alpha));

  {StopProfiler}
end;


end.

