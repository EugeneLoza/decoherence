{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Statbars used to display Actor's stats, such as HP, STA, CNC, MPH *)

{$INCLUDE compilerconfig.inc}

unit DecoStatBars;

interface

uses
  DecoInterfaceCore, DecoInterfaceBars, DecoFramedElement,
  DecoBaseActor,
  DecoTime;

type
  { An abstract stat bar with a Target. }
  DStatBar = class abstract(DFramedBar)
  strict protected
    FTarget: DBaseActor;
    procedure SetTarget(const aTarget: DBaseActor);
  public
    { This is a character for whom the stat bar is displayed. }
    property Target: DBaseActor read FTarget write SetTarget;
  end;

type
  { Framed health bar }
  DHealthBar = class(DStatBar)
  public
    procedure Update; override;
    constructor Create; override;
  end;

type
  { Framed stamina bar }
  DStaminaBar = class(DStatBar)
  public
    procedure Update; override;
    constructor Create; override;
  end;

type
  { Framed concentration bar }
  DConcentrationBar = class(DStatBar)
  public
    procedure Update; override;
    constructor Create; override;
  end;

type
  { Framed metaphysics bar }
  DMetaphysicsBar = class(DStatBar)
  public
    procedure Update; override;
    constructor Create; override;
  end;

type
  { This is an arranger for three/four stat bars of the player character.
    Automatically determines if the character is capable of metaphysics
    and displays/hides metaphysics bar accordingly. }
  DPlayerBars = class(DRectagonalFramedElement)
  strict private
    HealthBar: DHealthBar;
    StaminaBar: DStaminaBar;
    ConcentrationBar: DConcentrationBar;
    MetaphysicsBar: DMetaphysicsBar;
    FTarget: DBaseActor;
    procedure SetTarget(const aTarget: DBaseActor);
  strict protected
    procedure ArrangeChildren(const Animate: TAnimationStyle; const Duration: DTime); override;
    procedure Update; override;
    { Select this actor }
    procedure doLeftClick;
  public
    { This is a character for whom the stat bars are displayed. }
    property Target: DBaseActor read FTarget write SetTarget;
  public
    constructor Create; override;
  end;

{............................................................................}
implementation
uses
  SysUtils,
  DecoFont,
  DecoInterfaceContainer, DecoImages, DecoFrames,
  DecoCharacterSpace,
  DecoGlobal, DecoMath, DecoLog {%Profiler%};

procedure DStatBar.SetTarget(const aTarget: DBaseActor);
begin
  if FTarget <> aTarget then
  begin
    FTarget := aTarget;
    //and update something here?
  end;
end;

{===========================================================================}

constructor DHealthBar.Create;
begin
  inherited Create;
  LoadFrame(GetFrameByName('PlayerStatBarFrame'));
  FBar.Load(GetImageByName('PlayerHealthBarImage'));
  FBar.Kind := bsVertical;
end;

{---------------------------------------------------------------------------}

procedure DHealthBar.Update;
begin
  {StartProfiler}

  inherited Update;
  if FTarget <> nil then
  begin
    FBar.Min := 0;
    FBar.Max := FTarget.Hp.Value[2];
    FBar.Position := ClampZero(FTarget.Hp.Value[0], FBar.Max);
  end;

  {StopProfiler}
end;

{--------------------------------------------------------------------------}

constructor DStaminaBar.Create;
begin
  inherited Create;
  LoadFrame(GetFrameByName('PlayerStatBarFrame'));
  FBar.Load(GetImageByName('PlayerStaminaBarImage'));
  FBar.Kind := bsVertical;
end;

{---------------------------------------------------------------------------}

procedure DStaminaBar.Update;
begin
  {StartProfiler}

  inherited Update;
  if FTarget <> nil then
  begin
    FBar.Min := 0;
    FBar.Max := FTarget.Sta.Value[2];
    FBar.Position := ClampZero(FTarget.Sta.Value[0], FBar.Max);
  end;

  {StopProfiler}
end;

{--------------------------------------------------------------------------}

constructor DConcentrationBar.Create;
begin
  inherited Create;
  LoadFrame(GetFrameByName('PlayerStatBarFrame'));
  FBar.Load(GetImageByName('PlayerConcentrationBarImage'));
  FBar.Kind := bsVertical;
end;

{---------------------------------------------------------------------------}

procedure DConcentrationBar.Update;
begin
  {StartProfiler}

  inherited Update;
  if FTarget <> nil then
  begin
    FBar.Min := 0;
    FBar.Max := FTarget.Cnc.Value[2];
    FBar.Position := ClampZero(FTarget.Cnc.Value[0], FBar.Max);
  end;

  {StopProfiler}
end;

{--------------------------------------------------------------------------}

constructor DMetaphysicsBar.Create;
begin
  inherited Create;
  LoadFrame(GetFrameByName('PlayerStatBarFrame'));
  FBar.Load(GetImageByName('PlayerMetaphysicsBarImage'));
  FBar.Kind := bsVertical;
end;

{---------------------------------------------------------------------------}

procedure DMetaphysicsBar.Update;
begin
  {StartProfiler}

  inherited Update;
  if FTarget <> nil then
  begin
    FBar.Min := 0;
    FBar.Max := FTarget.Mph.Value[2];
    FBar.Position := ClampZero(FTarget.Mph.Value[0], FBar.Max);
  end;

  {StopProfiler}
end;

{==========================================================================}

procedure DPlayerBars.SetTarget(const aTarget: DBaseActor);
begin
  {StartProfiler}

  if FTarget <> aTarget then
  begin
    FTarget := aTarget;
    HealthBar.Target := FTarget;
    StaminaBar.Target := FTarget;
    ConcentrationBar.Target := FTarget;
    MetaphysicsBar.Target := FTarget;
    //ArrangeChildren?
  end;

  {StopProfiler}
end;

{--------------------------------------------------------------------------}

procedure DPlayerBars.Update;
begin
  inherited Update;
  if FTarget <> nil then
  begin
    if FTarget.isMage then
      Hint := 'HP: ' + IntToStr(Round(FTarget.HP.Value[0])) + dLineBreak +
              'STA: ' + IntToStr(Round(FTarget.STA.Value[0])) + dLineBreak +
              'CNC: ' + IntToStr(Round(FTarget.CNC.Value[0])) + dLineBreak +
              'MPH: ' + IntToStr(Round(FTarget.MPH.Value[0]))
    else
      Hint := 'HP: ' + IntToStr(Round(FTarget.HP.Value[0])) + dLineBreak +
              'STA: ' + IntToStr(Round(FTarget.STA.Value[0])) + dLineBreak +
              'CNC: ' + IntToStr(Round(FTarget.CNC.Value[0]));
  end else
    Hint := '';
end;

{--------------------------------------------------------------------------}

procedure DPlayerBars.ArrangeChildren(const Animate: TAnimationStyle; const Duration: DTime);
  function DoArrange(const ArrangeContainer: DInterfaceContainer; const XShift, WidthMod: DFloat): DInterfaceContainer;
  begin
    Result.AssignFrom(ArrangeContainer);
    Result.SetIntSize(Round(ArrangeContainer.x + ArrangeContainer.w * XShift), ArrangeContainer.y,
      Round(ArrangeContainer.w * WidthMod), ArrangeContainer.h, ArrangeContainer.a);
  end;
begin
  {StartProfiler}

  //inherited ArrangeChildren(Animate, Duration); <------- This arranger is different
  UpdateFrame(Animate, Duration);
  if (FTarget = nil) or (FTarget.isMage) then
  begin
    HealthBar.SetBaseSize(DoArrange(ToState, 0/4, 1/4), Animate, Duration);
    StaminaBar.SetBaseSize(DoArrange(ToState, 1/4, 1/4), Animate, Duration);
    ConcentrationBar.SetBaseSize(DoArrange(ToState, 2/4, 1/4), Animate, Duration);
    MetaphysicsBar.SetBaseSize(DoArrange(ToState, 3/4, 1/4), Animate, Duration);
  end else
  begin
    HealthBar.SetBaseSize(DoArrange(ToState, 0/3, 1/3), Animate, Duration);
    StaminaBar.SetBaseSize(DoArrange(ToState, 1/3, 1/3), Animate, Duration);
    ConcentrationBar.SetBaseSize(DoArrange(ToState, 2/3, 1/3), Animate, Duration);
  end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DPlayerBars.doLeftClick;
begin
  if Parent is DCharacterSpace then
    DCharacterSpace(Parent).isSelected := true
  else
    Log(CurrentRoutine, 'ERROR: Portrait''s Parent is not DCharacterSpace!');
end;

{--------------------------------------------------------------------------}

constructor DPlayerBars.Create;
begin
  inherited Create;
  HealthBar := DHealthBar.Create;
  StaminaBar := DStaminaBar.Create;
  ConcentrationBar := DConcentrationBar.Create;
  MetaphysicsBar := DMetaphysicsBar.Create;
  LoadFrame(GetFrameByName('PlayerBarsFrame'));
  InsertFront(HealthBar);
  InsertFront(StaminaBar);
  InsertFront(ConcentrationBar);
  InsertFront(MetaphysicsBar);

  CanMouseOver := true;
  onMouseLeftButton := @doLeftClick;
end;

end.

