{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* A stop screen displayed over the rendered World
   if the game needs to do some calculation-heavy tasks *)

{$INCLUDE compilerconfig.inc}

unit DecoStopScreen;

interface

uses
  DecoProgressBars, DecoFramedElement, DecoLabels,
  DecoGlobal;

type
  { Displays a frame, "Loading" text and progress bar }
  DStopScreen = class(DRectagonalFramedElement)
  strict private
    procedure SetPosition(const AValue: DFloat);
  strict protected
    { Progressbar to display progress }
    FProgressBar: DLabeledProgressBar;
    { Label with "Loading..." text }
    FLabel: DLabelImage;
  public
    { Current position (0..1) of the progressbar }
    property Position: DFloat write SetPosition;
  public
    constructor Create; override;
    //destructor Destroy; override;
  end;

{............................................................................}
implementation
uses
  DecoInterfaceCore, DecoGUIScale,
  DecoFrames, DecoFont,
  DecoLog {%Profiler%};

constructor DStopScreen.Create;
begin
  inherited Create;
  Self.SetBaseSize(GUICenterX - GUIUnit * 3, GUIUnit, GUIUnit * 6, GUIUnit);
  LoadFrame(GetFrameByName('StopScreenFrame'));
  FProgressBar := DLabeledProgressBar.Create;
  FProgressBar.SetBaseSize(GUICenterX - GUIUnit * 3 + GUILargeGap, GUIUnit + GUILargeGap,
    GUIUnit * 6 - 2 * GUILargeGap, GUIUnit2);
  InsertFront(FProgressBar);
  FLabel := DLabelImage.Create;
  FLabel.SetBaseSize(GUICenterX - GUIUnit * 3 + GUILargeGap, GUIUnit * 3 div 2 + GUILargeGap,
    GUIUnit * 6 - 2 * GUILargeGap, GUIUnit2);
  FLabel.Font := GetFontByName('StopScreenLabel');
  FLabel.ShadowIntensity := 1;
  FLabel.Text := 'Loading...';
  InsertFront(FLabel);
  FLabel.ResetToRealSize(true);
end;

{-----------------------------------------------------------------------------}

procedure DStopScreen.SetPosition(const aValue: DFloat);
begin
  FProgressBar.Position := aValue;
  if aValue >= 1.0 then
    Self.AnimateMe(asFadeOut);
end;


end.

