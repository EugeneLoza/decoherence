{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ --------------------------------------------------------------------------- }

(* Requests Window.Close
   and monitors if the CloseConfirmation is required (based on unsaved gameplay time) *)

{$INCLUDE compilerconfig.inc}

unit DecoWindowCloseConfirmation;

interface

//uses nothing;

const
  { Should CloseConfirmation be issued at all? }
  RequestCloseConfirmation = true;

{ Try close the Window }
procedure TryWindowClose;
{ Initialize Window.OnCloseQuery event }
procedure InitWindowCloseConfirmation;
{ Free Window.OnCloseQuery }
procedure FreeWindowCloseConfirmation;
{............................................................................}
implementation
uses
  CastleMessages, CastleWindow,
  DecoInterfaceText,
  DecoTime, DecoWindow, DecoLog {%Profiler%};

procedure TryWindowClose;
var
  CloseWindow: Boolean;
begin
  if RequestCloseConfirmation and SaveWasTooLongAgo then
    CloseWindow := MessageYesNo(Window, GetInterfaceText('ExitConfirmation'))
  else
    CloseWindow := true;

  if CloseWindow then
  begin
    Log(CurrentRoutine, 'Window.Close initiated');
    Window.Close;
  end;
end;

{-----------------------------------------------------------------------------}

{$PUSH}{$WARN 5024 off : Parameter "$1" not used}  // It's ok to have unused variables here as these are CGE routines and we're not using Container inside at the moment
{ Call-back for Window.OnCloseQuery }
procedure WindowCloseConfirmation(Container: TUIContainer);
begin
  Log(CurrentRoutine, 'Trying to close the Window through OS Window Manager');
  TryWindowClose;
end;
{$POP}

{-----------------------------------------------------------------------------}

procedure InitWindowCloseConfirmation;
begin
  Window.OnCloseQuery := @WindowCloseConfirmation;
end;

{-----------------------------------------------------------------------------}

procedure FreeWindowCloseConfirmation;
begin
  //so that accidentally it won't be fired during shutdown process (paranoia)
  Window.OnCloseQuery := nil;
end;

end.

