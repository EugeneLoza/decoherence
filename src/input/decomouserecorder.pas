{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Adaptive gameplay
   Records player mouse dynamics and tries to analyze it.
   At the moment, it's just gathering statisctics and averaging it. *)

{$INCLUDE compilerconfig.inc}

unit DecoMouseRecorder;

interface

uses
  CastleVectors,
  DecoTime, DecoGlobal;

//{$DEFINE WriteMouseToFile}
{$DEFINE RecordCosine}

type
  { Records mouse speed and angle }
  DMouseRecorder = class(DObject)
  strict private
    { Number of records, optimized for dynamic arrays rescaling }
    N, MaxN: DInt32;
    { Mouse speed }
    RawData: array of DFloat;
    { Last mouse position }
    LastPos: TVector2;
    {$IFDEF RecordCosine}
    { Cosines of mouse angle change }
    RawCosine: array of DFloat;
    { Last mouse movement vector (direction, non-normalized) }
    LastV: TVector2;
    { Last mouse speed }
    LastD: Single;
    {$ENDIF}
    { Analyze and save results of the recorder }
    procedure SaveResults;
    { Prepare to record a value }
    procedure IncN; TryInline
    { Reset LastPos }
    procedure ReinitMousePos;
  public
    { Called every frame,
      records current mouse speed and angle }
    procedure Manage;
  public
    constructor Create; //override;
    destructor Destroy; override;
  end;

var
  MouseRecorder: DMouseRecorder;

{ Initialize MouseRecorder }
procedure InitMouseRecorder;
{ Free MouseRecorder }
procedure FreeMouseRecorder;
{............................................................................}
implementation
uses
  SysUtils,
  DecoWindow, DecoGameMode,
  DecoLog {%Profiler%};

procedure DMouseRecorder.IncN; TryInline
begin
  inc(N);
  if N > MaxN then
  begin
    inc(MaxN, 1000);
    SetLength(RawData, MaxN);
    {$IFDEF RecordCosine}
    SetLength(RawCosine, MaxN);
    {$ENDIF}
  end;
end;

{----------------------------------------------------------------------------}

procedure DMouseRecorder.Manage;
const
  MaxDeltaT = 1; {seconds}
var
  D: Single;
  V: TVector2;
begin
  {StartProfiler}

  if (DeltaT > 0) and (DeltaT < MaxDeltaT) then
  begin
    {If GameMode in [...] then begin}
    IncN;
    V := (Window.MousePosition - LastPos) / DeltaT;
    D := V.Length; //LengthSqr

    RawData[Pred(N)] := D ;

    {$IFDEF RecordCosine}
    if (LastD > 0) and (D > 0) then
      RawCosine[Pred(N)] := 1 - (V.Data[0] * LastV.Data[0] + V.Data[1] * LastV.Data[1]) / (D * LastD)
    else
      RawCosine[Pred(N)] := 0;

    LastD := D;
    LastV := V;
    {$ENDIF}
    LastPos := Window.MousePosition;
    {end else ReinitMousePos;}
  end else
    ReinitMousePos;

  {StopProfiler}
end;

{----------------------------------------------------------------------------}

procedure DMouseRecorder.ReinitMousePos;
begin
  LastPos := Window.MousePosition;
  {$IFDEF RecordCosine}
  LastV := TVector2.Zero;
  LastD := 0;
  {$ENDIF}
end;

{----------------------------------------------------------------------------}

procedure DMouseRecorder.SaveResults;
var
  i: Integer;
  Avg, Disp, Accel: Double;
  {$IFDEF WriteMouseToFile}
  f1: Text;
  {$ENDIF}
begin
  if N > 0 then
  begin
    {$IFDEF WriteMouseToFile}
    AssignFile(f1, 'MouseData' + NiceDate + '.log');
    Rewrite(f1);
    for i := 0 to Pred(N) do
      WriteLn(f1, RawData[i]{$IFDEF RecordCosine}, RawCosine[i]{$ENDIF});
    CloseFile(f1);
    {$ENDIF}

    Avg := 0;
    for i := 0 to Pred(N) do
      Avg += RawData[i];
    Avg := Avg / N;
    Log(CurrentRoutine, 'Average mouse speed = ' + FloatToStr(Round(Avg * 100) / 100));

    Disp := 0;
    for i := 0 to Pred(N) do
      Disp += Sqr(RawData[i] - Avg);
    Disp := sqrt(Disp / N);
    Log(CurrentRoutine, 'Mouse speed dispersion = ' + FloatToStr(Round(Disp * 100) / 100));

    Accel := 0;
    for i := 1 to Pred(N) do
      Accel += Abs(RawData[i] - RawData[i-1]);
    Accel := Accel / N;
    Log(CurrentRoutine, 'Average mouse acceleration = ' + FloatToStr(Round(Accel * 100) / 100));
  end;
end;

{----------------------------------------------------------------------------}

constructor DMouseRecorder.Create;
begin
  //inherited <----------- parent is empty

  MaxN := 0;
  N := 0;
  ReinitMousePos;
end;

{----------------------------------------------------------------------------}

destructor DMouseRecorder.Destroy;
begin
  SaveResults;
  Finalize(RawData);
  {$IFDEF RecordCosine}
  Finalize(RawCosine);
  {$ENDIF}
  inherited Destroy;
end;

{............................................................................}

procedure InitMouseRecorder;
begin
  MouseRecorder := DMouseRecorder.Create;
end;

{----------------------------------------------------------------------------}

procedure FreeMouseRecorder;
begin
  MouseRecorder.Free;
end;

end.

