{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* This is a scene that has initialized navigation and a single light source
   used to provide for Player party navigation and portraits rendering *)

{$INCLUDE compilerconfig.inc}

unit DecoNavigationScene;

interface

uses
  Classes,
  X3DNodes, CastleScene, CastleVectors;

type
  { A scene with a Light and Navigation nodes }
  DNavigationScene = class(TCastleScene)
  strict private
    FLight: TPointLightNode;
    FViewpoint: TViewpointNode;
    procedure SetLocation(const aLocation: TVector3);
    function GetLocation: TVector3;
    procedure SetColor(const aColor: TVector3);
    function GetColor: TVector3;
    procedure SetAttenuation(const aAttenuation: TVector3);
    function GetAttenuation: TVector3;
    procedure SetIntensity(const aIntensity: Single);
    function GetIntensity: Single;
    procedure SetRadius(const aRadius: Single);
    function GetRadius: Single;
    procedure SetOn(const aOn: Boolean);
    function GetOn: Boolean;
    procedure SetFOV(const aFOV: Single);
    function GetFOV: Single;
  public
    { Wrappers for light and FOV management routines }
    property Location: TVector3 read GetLocation write SetLocation;
    property Color: TVector3 read GetColor write SetColor;
    property Attenuation: TVector3 read GetAttenuation write SetAttenuation;
    property Intensity: Single read GetIntensity write SetIntensity;
    property Radius: Single read GetRadius write SetRadius;
    property isOn: Boolean read GetOn write SetOn;
    property FOV: Single read GetFOV write SetFOV;
  public
    constructor Create(AOwner: TComponent); override;
  end;

{............................................................................}
implementation
uses
  DecoWindow, DecoLog {%Profiler%};

constructor DNavigationScene.Create(AOwner: TComponent);
var
  Nav: TKambiNavigationInfoNode;
  NavRoot: TX3DRootNode;
begin
  inherited Create(aOwner);

  NavRoot := TX3DRootNode.Create;

  FLight := TPointLightNode.Create;
  FLight.Color := Vector3(1, 1, 1);
  FLight.Attenuation := Vector3(1, 0, 0);
  FLight.Radius := 3;
  FLight.Intensity := 1;
  FLight.FdOn.Value := true;
  FLight.Shadows := false;
  NavRoot.FdChildren.Add(FLight);

  //and create a respective navigation node
  Nav := TKambiNavigationInfoNode.Create;
  Nav.FdHeadLightNode.Value := FLight;
  Nav.FdHeadlight.Value := false;
  NavRoot.FdChildren.Add(Nav);

  FViewpoint := TViewpointNode.Create;
  FViewpoint.FieldOfView := 1;

  NavRoot.FdChildren.Add(FViewpoint);

  Self.Spatial := [];
  Self.ProcessEvents := true;
  Self.ShadowMaps := true;
  Self.Load(NavRoot, true);
end;

{-------------------------------------------------------------------------}

procedure DNavigationScene.SetLocation(const aLocation: TVector3);
begin
  FLight.Location := aLocation;
  Self.ChangedAll;
end;
function DNavigationScene.GetLocation: TVector3;
begin
  Result := FLight.Location;
end;

procedure DNavigationScene.SetColor(const aColor: TVector3);
begin
  FLight.Color := aColor;
  Self.ChangedAll;
end;
function DNavigationScene.GetColor: TVector3;
begin
  Result := FLight.Color;
end;

procedure DNavigationScene.SetAttenuation(const aAttenuation: TVector3);
begin
  FLight.Attenuation := aAttenuation;
  Self.ChangedAll;
end;
function DNavigationScene.GetAttenuation: TVector3;
begin
  Result := FLight.Attenuation;
end;

procedure DNavigationScene.SetIntensity(const aIntensity: Single);
begin
  FLight.Intensity := aIntensity;
  Self.ChangedAll;
end;
function DNavigationScene.GetIntensity: Single;
begin
  Result := FLight.Intensity;
end;

procedure DNavigationScene.SetRadius(const aRadius: Single);
begin
  FLight.Radius := aRadius;
  Self.ChangedAll;
end;
function DNavigationScene.GetRadius: Single;
begin
  Result := FLight.Radius;
end;

procedure DNavigationScene.SetOn(const aOn: Boolean);
begin
  FLight.FdOn.Value := aOn;
  Self.ChangedAll;
end;
function DNavigationScene.GetOn: Boolean;
begin
  Result := FLight.FdOn.Value;
end;

procedure DNavigationScene.SetFOV(const aFOV: Single);
begin
  FViewpoint.FieldOfView := aFOV;
  Self.ChangedAll;
end;
function DNavigationScene.GetFOV: Single;
begin
  Result := FViewpoint.FieldOfView
end;


end.

