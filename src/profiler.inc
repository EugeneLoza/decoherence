{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Profiler is used only in GAME, this inc is automatically adjusted by
   console on/off script to turn profiler on or off. *)

{$IFDEF GAME}
{$UNDEF UseProfiler}
{$ENDIF}

{$IFDEF DEBUG}
{ Write TextureProfiler summary }
//{$DEFINE UseTextureProfiler}
{ CastleProflier calculates and logs how long it takes to load assets }
//{$DEFINE UseCastleProfiler}
{$ENDIF}


