{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ --------------------------------------------------------------------------- }

(* Global configuration of the game options *)

{$INCLUDE compilerconfig.inc}

unit DecoConfig;

interface

uses
  CastleVectors,
  DecoColors, DecoGlobal;

var
  { Should the game hide mouse cursor in the screenshots? }
  HideMouseCursorInScreenshot: Boolean = true;
  { Show FPS label }
  ShowFPS: Boolean = true;
  { The game will try to balance amount of tasks each frame, so that the frame duration
    won't go (far) above this number except some critical tasks are queued. }
  FPSGoal: DFloat;
  { Should the mouse movements be recorded and processed?
    Theoretically this saves up a bit of CPU time and RAM,
    However, disables relative adaptive gameplay features }
  UseMouseRecorder: Boolean;

  { Is the game run full-screen? }
  ConfigFullScreen: Boolean;
  { If not full screen then set Window width/height }
  ConfigWindowWidth: DInt16;
  ConfigWindowHeight: DInt16;
  { Should the game perform all the Tasks before taking a screenshot?
    This will make screenshots look a bit better, due to all objects in
    the World processed/loaded correctly }
  PreformAllTasksBeforeScreenshot: Boolean;
  { Global tint of the interface }
  GUITint: DColor;

{ Save configuration data }
procedure WriteConfiguration;
{ Read configuration data }
procedure ReadConfiguration;
{.......................................................................}
implementation
uses
  DOM, DecoFiles, DecoFolders,
  DecoLog {%Profiler%};

procedure WriteConfiguration;
var
  RootNode: TDOMElement;
begin
  RootNode := CreateFile(GameConfigFolder('Window.xml'));
  WriteBoolean(RootNode, 'FullScreen', ConfigFullScreen);
  WriteInteger(RootNode, 'Width', ConfigWindowWidth);
  WriteInteger(RootNode, 'Height', ConfigWindowHeight);
  WriteBoolean(RootNode, 'HideMouseCursorInScreenshot', HideMouseCursorInScreenshot);
  WriteBoolean(RootNode, 'PreformAllTasksBeforeScreenshot', PreformAllTasksBeforeScreenshot);
  WriteBoolean(RootNode, 'ShowFPS', ShowFPS);
  WriteBoolean(RootNode, 'UseMouseRecorder', UseMouseRecorder);
  WriteFloat(RootNode, 'FPSGoal', FPSGoal);
  WriteVector4(RootNode, 'GUITint', GUITint);
  WriteFile;
end;

{----------------------------------------------------------------------------}

procedure ReadConfiguration;
  procedure DefaultWindowConfig;
  begin
    ConfigFullScreen := false;
    ConfigWindowWidth := 1024;
    ConfigWindowHeight := 600;
    HideMouseCursorInScreenshot := true;
    PreformAllTasksBeforeScreenshot := true;
    ShowFPS := true;
    FPSGoal := 1/30;
    UseMouseRecorder := true;
    GUITint := Vector4(1, 0.95, 0.8, 1);
  end;
var
  RootNode: TDOMElement;
begin
  RootNode := StartReadFile(GameConfigFolder('Window.xml'));
  if RootNode = nil then
  begin
    DefaultWindowConfig;
    //WriteWindowConfiguration; //it's default, no need to write it
  end else
  begin
    { Read config from a file }
    ConfigFullScreen := ReadBoolean(RootNode, 'FullScreen');
    ConfigWindowWidth := ReadInteger(RootNode, 'Width');
    ConfigWindowHeight := ReadInteger(RootNode, 'Height');
    HideMouseCursorInScreenshot := ReadBoolean(RootNode, 'HideMouseCursorInScreenshot');
    PreformAllTasksBeforeScreenshot := ReadBoolean(RootNode, 'PreformAllTasksBeforeScreenshot');
    ShowFPS := ReadBoolean(RootNode, 'ShowFPS');
    FPSGoal := ReadFloat(RootNode, 'FPSGoal');
    UseMouseRecorder := ReadBoolean(RootNode, 'UseMouseRecorder');
    GUITint := ReadVector4(RootNode, 'GUITint');
    EndReadFile;
  end;
end;

end.

