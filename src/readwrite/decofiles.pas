{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Implementation of reading and writing routines and relative objects
   this is highly experimental feature for now.
   WARNING: only one file may be open at a time. So, this unit's routines are
   absolutely thread-unsafe. *)

{$INCLUDE compilerconfig.inc}

unit DecoFiles;

interface

uses
  Classes, DOM, CastleVectors,
  DecoGenerics, DecoGlobal;

type
  { This is a reference to a generic list enumerator procedure
    which should be a local/nested procedure in the writing routine

    like this:
      procedure SomeNestedProcedure(constref aParent: TDOMElement);
      var
        i: TObject1;
      begin
        for i in List do
          i.WriteMe(aParent);
      end;
  }
  TListWriterProcedure = procedure(constref aParent: TDOMElement) is nested;

  { This is a reference to a generic list reader procedure
    which should be a local/nested procedure in the reading routine
    it creates and reads the object

    like this:
      procedure SomeNestedProcedure(constref aParent: TDOMElement);
      begin
        List.Add(TObject1.Create.ReadMe(aParent));
      end;

    I also highly insist on making a wrapping function to create and
    read the whole generic list

    like this:
    function ReadGenericList(aParentBase: TDomElement): TGenericList;
    var
      GenericList: TGenericList;
      procedure SomeNestedProcedure(constref aParent: TDOMElement);
      begin
        GenericList.Add(TObject1.Create.ReadMe(aParent)); //Maybe I can use Result here?
      end;
    begin
      GenericList := TGenericList.Create;
      ReadList(aParentBase, 'ThisListName', @SomeNestedProcedure);
      Result := GenericList;
    end;
  }
  TListReaderProcedure = procedure(constref aParent: TDOMElement) is nested;

type
  { This is an abstract class with support of read and write procedures
    capable of inheritance }
  //RWObject = class abstract(DObject)
  IReadWrite = interface
   ['{8834C4DE-5CA0-4293-8A8A-C633A509B40A}']
    procedure ReadMe(constref aParent: TDOMElement);
    procedure WriteMe(constref aParent: TDOMElement);
  end;

{type
  TInterfaceLink = function: IReadWrite;}

{ Start reading or writing file }
function StartReadFile(const URL: String): TDOMElement;
function CreateFile(const URL: String): TDOMElement;
{ Finish reading or rwiting file }
procedure WriteFile;
procedure EndReadFile;

{ Pairs of read/write procedures }
procedure WriteInteger(const aParent: TDOMElement; const aName: String; const aValue: Integer);
function ReadInteger(const aParent: TDOMElement; const aName: String): Integer;
procedure WriteBoolean(const aParent: TDOMElement; const aName: String; const aValue: Boolean);
function ReadBoolean(const aParent: TDOMElement; const aName: String): Boolean;
procedure WriteFloat(const aParent: TDOMElement; const aName: String; const aValue: DFloat);
function ReadFloat(const aParent: TDOMElement; const aName: String): DFloat;
procedure WriteString(const aParent: TDOMElement; const aName: String; const aValue: String);
function ReadString(const aParent: TDOMElement; const aName: String): String;

{ Reading and writing float vectors }
procedure WriteVector2(const aParent: TDOMElement; const aName: String; const aValue: TVector2);
function ReadVector2(const aParent: TDOMElement; const aName: String): TVector2;
procedure WriteVector3(const aParent: TDOMElement; const aName: String; const aValue: TVector3);
function ReadVector3(const aParent: TDOMElement; const aName: String): TVector3;
procedure WriteVector4(const aParent: TDOMElement; const aName: String; const aValue: TVector4);
function ReadVector4(const aParent: TDOMElement; const aName: String): TVector4;
{ Reading and writing Integer vectors,
  Caution, Integer vectors are managed through a "hack"
  and therefore there is no type-checking during reading
  (i.e. reading a float vector would just round it to Integer) }
procedure WriteVector2int(const aParent: TDOMElement; const aName: String; const aValue: TVector2Integer);
function ReadVector2int(const aParent: TDOMElement; const aName: String): TVector2Integer;
procedure WriteVector3int(const aParent: TDOMElement; const aName: String; const aValue: TVector3Integer);
function ReadVector3int(const aParent: TDOMElement; const aName: String): TVector3Integer;
procedure WriteVector4int(const aParent: TDOMElement; const aName: String; const aValue: TVector4Integer);
function ReadVector4int(const aParent: TDOMElement; const aName: String): TVector4Integer;

{ Write simple predefined generic lists }
procedure WriteStringList(const aParent: TDOMElement; const aName: String; const aValue: TStringList);
function ReadStringList(const aParent: TDOMElement; const aName: String): TStringList;
procedure WriteStringDictionary(const aParent: TDOMElement; const aName: String; const aValue: DStringDictionary);
function ReadStringDictionary(const aParent: TDOMElement; const aName: String): DStringDictionary;

{ This is an ugly endeavour to automatize reading of a generic lists
  See examples of how aWriterProcedure/aReaderProcedure should look like
  Pay attention, that ReadList is a procedure, not a function,
  so the generic list and all its children should be created in host reading routines }
procedure WriteList(const aParent: TDOMElement; const aName: String; const aWriterProcedure: TListWriterProcedure);
procedure ReadList(const aParent: TDOMElement; const aName: String; const aReaderProcedure: TListReaderProcedure);
{............................................................................}
implementation
uses
  SysUtils,
  CastleXMLUtils, CastleURIUtils,
  DecoMathVectors,
  DecoHDD, DecoLog {%Profiler%};

var
  CurrentFileURL: String;
  FileOpen: Boolean = false;

  XMLDoc: TXMLDocument;

{ set the global variables and make a FileOpen check }
procedure PrepareFileOpen(const URL: String);
begin
  if FileOpen then
    Log(CurrentRoutine, 'ERROR: File ' + CurrentFileURL +
      ' was not closed properly!');
  CurrentFileURL := URL;
  FileOpen := true;
end;

{-----------------------------------------------------------------------------}

function StartReadFile(const URL: String): TDOMElement;
begin
  PrepareFileOpen(URL);
  if URIFileExists(CurrentFileURL) then
  begin
    Log(CurrentRoutine, 'Reading file ' + CurrentFileURL);
    XMLDoc := URLReadXMLSafe(CurrentFileURL);
    Result := XMLDoc.DocumentElement;
  end else
  begin
    Log(CurrentRoutine, 'WARNING: File does not exist: ' + CurrentFileURL);
    Result := nil;
    FileOpen := false;
  end;
end;

{-----------------------------------------------------------------------------}

procedure EndReadFile;
begin
  if FileOpen then
    XMLDoc.Free
  else
    Log(CurrentRoutine, 'Error: Cannot close file for read. It''s not open! ');

  FileOpen := false;
end;

{-----------------------------------------------------------------------------}

function CreateFile(const URL: String): TDOMElement;
begin
  PrepareFileOpen(URL);
  Log(CurrentRoutine, 'Creating file ' + CurrentFileURL);
  XMLDoc := TXMLDocument.Create;
  Result := XMLDoc.CreateElement('Root');
  XMLDoc.AppendChild(Result);
end;

{-----------------------------------------------------------------------------}

procedure WriteFile;
begin
  if (FileOpen) or (XMLDoc = nil) then
  begin
    URLWriteXMLSafe(XMLdoc, CurrentFileURL);
    XMLDoc.Free;
  end else
    Log(CurrentRoutine, 'Error: Cannot write file. It''s not open! ');

  FileOpen := false;
end;

{================================ READ/WRITE =================================}

procedure WriteInteger(const aParent: TDOMElement; const aName: String; const aValue: Integer);
begin
  aParent.CreateChild(aName).AttributeSet('Value', aValue);
end;
function ReadInteger(const aParent: TDOMElement; const aName: String): Integer;
begin
  try
    Result := aParent.ChildElement(aName).AttributeInteger('Value');
  except
    Log(CurrentRoutine, 'ERROR: Unable to read ' + aName + ' assuming = 0.');
    Result := 0;
  end;
end;
procedure WriteBoolean(const aParent: TDOMElement; const aName: String; const aValue: Boolean);
begin
  aParent.CreateChild(aName).AttributeSet('Value', aValue);
end;
function ReadBoolean(const aParent: TDOMElement; const aName: String): Boolean;
begin
  try
    Result := aParent.ChildElement(aName).AttributeBoolean('Value');
  except
    Log(CurrentRoutine, 'ERROR: Unable to read ' + aName + ' assuming = false.');
    Result := false;
  end;
end;
procedure WriteFloat(const aParent: TDOMElement; const aName: String; const aValue: DFloat);
begin
  aParent.CreateChild(aName).AttributeSet('Value', aValue);
end;
function ReadFloat(const aParent: TDOMElement; const aName: String): DFloat;
begin
  try
    Result := aParent.ChildElement(aName).AttributeFloat('Value');
  except
    Log(CurrentRoutine, 'ERROR: Unable to read ' + aName + ' assuming = 0.');
    Result := 0;
  end;
end;
procedure WriteString(const aParent: TDOMElement; const aName: String; const aValue: String);
begin
  aParent.CreateChild(aName).AttributeSet('Value', aValue);
end;
function ReadString(const aParent: TDOMElement; const aName: String): String;
begin
  try
    Result := aParent.ChildElement(aName).AttributeString('Value');
  except
    Log(CurrentRoutine, 'ERROR: Unable to read ' + aName + ' assuming = ".');
    Result := '';
  end;
end;

{-----------------------------------------------------------------------------}

procedure WriteVector2(const aParent: TDOMElement; const aName: String; const aValue: TVector2);
begin
  aParent.CreateChild(aName).AttributeSet('Value', aValue);
end;
function ReadVector2(const aParent: TDOMElement; const aName: String): TVector2;
begin
  try
    Result := aParent.ChildElement(aName).AttributeVector2('Value');
  except
    Log(CurrentRoutine, 'ERROR: Unable to read ' + aName + ' assuming zero vector.');
    Result := TVector2.Zero;
  end;
end;
procedure WriteVector3(const aParent: TDOMElement; const aName: String; const aValue: TVector3);
begin
  aParent.CreateChild(aName).AttributeSet('Value', aValue);
end;
function ReadVector3(const aParent: TDOMElement; const aName: String): TVector3;
begin
  try
    Result := aParent.ChildElement(aName).AttributeVector3('Value');
  except
    Log(CurrentRoutine, 'ERROR: Unable to read ' + aName + ' assuming zero vector.');
    Result := TVector3.Zero;
  end;
end;
procedure WriteVector4(const aParent: TDOMElement; const aName: String; const aValue: TVector4);
begin
  aParent.CreateChild(aName).AttributeSet('Value', aValue);
end;
function ReadVector4(const aParent: TDOMElement; const aName: String): TVector4;
begin
  try
    Result := aParent.ChildElement(aName).AttributeVector4('Value');
  except
    Log(CurrentRoutine, 'ERROR: Unable to read ' + aName + ' assuming zero vector.');
    Result := TVector4.Zero;
  end;
end;

{-----------------------------------------------------------------------------}

{ we're using a relatively clean hack to convert Integer->float->Integer
  here through DecoMathVectors as there is no support
  for directly reading/writing Integer vectors in CastleXMLUtils at the moment
  (and might be unneeded actually) }
procedure WriteVector2int(const aParent: TDOMElement; const aName: String; const aValue: TVector2Integer);
begin
  aParent.CreateChild(aName).AttributeSet('Value', VectorIntegerToFloat(aValue));
end;
function ReadVector2int(const aParent: TDOMElement; const aName: String): TVector2Integer;
begin
  try
    Result := VectorFloatToInteger(aParent.ChildElement(aName).AttributeVector2('Value'));
  except
    Log(CurrentRoutine, 'ERROR: Unable to read ' + aName + ' assuming zero vector.');
    Result := TVector2Integer.Zero;
  end;
end;
procedure WriteVector3int(const aParent: TDOMElement; const aName: String; const aValue: TVector3Integer);
begin
  aParent.CreateChild(aName).AttributeSet('Value', VectorIntegerToFloat(aValue));
end;
function ReadVector3int(const aParent: TDOMElement; const aName: String): TVector3Integer;
begin
  try
    Result := VectorFloatToInteger(aParent.ChildElement(aName).AttributeVector3('Value'));
  except
    Log(CurrentRoutine, 'ERROR: Unable to read ' + aName + ' assuming zero vector.');
    Result := TVector3Integer.Zero;
  end;
end;
procedure WriteVector4int(const aParent: TDOMElement; const aName: String; const aValue: TVector4Integer);
begin
  aParent.CreateChild(aName).AttributeSet('Value', VectorIntegerToFloat(aValue));
end;
function ReadVector4int(const aParent: TDOMElement; const aName: String): TVector4Integer;
begin
  try
    Result := VectorFloatToInteger(aParent.ChildElement(aName).AttributeVector4('Value'));
  except
    Log(CurrentRoutine, 'ERROR: Unable to read ' + aName + ' assuming zero vector.');
    Result := TVector4Integer.Zero;
  end;
end;

{-----------------------------------------------------------------------------}

procedure WriteStringList(const aParent: TDOMElement; const aName: String; const aValue: TStringList);
var
  ContainerNode: TDOMElement;
  i: Integer;
begin
  if aValue = nil then
  begin
    Log(CurrentRoutine, 'ERROR: TStringList is nil!');
    Exit;
  end;

  ContainerNode := aParent.CreateChild(aName);
  for i := 0 to Pred(aValue.Count) do
    ContainerNode.CreateChild('String_' + i.ToString).AttributeSet('Value', aValue[i]);
end;
function ReadStringList(const aParent: TDOMElement; const aName: String): TStringList;
var
  Iterator: TXMLElementIterator;
begin
  Result := TStringList.Create;
  try
    Iterator := aParent.ChildElement(aName).ChildrenIterator;
    try
      while Iterator.GetNext do
        Result.Add(Iterator.Current.AttributeString('Value'));
    finally
      FreeAndNil(Iterator);
    end;
  except
    Log(CurrentRoutine, 'ERROR: Unable to read ' + aName + ' assuming empty list.');
  end;
end;

{-----------------------------------------------------------------------------}

procedure WriteStringDictionary(const aParent: TDOMElement; const aName: String; const aValue: DStringDictionary);
var
  ContainerNode: TDOMElement;
  WorkNode: TDOMElement;
  i: Integer;
  v: String;
begin
  if aValue = nil then
  begin
    Log(CurrentRoutine, 'ERROR: DStringDictionary is nil!');
    Exit;
  end;

  ContainerNode := aParent.CreateChild(aName);
  i := 0;
  for v in aValue.keys do
  begin
    WorkNode := ContainerNode.CreateChild('String_' + i.ToString);
    WorkNode.AttributeSet('Key', v);
    WorkNode.AttributeSet('Value', GetStringByKey(aValue, v));
    inc(i);
  end;
end;
function ReadStringDictionary(const aParent: TDOMElement; const aName: String): DStringDictionary;
var
  Iterator: TXMLElementIterator;
  WorkNode: TDOMElement;
begin
  Result := DStringDictionary.Create;
  try
    Iterator := aParent.ChildElement(aName).ChildrenIterator;
    try
      while Iterator.GetNext do
      begin
        WorkNode := Iterator.Current;
        Result.Add(WorkNode.AttributeString('Key'), WorkNode.AttributeString('Value'));
      end;
    finally
      FreeAndNil(Iterator);
    end;
  except
    Log(CurrentRoutine, 'ERROR: Unable to read ' + aName + ' assuming empty list.');
  end;
end;

{-----------------------------------------------------------------------------}

procedure WriteList(const aParent: TDOMElement; const aName: String; const aWriterProcedure: TListWriterProcedure);
var
  ContainerNode: TDOMElement;
begin
  ContainerNode := aParent.CreateChild(aName);
  aWriterProcedure(ContainerNode);
end;
procedure ReadList(const aParent: TDOMElement; const aName: String; const aReaderProcedure: TListReaderProcedure);
var
  Iterator: TXMLElementIterator;
begin
  try
    Iterator := aParent.ChildElement(aName).ChildrenIterator;
    try
      while Iterator.GetNext do
        aReaderProcedure(Iterator.Current);
    finally
      FreeAndNil(Iterator);
    end;
  except
    Log(CurrentRoutine, 'ERROR: Unable to read ' + aName + '; no operations were performed.');
  end;
end;

end.

