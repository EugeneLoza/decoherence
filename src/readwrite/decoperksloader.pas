{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* {} *)

{$INCLUDE compilerconfig.inc}

unit DecoPerksLoader;

interface

uses
  DecoPerks;

var
  { All loaded perks }
  PerkDictionary: DPerkDictionary;
  { All loaded multiperks (actions) }
  MultiperkDictionary: DMultiperkDictionary;
  {}
  ActionDictionary: DActionDictionary;

{}
procedure LoadPerks;
{}
procedure LoadMultiperks;
{}
procedure LoadActions;
{............................................................................}
implementation
uses
  Generics.Collections,
  DecoImageLoader, DecoGUIScale,
  DecoInfluence,
  DecoLog {%Profiler%};

procedure LoadPerks;
  function AddPerk(PType: DPerkType; PAction: DPerkAction; PValue: DPerkValue): DPerk;
  begin
    Result := DPerk.Create;
    Result.PerkType := PType;
    Result.PerkAction := PAction;
    Result.Value := PValue;
  end;
begin
  PerkDictionary := DPerkDictionary.Create([doOwnsValues]); //owns Perks

  PerkDictionary.Add('SimpleAttack', AddPerk(ptPassive, itPhysicalDamage, 10));
end;

{-----------------------------------------------------------------------------}

procedure LoadMultiperks;
var
  aMultiperk: DMultiperk;
begin
  MultiperkDictionary := DMultiperkDictionary.Create([doOwnsValues]); //owns Multiperks

  aMultiperk := DMultiperk.Create;
  aMultiperk.Image := LoadDecoImage('GUI/Actions/crossed-swords_CC-BY_by_Lorc.png', GUIUnit, GUIUnit);
  with aMultiperk do
  begin
    TargetType := ttEnemy;
    Perks.Add(GetPerkByName('SimpleAttack'));
  end;
  MultiperkDictionary.Add('MeleeAttack', aMultiperk);
end;

{-----------------------------------------------------------------------------}

procedure LoadActions;
var
  aAction: DAction;
begin
  ActionDictionary := DActionDictionary.Create([doOwnsValues]); //owns Multiperks

  aAction := DAction.Create;
  aAction.Multiperk := GetMultiperkByName('MeleeAttack');
  ActionDictionary.Add('MeleeAttack', aAction);
end;

end.

