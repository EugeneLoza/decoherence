{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ --------------------------------------------------------------------------- }

(* URL tools *)

{$INCLUDE compilerconfig.inc}

unit DecoURL;

interface

{uses
  Classes, SysUtils;}

{ check if URL is valid }
function URLValid(const aURL: String): Boolean;
{............................................................................}
implementation
{uses
  DecoLog {%Profiler%};}

function URLValid(const aURL: String): Boolean;
begin
  Result := aURL <> '';
end;

end.

