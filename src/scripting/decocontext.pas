{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Context is a way to choose an object from a huge set of objects
   depending on multiple context variables (conditions)

   Each condition is a DContextElement
   The set of conditions is DContext
   The Result is determined as "convolution" of 2 DContext instances

   Amont the tasks there are: selecting a phrase variant based on character's
   parameters, such as gender, character, mood, health, availability of voice acting etc.,
   time of the day, location, etc. *)

{$INCLUDE compilerconfig.inc}

unit DecoContext;

interface
uses
  CastleUtils;
  //DecoGlobal

const
  { How more important are matches of DEMAND elements than matches of DENY elements }
  GoodConvolutionBonus = 100;

type
  { A result returned by convolution, including Good and Bad context
    ConvolutionResult.Good = -1 means convolution is impossible
    Caution: 32 bit Integer provides for at least 42419 convolutions.
    While it seems like pefectly enough for current needs
    it might need to be extended in Oazis }
  DConvolutionResult = record
    Good, Bad: DInt32;
  end;

type
  { All context conditions are referenced by their Name/ID and this is it }
  DContextElement = String;

type
  {<temporary>}
  DFloat = Single;
  DObject = TObject;

const
  { Constant context imporance, for easier definition }
  DEMAND = 125; {anything > 100}
  INSIST = 100;
  ALLOW = 0;
  AVOID = -100;
  DENY = -125; {anything < -100}

type
  { This is a basic context variable, determining the context element (name) and importance }
  DContextRecord = record
    { Name of this context Element }
    Element: DContextElement;
    { Importance of this context element,
      determines, whether this element ALLOWS, DENIES or DEMANDS and how important is this }
    Importance: shortint;
  end;

type
  { A list of context records }
  DContextList = specialize TStructList<DContextRecord>;

type
  { A basic containter for Context

    Example: "Good morning!"
      Demand = 'MORNING', 'GREETING'
      Allow = 'MALE','FEMALE'
      DENY = 'HOSTILE'
    means that "Good morning!" would be spoken as a *greeting* and only in the *morning*
    the character may be either male or female
    and this phrase may not be spoken by a hostile character. }
  DContext = class(DObject)
  public
    { A set of context records within this Context }
    ContextList : DContextList;
    { An easy method to find if this element contains the requrested context element
      returns it's ID in ContextList }
    function Find(const aContext: DContextElement): DInt32;
    { Add a single context element }
    procedure Add(const aElement: DContextElement; const aImportance: shortint);
    procedure Add(const aRecord: DContextRecord);
    { Construct a DContext from an array of DContextRecords }
    procedure Construct(ContextArray: Array of DContextRecord);
  public
    constructor Create;
    destructor Destroy; override;
  end;

{ The heart of the algorithm. Makes a convolution of two Contexts
  and returns an estimate of how well they match each other }
function ContextConvolution(const C1, C2: DContext): DConvolutionResult;
{ Constructs a DContextRecord from aElement and aImportance }
function cnx(const aElement: DContextElement; const aImportance: shortint): DContextRecord;
implementation
{uses
  DecoLog {%Profiler%};}

function ContextConvolution(const C1, C2: DContext): DConvolutionResult;
var
  ConvolutionResult: DConvolutionResult;
  ConvolutionPossible: Boolean;

  procedure ListConvolution(const aC1, aC2: DContext);
  var
    c: DContextRecord;
    i: Integer;
    tmp: DInt16;
    function GetContextImportance(aValue: shortint): DInt16; TryInline
    begin
      if aValue > 0 then
        Result := aValue + GoodConvolutionBonus
      else
        Result := aValue;
    end;
  begin
    for c in aC1.ContextList do
      begin
        i := aC2.Find(c.Element);
        if (i >= 0) then
        begin
          tmp := GetContextImportance(c.Importance) * GetContextImportance(aC2.ContextList[i].Importance);
          if tmp < -100 then
          begin
            ConvolutionPossible := false;
            Exit;
          end;
          if tmp > 0 then
            ConvolutionResult.Good += tmp
          else
            ConvolutionResult.Bad += tmp;
        end else
          if (c.Importance > 0) then
          begin
            if (c.Importance <= 100) then
              ConvolutionResult.Bad -= Sqr(c.Importance)
            else
              if c.Importance > 100 then
              begin
                ConvolutionPossible := false;
                Exit;
              end;
          end; // we don't care about c.Importance <= 0 as those are ALLOW or DENY
      end;
  end;

begin
  ConvolutionResult.Good := 0;
  ConvolutionResult.Bad := 0;
  ConvolutionPossible := true;

  ListConvolution(C1, C2);

  if not ConvolutionPossible then
  begin
    ConvolutionResult.Good := -1;
    Result := ConvolutionResult;
    Exit;
  end;

  ListConvolution(C2, C1);

  Result := ConvolutionResult;

  if not ConvolutionPossible then
  begin
    ConvolutionResult.Good := -1;
    Exit;
  end;
end;

{--------------------------------------------------------------------------}

function Cnx(const aElement: DContextElement; const aImportance: shortint): DContextRecord;
begin
  Result.Element := aElement;
  Result.Importance := aImportance;
end;

{==========================  Context  ===============================}

function DContext.Find(const aContext: DContextElement): DInt32;
begin
  for Result := 0 to Pred(ContextList.Count) do
    if ContextList[Result].Element = aContext then
      Exit;
  Result := -1;
end;

{--------------------------------------------------------------------------}

procedure DContext.Add(const aElement: DContextElement; const aImportance: shortint);
var
  c: DContextRecord;
begin
  if Find(aElement) < 0 then
  begin
    c.Element := aElement;
    c.Importance := aImportance;
    ContextList.Add(c);
  end else
    ; //log error
end;

{--------------------------------------------------------------------------}

procedure DContext.Add(const aRecord: DContextRecord);
begin
  if Find(aRecord.Element) < 0 then
    ContextList.Add(aRecord)
  else
    ; //log error
end;

{--------------------------------------------------------------------------}

procedure DContext.Construct(ContextArray: Array of DContextRecord);
var
  c: DContextRecord;
begin
  for c in ContextArray do
    Add(c);
end;

{--------------------------------------------------------------------------}

constructor DContext.Create;
begin
  //inherited Create;
  ContextList := DContextList.Create;
end;

{--------------------------------------------------------------------------}

destructor DContext.Destroy;
begin
  ContextList.Free;
  inherited Destroy;
end;

end.

