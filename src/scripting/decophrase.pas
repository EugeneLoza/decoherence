{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* {} *)

unit DecoPhrase;

{$INCLUDE compilerconfig.inc}

interface

uses
  Generics.Collections,
  DecoContext;

type
  { Event which requires Context to happen }
  DContextEvent = class(DObject)
  public
    { Context of this Event }
    Context: DContext;
    { If Event fired successfully, then Request is added to local level Global Context
      If Request is nil then this event sequence breaks and makes a "level-up" }
    Request: DContext;
  public
    constructor Create; //override;
    destructor Destroy; override;
  end;

type
  { Simple phrase Event (like a single phrase in a dialogue) }
  DPhrase = class(DContextEvent)
  public
    { Text content (should be parsed by script) }
    Text: String;
    { Is speaker switched after this phrase? }
    SwitchSpeaker: Boolean;
  public
    { a simple way to construct a phrase in one line }
    constructor Create(aText: String; aContext: Array of DContextRecord;
      aRequest: Array of DContextRecord; aSwitchSpeaker: Boolean); //override;
  end;

type
  { List of phrases }
  DPhraseList = specialize TObjectList<DPhrase>;

type
  { Global array of possible phrases }
  DPhraseCollector = class(DObject)
  public
    { All phrases available }
    PhraseList: DPhraseList;
    { Creates and adds a phrase to the heap }
    procedure Add(aText: String; aContext: Array of DContextRecord;
      aRequest: Array of DContextRecord; aSwitchSpeaker: Boolean);
  public
    constructor Create; virtual; //override;
    destructor Destroy; override;
  end;

{............................................................................}
implementation
uses
  SysUtils,
  DecoLog {%Profiler%};

constructor DContextEvent.Create;
begin
  //inherited <--------- Parent is empty
  Context := DContext.Create;
  Request := nil; //normally event won't request anything
end;

{----------------------------------------------------------------------------}

destructor DContextEvent.Destroy;
begin
  FreeAndNil(Context);
  FreeAndNil(Request);
  inherited Destroy;
end;

{============================================================================}

constructor DPhrase.Create(aText: String; aContext: Array of DContextRecord;
  aRequest: Array of DContextRecord; aSwitchSpeaker: Boolean);
begin
  inherited Create;

  Text := aText;
  SwitchSpeaker := aSwitchSpeaker;
  Context.Construct(aContext);
  if Length(aRequest) > 0 then
  begin
    Request := DContext.Create;
    Request.Construct(aRequest);
  end;
{  else
    Request := nil; // already done in parent }
end;

{=============================== Phrase collector ===========================}

procedure DPhraseCollector.Add(aText: String; aContext: Array of DContextRecord;
      aRequest: Array of DContextRecord; aSwitchSpeaker: Boolean);
begin
  PhraseList.Add(DPhrase.Create(aText, aContext, aRequest, aSwitchSpeaker));
end;

{----------------------------------------------------------------------------}

constructor DPhraseCollector.Create;
begin
  //inherited
  PhraseList := DPhraseList.Create(true);
end;

{----------------------------------------------------------------------------}

destructor DPhraseCollector.Destroy;
begin
  PhraseList.Free;
  inherited Destroy;
end;

end.


