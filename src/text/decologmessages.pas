{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* A container for in-game log messages
   such as result of Actors actions if they concern the player characters  *)

{$INCLUDE compilerconfig.inc}

unit DecoLogMessages;

interface

uses
  Generics.Collections,
  CastleVectors,
  DecoColors, DecoTime, DecoGlobal;

type
  { A simple message to be recorded }
  DMessage = record
    { Text of the message }
    Message: String;
    { Color of the message }
    Color: DColor;
    { Time of origin }
    TimeStamp: DTime;
  end;

type
  { List of messages (read-only) }
  DMessageList = specialize TList<DMessage>;

type
  { A container for log messages with all required routines to process them }
  DMessageContainer = class(DObject)
  strict private
    const
      MaxMessageListLength = 10;
      MaxMessageStorageTime = 60; {seconds}
  public
    { Messages in this container }
    MessageList: DMessageList;
  public
    { Called every time a message has been added/removed }
    onChanged: TSimpleProcedure;
    { Adds a message to the list, default color = clWhite }
    procedure AddMessage(const aMessage: String);
    procedure AddMessage(const aMessage: String; const aColor: DColor);
    { Manage message queue (called every frame)
      deletes old messages }
    procedure Manage;
  public
    constructor Create; //virtual; //override;
    destructor Destroy; override;
  end;

{............................................................................}
implementation
uses
  DecoLog {%Profiler%};

const
  { Default message color }
  DefaultColor: DColor = (Data: ( 1.0 , 1.0 , 1.0 , 1.0)); // = clWhite

procedure DMessageContainer.AddMessage(const aMessage: String);
begin
  Addmessage(aMessage, DefaultColor);
end;
procedure DMessageContainer.AddMessage(const aMessage: String; const aColor: DColor);
var
  M: DMessage;
begin
  {StartProfiler}

  Log(CurrentRoutine, aMessage);
  M.Message := aMessage;
  M.Color := aColor;
  M.TimeStamp := DecoNow;
  MessageList.Add(M);
  //if too many messages, then delete the first one
  if MessageList.Count > MaxMessageListLength then
    MessageList.Delete(0);
  //and call onChanged if done
  if Assigned(onChanged) then
    onChanged;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DMessageContainer.Manage;
begin
  {StartProfiler}

  while (MessageList.Count > 0) and (DecoNow - MessageList[0].TimeStamp > MaxMessageStorageTime) do
    MessageList.Delete(0);

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

constructor DMessageContainer.Create;
begin
  //inherited <--------- Parent is non-virtual
  MessageList := DMessageList.Create;
end;

{-----------------------------------------------------------------------------}

destructor DMessageContainer.Destroy;
begin
  MessageList.Free;
  inherited Destroy;
end;

end.

