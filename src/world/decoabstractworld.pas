{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Abstract world - the most general routines for the World management *)

{$INCLUDE compilerconfig.inc}

unit DecoAbstractWorld;

interface

uses
  CastleVectors, CastleRandom, CastleTransform,
  DecoTransformWorld, DecoNavigation, DecoTasks,
  DecoBaseActor, DecoActorGroup,
  DecoGlobal, DecoTime;

type
  { Most abstract routines used in all world types }
  DAbstractWorld = class abstract(DObject)
  strict protected
    { Is this the first time this world renders? }
    fIsFirstRun: Boolean;
    { If "unsafe" part of this World is active? }
    fIsActive: Boolean;
    { Size of this World's NAV array }
    FSizeX, FSizeY, FSizeZ: DInt16;
    { Array, determining blocking/releasing of the cells used by Actors }
    BlockedNav: TNavArrayBoolean;
    { A simple shortcut to get BlockedNav at an aNav }
    function GetBlockedNav(const aNav: TNav): Boolean; TryInline
    { Is this aNav located within this World size? }
    function isNavValid(const aNav: TNav): Boolean; TryInline
  public
    { Convert Nav to Translation  }
    function NavToTranslation(const aNav: TNav): TVector3; virtual; abstract;
    { Try convert Translation to Nav 
      This is a very ugly thing to do and should be avoided at any cost }
    function TranslationToNav(const aTranslation: TVector3): TNav; virtual; abstract;
    { Block aNav by Actor }
    function BlockNav(const aNav: TNav): Boolean;
    { Relase aNav (as the Actor moved to another Nav point) }
    procedure ReleaseNav(const aNav: TNav);
    { Size of this World's Nav array }
    property SizeX: DInt16 read FSizeX write FSizeX;
    property SizeY: DInt16 read FSizeY write FSizeY;
    property SizeZ: DInt16 read FSizeZ write FSizeZ;
    { Inject aObject to this World and Window.SceneManager }
    procedure AddObject(const aObject: DTransformWorld); virtual;
    { Remove aObject from this World and Window.SceneManager }
    procedure RemoveObject(const aObject: DTransformWorld); virtual;
    { Add an actor to this World }
    procedure AddActor(const aActor: DBaseActor);
    { Remove an actor from this World }
    procedure RemoveActor(const aActor: DBaseActor);
  strict protected
    { List of Actors inside this World }
    FActors: DActorList;
  strict protected
    { Internal random this World uses to build itself
      it is different from Global.DRND as it has a predefined seed }
    InternalRandom: TCastleRandom;
    fSeed: LongWord;
    procedure SetSeed(aSeed: longword);
    { Seed for building this World. It must be predefined. }
    property Seed: longword read fSeed write SetSeed;
  public
    { All objects inside this world,
      separated in "safe" - those than appear only in the world,
      and "unsafe" - which may have other ways of rendering (e.g. into portraits) }
    WorldSafeContent, WorldUnsafeContent: TCastleTransform;
    { Add this world to Window.SceneManager }
    procedure BuildWorld;
    { Activate unsafe content in this World }
    procedure ActivateWorld;
    { Deactivate unsafe content in this world }
    procedure DeactivateWorld;
    { If this World's unsafe elements are activated at the moment? }
    property isActive: Boolean read fIsActive;
    { World management routine. Called every frame;
      Most important thing it does is managing LODs of tiles/landscape
      And hiding/LODding World chunks
      x,y,z are current World coordinates of render camera }
    procedure Manage; virtual;
  strict private
    const
      { How often should the Window.SceneManager.MouseRay be updated in case Mouse isn't moving }
      UpdateMouseRayFrequency = 1; {seconds}
  strict private
    { Last time the Window.SceneManager.MouseRay was updated }
    LastMouseRayHitUpdated: DTime;
    { Was Window.SceneManager.MouseRay updated this frame? }
    MouseRayHitUpdatedThisFrame: Boolean;
    { What last DTransform was selected? }
    LastSelectedTransform: DTransformWorld;
    { Task to update Window.SceneManager.MouseRay in case Mouse isn't moving }
    UpdateMouseRayHitTask: DTask;
    { Forces update Window.SceneManager.MouseRay }
    procedure UpdateMouseRayHit;
    { Try get DTransform under mouse pointer,
      will also try to send corresponding "onMouseEnter/onMouseLeave" events }
    function GetTransformUnderMouse: DTransformWorld; TryInline
  public
    { Call this every time after Window.SceneManager.MouseRay was updated }
    procedure MouseRayHitUpdated;
    { Check which DTransform is the Mouse pointing at and raise events }
    procedure WorldMouseOver;
    { Try left-clicking a World object }
    procedure WorldMouseLeftClick;
    { Try right-clicking a World object}
    procedure WorldMouseRightClick;
  public
    constructor Create; virtual; //override;
    destructor Destroy; override;
  end;

{............................................................................}
implementation
uses
  SysUtils, CastleSceneManager, DecoGUI,
  DecoEmbodiedActor,
  DecoWindow, DecoLog {%Profiler%};

procedure DAbstractWorld.Manage;
var
  b: DBaseActor;
begin
  //grab Player.Position here;

  //manage actors
  for b in FActors do
    b.Manage;

  //update mouse events
  WorldMouseOver;
end;

{---------------------------------------------------------------------------}

function DAbstractWorld.GetBlockedNav(const aNav: TNav): Boolean; TryInline
begin
  Result := BlockedNav[aNav[0], aNav[1], aNav[2]];
end;

{---------------------------------------------------------------------------}

function DAbstractWorld.BlockNav(const aNav: TNav): Boolean;
begin
  {StartProfiler}

  if isNavValid(aNav) then
  begin
    if GetBlockedNav(aNav) then
    begin
      Result := false;
      Log(CurrentRoutine, 'Warning: Trying to block an already blocked Nav (pathfinding error)');
    end else
    begin
      //if Nav is valid and not blocked then block it
      Result := true;
      BlockedNav[aNav[0], aNav[1], aNav[2]] := true;
    end;
  end else
    Result := false;

  {StopProfiler}
end;

{---------------------------------------------------------------------------}

procedure DAbstractWorld.ReleaseNav(const aNav: TNav);
begin
  {StartProfiler}

  if isNavValid(aNav) then
  begin
    if not GetBlockedNav(aNav) then
      Log(CurrentRoutine, 'Warning: Trying to release a non-blocked Nav');

    BlockedNav[aNav[0], aNav[1], aNav[2]] := false;
  end else
    Log(CurrentRoutine, 'ERROR: Trying to operate Nav beyond the World');

  {StopProfiler}
end;

{---------------------------------------------------------------------------}

function DAbstractWorld.isNavValid(const aNav: TNav): Boolean; TryInline
begin
  {StartProfiler}

  if (aNav[0] >=0) and (aNav[0] < FSizeX) and
    (aNav[1] >=0) and (aNav[1] < FSizeY) and
    (aNav[2] >=0) and (aNav[2] < FSizeZ) then
    Result := true
  else
  begin
    Result := false;
    Log(CurrentRoutine, 'Warning: Trying to access Nav beyond the World');
  end;

  {StopProfiler}
end;

{---------------------------------------------------------------------------}

procedure DAbstractWorld.SetSeed(aSeed: longword);
begin
  if InternalRandom = nil then
    InternalRandom := TCastleRandom.Create(aSeed)
  else
    InternalRandom.Initialize(aSeed);
end;

{---------------------------------------------------------------------------}

procedure DAbstractWorld.AddObject(const aObject: DTransformWorld);
begin
  if aObject.isRenderSafe then
    WorldSafeContent.Add(aObject)
  else
    WorldUnsafeContent.Add(aObject);
end;

procedure DAbstractWorld.RemoveObject(const aObject: DTransformWorld);
begin
  if aObject.isRenderSafe then
    WorldSafeContent.Remove(aObject)
  else
    WorldUnsafeContent.Remove(aObject);
end;

{---------------------------------------------------------------------------}

procedure DAbstractWorld.AddActor(const aActor: DBaseActor);
begin
  FActors.Add(aActor);
  if aActor is DEmbodiedActor then
    AddObject(DEmbodiedActor(aActor).FBody);
end;

procedure DAbstractWorld.RemoveActor(const aActor: DBaseActor);
begin
  FActors.Extract(aActor);
  if aActor is DEmbodiedActor then
    RemoveObject(DEmbodiedActor(aActor).FBody);
end;

{---------------------------------------------------------------------------}

procedure DAbstractWorld.BuildWorld;
begin
  Window.SceneManager.Items.Add(WorldSafeContent);
  ActivateWorld;
end;

{---------------------------------------------------------------------------}

procedure DAbstractWorld.ActivateWorld;
begin
  {StartProfiler}

  if not fIsActive then
  begin
    //Log(CurrentRoutine, 'ActivateWorld');
    Window.SceneManager.Items.Add(WorldUnsafeContent);
    if fIsFirstRun then
    begin
      //add SafeContent only once per World
      Window.SceneManager.Items.Add(WorldSafeContent);
      fIsFirstRun := false;
    end;
    fIsActive := true;
  end;

  {StopProfiler}
end;

{---------------------------------------------------------------------------}

procedure DAbstractWorld.DeactivateWorld;
begin
  {StartProfiler}

  if fIsActive then
  begin
    //Log(CurrentRoutine, 'DeactivateWorld');
    Window.SceneManager.Items.Remove(WorldUnsafeContent);
    fIsActive := false;
  end;

  {StopProfiler}
end;

{===========================================================================}

procedure DAbstractWorld.UpdateMouseRayHit;
begin
  {StartProfiler}

  if not MouseRayHitUpdatedThisFrame then
  begin
    Window.SceneManager.UpdateMouseRayHit;
    MouseRayHitUpdated;
  end;

  {StopProfiler}
end;

{---------------------------------------------------------------------------}

procedure DAbstractWorld.MouseRayHitUpdated;
begin
  LastMouseRayHitUpdated := DecoNow;
  MouseRayHitUpdatedThisFrame := true;
end;

{---------------------------------------------------------------------------}

function DAbstractWorld.GetTransformUnderMouse: DTransformWorld; TryInline
var
  MouseHit: TRayCollision;
  TransformIndex: DInt16;
begin
  {StartProfiler}

  if GUI.AllowWorldMouseRay then
    MouseHit := Window.SceneManager.MouseRayHit
  else
    MouseHit := nil;

  if (MouseHit <> nil) then
  begin
    //Log(CurrentRoutine, MouseHit.ClassName);
    TransformIndex := MouseHit.IndexOfItem(DTransformWorld);
    if (TransformIndex <> -1) {$IFDEF ExactBodyCollisions}and (Window.SceneManager.MouseRayHit[0].Triangle <> nil){$ENDIF} then
      Result := MouseHit[TransformIndex].Item as DTransformWorld
    else
      Result := nil;
  end else
    Result := nil;

  //handle mouseover event
  if (Result <> LastSelectedTransform) then
  begin
    if Result <> nil then
      Result.WorldMouseEnter;
    if LastSelectedTransform <> nil then
      LastSelectedTransform.WorldMouseLeave;
  end;

  LastSelectedTransform := Result;

  {StopProfiler}
end;

{---------------------------------------------------------------------------}

procedure DAbstractWorld.WorldMouseOver;
begin
  //{StartProfiler}

  if not MouseRayHitUpdatedThisFrame then
  begin
    if DecoNow - LastMouseRayHitUpdated > UpdateMouseRayFrequency then
    begin
      UpdateMouseRayHitTask.StartTask(CriticalTask);
    end else
      UpdateMouseRayHitTask.StartTask(Round(SmallTask * (DecoNow - LastMouseRayHitUpdated) / UpdateMouseRayFrequency));
    {$IFDEF ImmediateCriticalTasks}if not MouseRayHitUpdatedThisFrame then{$ENDIF}
    Exit;
  end;

  //Let GUI.Cursor know of transform under Mouse
  GUI.Cursor.OverTransform := GetTransformUnderMouse;

  MouseRayHitUpdatedThisFrame := false;

  //{StopProfiler}
end;

{---------------------------------------------------------------------------}

procedure DAbstractWorld.WorldMouseLeftClick;
var
  T: DTransformWorld;
begin
  {StartProfiler}

  T := GetTransformUnderMouse;
  if (T <> nil) and (Assigned(T.OnMouseLeftButton)) then
    T.OnMouseLeftButton;

  {StopProfiler}
end;

{---------------------------------------------------------------------------}

procedure DAbstractWorld.WorldMouseRightClick;
var
  T: DTransformWorld;
begin
  {StartProfiler}

  T := GetTransformUnderMouse;
  if (T <> nil) and (Assigned(T.OnMouseRightButton)) then
    T.OnMouseRightButton;

  {StopProfiler}
end;

{---------------------------------------------------------------------------}

constructor DAbstractWorld.Create;
begin
  //inherited Create; <---------- Parent is empty
  WorldSafeContent := TCastleTransform.Create(nil);
  WorldUnsafeContent := TCastleTransform.Create(nil);
  UpdateMouseRayHitTask := DTask.Create;
  UpdateMouseRayHitTask.Task := @UpdateMouseRayHit;
  LastMouseRayHitUpdated := -1;
  fIsActive := false;
  fIsFirstRun := true;

  FActors := DActorList.Create(false);
end;

{---------------------------------------------------------------------------}

destructor DAbstractWorld.Destroy;
begin
  FActors.Free;
  FreeAndNil(InternalRandom);
  UpdateMouseRayHitTask.Free;
  WorldSafeContent.Free;
  WorldUnsafeContent.Free;
  inherited Destroy;
end;

end.
