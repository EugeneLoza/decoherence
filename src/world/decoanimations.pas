{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Handles definitions for animations
   and tries to request fall-back animations
   (e.g. in case the animation is not available for an Actor) *)

{$INCLUDE compilerconfig.inc}

unit DecoAnimations;

interface

uses
  DecoGlobal;

type
  { How animation is defined
    I'm almost sure it will always remain "String" because such approach
    provides for much more freedom in adding animations
    (first of all unique animations in cutscenes) }
  TAnimationType = String;
const
  { References to standard animations. As a rule, all the bodies must have those.
    In future I think we need to provide some fallback if animation is unavailable }

  //empty (default) animation for non-animated objects

  atDefault = '';

  //idle is the most important animation every model MUST have if it is not availabie - it will result in an exception

  atIdle = 'idle';

  //core animations, every model should have those

  atWalk = 'walk';
  atAttack = 'attack';
  atHurt = 'hurt';
  atDie = 'die';

  //extended animations, issued by the Actor

  atHurtCritical = 'hurt-critical';
  atHurtMinor = 'hurt-minor';
  atHealing = 'healing';
  atStandUp = 'stand-up';
  atKnockDown = 'knock-down';

  atIdleAlert = 'idle-alert';
  atIdleCombat = 'idle-combat';

type
  { What to do after animation has ended?
    aeLoop - just repeat it forever (e.g. Idle or Walk)
    aeStop - stop the animation an fix the Actor in last position (i.e. dead)
    aeIdle - switch to idle (after any action finished)
    mind that this determines automatic management of animations,
    "Walk" animation loops forever and is managed by Actor AI }
  TAnimationEnd = (aeLoop, aeStop, aeIdle);

{ Tries to use another animation instead of aAnimation
  as the last resorts falls back to atIdle }
function AnimationFallBack(const aAnimation: TAnimationType): TAnimationType;
{ Which action must follow the current animation
  Usually all the animations switch to atIdle as they're finished
  However, atDie should end in a "static" dead state
  and atIdle and atWalk just loop over until the action is finished
  In future there will be some more animations that loop (e.g. NPC chores) }
function AnimationEnd(const at: TAnimationType): TAnimationEnd;

{ Initialize and load animation fallback list }
procedure InitAnimations;
{ Free animation fallback list }
procedure FreeAnimations;
{............................................................................}
implementation
uses
  SysUtils,
  DecoGenerics,
  DecoLog {%Profiler%};

function AnimationEnd(const at: TAnimationType): TAnimationEnd;
begin
  {StartProfiler}

  case at of
    atIdle, atWalk: Result := aeLoop;
    atDie: Result := aeStop;
    else
      Result := aeIdle;
  end;

  {StopProfiler}
end;

{===========================================================================}

var
  { Dictionary determining which animations may be used as a replacement to each other }
  AnimationsFallBackList: DStringDictionary;

function AnimationFallBack(const aAnimation: TAnimationType): TAnimationType;
begin
  {StartProfiler}

  {if aAnimation = atIdle then
  begin
    Log(CurrentRoutine, 'CRITICAL: Nowhere to fall back from atIdle animation!');
    Result := '';
  end;}

  if not AnimationsFallBackList.TryGetValue(aAnimation, Result) then
    Result := atIdle;

  {StopProfiler}
end;

{............................................................................}

procedure LoadAnimationsFallBack;
begin
  //idle animation fall-back to default animation, maybe it'll be changed in future. See https://gitlab.com/EugeneLoza/decoherence/issues/1029
  AnimationsFallBackList.Add(atIdle, atDefault);

  //this are the most generic fall-backs.
  //AnimationsFallBackList.Add(atWalk, atIdle); <----- fallback to idle is automatic
  //AnimationsFallBackList.Add(atAttack, atIdle); <----- fallback to idle is automatic
  //AnimationsFallBackList.Add(atHurt, atIdle); <----- fallback to idle is automatic
  //AnimationsFallBackList.Add(atDie, atIdle); <----- fallback to idle is automatic

  //extended animations

  AnimationsFallBackList.Add(atHurtCritical, atHurt);
  AnimationsFallBackList.Add(atHurtMinor, atHurt);
  //AnimationsFallBackList.Add(atHealing, atIdle); <----- fallback to idle is automatic
  //AnimationsFallBackList.Add(atStandUp, atIdle); <----- fallback to idle is automatic
  AnimationsFallBackList.Add(atKnockDown, atDie);
  AnimationsFallBackList.Add(atIdleAlert, atIdleCombat);
  //AnimationsFallBackList.Add(atIdleCombat, atIdle); <----- fallback to idle is automatic
end;

{-----------------------------------------------------------------------------}

procedure InitAnimations;
begin
  AnimationsFallBackList := DStringDictionary.Create;
  LoadAnimationsFallBack;
end;

{-----------------------------------------------------------------------------}

procedure FreeAnimations;
begin
  AnimationsFallBackList.Free;
end;

end.

