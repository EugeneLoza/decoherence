{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Universe manager *)

{$INCLUDE compilerconfig.inc}

unit DecoArchitect;

interface

uses
  CastleVectors,
  DecoAbstractWorld,
  DecoGlobal;

const
  { Gravity direction of the World.
    At the moment it's fixed to a constant, and maybe will remain this way forever }
  WorldGravity: TVector3 = (Data: (0,  0, 1));

type
  { Operates all locations and actors in the worlds }
  DArchitect = class(DObject)
  strict private
    //...
  public
    { The World currently displayed in Game }
    CurrentWorld: DAbstractWorld;
    { Preforms Universe management routines each frame }
    procedure Manage;
  public
    destructor Destroy; override;
  end;

var
  { Manages locations, including current location }
  Architect: DArchitect;

{ Initialize Universe manager }
procedure InitArchitect;
{ Free Universe manager }
procedure FreeArchitect;
{............................................................................}
implementation
uses
  DecoLog {%Profiler%};

procedure DArchitect.Manage;
begin
  if CurrentWorld <> nil then
    CurrentWorld.Manage;
end;

{-----------------------------------------------------------------------------}

destructor DArchitect.Destroy;
begin
  { Frees the CurrentWorld if it's not nil
  However, it should be done by worlds manager and therefore will fire a warning }
  if CurrentWorld <> nil then
  begin
    Log(CurrentRoutine, 'Warning: World is not nil, freeing');
    CurrentWorld.Free;
  end;

  inherited Destroy;
end;

{............................................................................}

procedure InitArchitect;
begin
  Architect := DArchitect.Create;
end;

{-----------------------------------------------------------------------------}

procedure FreeArchitect;
begin
  Architect.Free;
end;

end.

