{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Basic body for actors that can be animated *)

{$INCLUDE compilerconfig.inc}

unit DecoBody;

interface

uses
  Classes,
  CastleVectors, CastleCameras,
  DecoBaseActor,
  DecoBodyPart,
  DecoTransformCore, DecoTransformAnimated, DecoTransformWorld, DecoAnimations,
  DecoTasks, DecoGlobal, DecoTimer, DecoTime;

type
  { Abstract API for body management.
    Body is a 3D manifestation of an Actor in the 3D world.
    First of all it manages animations of the Actor.
    T3DOrient has been chosen because it can translate/rotate the animated scene,
    however, the exact ancestor choise is not clear yet. }
  DBody = class(DTransformWorld)
  strict private
    const
      PortraitRotationAngle = 15/180*Pi;
  strict protected
    { Find a bodypart by name }
    function FindBodyPart(const aName: String): DTransformAnimated;
    { Location of Actor's face, used to align camera }
    function GlobalFace: TVector3;
    function LocalFace: TVector3;
    { How large Actor's face is, to locate the Camera correctly for portrait }
    function FaceDistance: Single;
    { How large Actor's body is, to locate the Camera correctly for paperdoll }
    function BodyDistance: Single;
  strict protected
    { Root of the Body
      temporary, should be defined by Skeleton}
    Root: DBodyPart;
    { Releases the body content and reduces refcount of the body content }
    procedure FreeBody;
  public
    { Name of this body (used to load the proper body content) }
    BodyName: String;
    { Actor who owns this Body }
    BodyOwner: DBaseActor;
    { Request body content for this body
      (tries to get the body content from a list of loaded bodies or requests a
       delayed task to load the body content from HDD)}
    procedure RequestBody;
    { Initialize aCamera with presets for portrait or PaperDoll }
    procedure LeftFaceCamera(const aCamera: TCamera);
    procedure RightFaceCamera(const aCamera: TCamera);
    procedure FullBodyCamera(const aCamera: TCamera);
  strict protected
    procedure HighlightBody;
    function StartIdleAnimation: TAnimationType; override;
  strict protected
    { A time-out counter to provide for delayed effects on this body }
    Timer: DAnimationTimer;
    { An animation postponed for some time }
    DelayedAnimationName: TAnimationType;
    { Force launch delayed animation now }
    procedure ForceDelayedAnimation;
    { AnimationDelay in seconds - is the delay before the animation starts up
      and the actual action (e.g. hit, or hurt) is performed
      Required to sync different animations so that the "action" happens simultaneously
      for both Actor who acts and the one who receives Influence }
    function AnimationDelay(const AnimationName: TAnimationType): DTime;
  public
    { Force start a new animation after a short delay
      Required when need to sync two animations
      both of which have different offset of the "main action" preceeded by some preparation time }
    procedure DelayAnimation(const AnimationName: TAnimationType; const aDelay: DTime);
    { Needs to be called every frame, updates Body's time and timer }
    procedure Manage; override;
    { Update hint displayed by this body onMouseOver }
    procedure UpdateHint;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

{............................................................................}
implementation
uses
  SysUtils,
  DecoArchitect,
  DecoLog {%Profiler%};

procedure DBody.RequestBody;
begin
  if Root = nil then
  begin
    Root := DBodyPart.Create(Self); //this is temporary and very wrong!
    Root.BodyPartAlias := BodyName;
    Root.BodyPartName := 'root';
    Self.Add(Root);
  end;
  Root.RequestBodyPart;
  FTransformReady := true;
end;

{----------------------------------------------------------------------------}

function DBody.FindBodyPart(const aName: String): DTransformAnimated;
var
  i: Integer;
  b: DBodyPart;
begin
  for i := 0 to Pred(List.Count) do
    if List[i] is DBodyPart then
    begin
      b := DBodyPart(List[i]).FindBodyPartByName(aName);
      if b <> nil then
        Exit(b);
    end;
  //if no respective bodypart found, just fall back to self
  Result := Self;
end;

{----------------------------------------------------------------------------}

function DBody.GlobalFace: TVector3;
begin
  {StartProfiler}

  Result := FindBodyPart('head').GlobalTranslation;

  {StopProfiler}
end;
function DBody.LocalFace: TVector3;
var
  FoundBodyPart: DTransformAnimated;
begin
  {StartProfiler}

  FoundBodyPart := FindBodyPart('head');
  if FoundBodyPart is DBodyPart then
    Result := DBodyPart(FoundBodyPart).LocalTranslation
  else
    Result := Vector3(0, 0, 0);

  {StopProfiler}
end;

{----------------------------------------------------------------------------}

function DBody.FaceDistance: Single;
begin
  {StartProfiler}

  Result := FindBodyPart('head').BoundingBox.MaxSize; //this should do, as scales are expected to be close to 1, but must keep this line in mind in case of bugs with genetic bodies

  {StopProfiler}
end;

{----------------------------------------------------------------------------}

function DBody.BodyDistance: Single;
begin
  {StartProfiler}

  Result := Self.BoundingBox.MaxSize;

  {StopProfiler}
end;

{----------------------------------------------------------------------------}

procedure DBody.LeftFaceCamera(const aCamera: TCamera);
var
  CameraLocation: TVector3;
begin
  {StartProfiler}
  CameraLocation := RotatePointAroundAxisRad(-PortraitRotationAngle, Self.FaceDistance * Direction, WorldGravity) - 0.5 * WorldGravity;
  aCamera.SetView(Self.GlobalFace + CameraLocation, -CameraLocation, WorldGravity, true);
  {StopProfiler}
end;
procedure DBody.RightFaceCamera(const aCamera: TCamera);
var
  CameraLocation: TVector3;
begin
  {StartProfiler}
  CameraLocation := RotatePointAroundAxisRad(+PortraitRotationAngle, Self.FaceDistance * Direction, WorldGravity){ - 0.5 * WorldGravity};
  aCamera.SetView(Self.GlobalFace + CameraLocation, -CameraLocation, WorldGravity, true);
  {StopProfiler}
end;
procedure DBody.FullBodyCamera(const aCamera: TCamera);
var
  CameraLocation: TVector3;
begin
  {StartProfiler}
  CameraLocation := Self.BoundingBox.Center + Self.BodyDistance * Direction;
  aCamera.SetView(CameraLocation, Self.BoundingBox.Center - CameraLocation, WorldGravity, true); //no need in GlobalCetner here as this one is top-level
  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DBody.HighlightBody;
begin
  {StartProfiler}

  if Self.BodyOwner.isPlayer then
    HighlightState := hsGreen
  else
  if Self.BodyOwner.isPlayerAlly then
    HighlightState := hsBlue
  else
  if Self.BodyOwner.isNeutral then
    HighlightState := hsYellow
  else
  if Self.BodyOwner.isHostile then
    HighlightState := hsRed
  else
    Log(CurrentRoutine, 'WARNING: ' +  Self.BodyName + ' body owner''s (' + Self.BodyOwner.CreatureType + ') relation to Player is undefined.');

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

function DBody.StartIdleAnimation: TAnimationType;
begin
  {StartProfiler}

  //inherited <------- no need

  case BodyOwner.AlertState of
    asRelaxed: Result := atIdle;
    asAlert: Result := atIdleAlert;
    asCombat: Result := atIdleCombat;
    else
      Result := atIdleAlert;
  end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

function DBody.AnimationDelay(const AnimationName: TAnimationType): DTime;
begin
  {StartProfiler}

  Result := 0; //should be manually defined and stored in XML, maybe, just as a body property

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DBody.ForceDelayedAnimation;
var
  i: Integer;
begin
  {StartProfiler}

  { in case we already play the delayed animation, we shouldn't change it,
    despite it is a bit off-sync, maybe will be changed in future,
    as delayed animations are time-critical to sync properly }
  if CurrentAnimationName <> DelayedAnimationName then
  begin
    for i := 0 to Pred(List.Count) do
      if List[i] is DTransformAnimated then
        DTransformAnimated(List[i]).ForceAnimation(CurrentAnimationName);
    CurrentAnimationName := DelayedAnimationName;
    ResetAnimation;
  end;

  {StopProfiler}
end;


{-----------------------------------------------------------------------------}

procedure DBody.DelayAnimation(const AnimationName: TAnimationType; const aDelay: DTime);
begin
  {StartProfiler}

  //doesn't need to pass this to children?
  DelayedAnimationName := AnimationName;
  Timer.SetTimeOut(aDelay - AnimationDelay(DelayedAnimationName));

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DBody.UpdateHint;
begin
  Hint := BodyName + ' ' + IntToStr(Round(BodyOwner.Hp.Value[0])) + ' / ' + IntToStr(Round(BodyOwner.Hp.Value[1]));
end;

{-----------------------------------------------------------------------------}

procedure DBody.Manage;
begin
  {StartProfiler}

  inherited Manage;
  Timer.Update;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DBody.FreeBody;
var
  i: Integer;
begin
  for i := 0 to Pred(List.Count) do
    if List[i] is DBodyPart then
      DBodyPart(List[i]).FreeBodyPart;
  Self.Clear;
end;

{-----------------------------------------------------------------------------}

constructor DBody.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  BodyName := '';

  Timer := DAnimationTimer.Create;
  Timer.onTimer := @ForceDelayedAnimation;

  OnMouseEnter := @HighlightBody;
  OnMouseLeave := @Unhighlight;
end;

{-----------------------------------------------------------------------------}

destructor DBody.Destroy;
begin
  FreeBody;
  Timer.Free;
  inherited Destroy;
end;

end.

