{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* A generic bodypart associated to Skeleton
   can parent model parts and clothes.
   The child components may be animated.
   Works as a hierarchy tree structure. *)

{$INCLUDE compilerconfig.inc}

unit DecoBodyPart;

interface

uses
  Classes,
  CastleVectors,
  DecoScene,
  DecoTransformCore, DecoTransformAnimated,
  DecoTasks;

type
  { Bodypart is attached to a bone and transforms its children accordingly
    Self.List contains all the children of the Bodypart
    These may be:
    DBodyPart, i.e. attached(docked) bodyparts to this bodypart
    DScene, i.e. the content of this bodypart.
    Clothes should also attach here as DScene.
    DScene may be animated, the current animation time and control is
    passed to every child recoursively.
    Skeleton may consist of just a single precalculated model (castle-anim-frames)
    Should support both MOCAP and procedural animations }
  DBodyPart = class(DTransformAnimated)
  strict protected
    { Top-most parent (expected to be DBody) of this BodyPart }
    function Parent: DTransformAnimated;
  strict protected
    { A delayed task to load the body content (or missing body part) from HDD }
    LoadTask: DDelayedTask;
    { Load body content for this bodypart
      WARNING: doesn't include clothes loading. }
    procedure LoadBodyPartContent;
    { Tries to get the body from a list of loaded bodies
      returns "true" if the bodypart content was successfully loaded and "false" otherwise}
    function TryGetBodyPart: Boolean;
  public
    { Definitive name of this bodypart in the Skeleton
      (in future, see skeleton specifications for a comprehensive list of those)
      is a part of the body name(tag), e.g. "rhand", "head", "lleg" in the Skeleton }
    BodyPartName: String;
    { How to load this bodypart (i.e. reference to a specific model)
      how to load this bodypart, e.g. "thess_lhand"
      WARNING: only body content, not clothes yet. }
    BodyPartAlias: String;
    { Recoursive search of bodypart name(tag) in children hierarchy }
    function FindBodyPartByName(const aName: String): DBodyPart;
    { Loads the body content from HDD and assigns it to this Body
      checks if the body can be obtained from a list of loaded bodies }
    procedure RequestBodyPart;
    { Frees all DScenes in this Bodypart }
    procedure FreeBodyPart;
  public
    { Transformation matrix of this BodyPart in Body local coordinate system }
    function LocalTransformationMatrix: TMatrix4; TryInline
    { Location of this BodyPart in Body local coordinate system }
    function LocalTranslation: TVector3;
    { Location of this BodyPart's center in Body local coordinate system }
    function LocalCenter: TVector3;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

{............................................................................}
implementation
uses
  DecoSceneLoader,
  DecoLog {%Profiler%};

function DBodyPart.FindBodyPartByName(const aName: String): DBodyPart;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Pred(List.Count) do
    if List[i] is DBodyPart then
    begin
      if DBodyPart(List[i]).BodyPartName = aName then
        Exit(DBodyPart(List[i]));
      Result := DBodyPart(List[i]).FindBodyPartByName(aName);
      if Result <> nil then
        Exit;
    end;
end;

{-----------------------------------------------------------------------------}

function DBodyPart.Parent: DTransformAnimated;
begin
  {StartProfiler}

  if fParent is DBodyPart then
    Result := DBodyPart(fParent).Parent
  else
  begin
    if fParent <> nil then
      Result := fParent
    else
      Result := Self;
  end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

function DBodyPart.LocalTransformationMatrix: TMatrix4; TryInline
begin
  {StartProfiler}

  if fParent is DBodyPart then
    Result := Self.Transform * DBodyPart(fParent).LocalTransformationMatrix
  else
    Result := Self.Transform;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

function DBodyPart.LocalTranslation: TVector3;
var
  LocalMatrix: TMatrix4;
begin
  {StartProfiler}

  LocalMatrix := LocalTransformationMatrix;
  Result.Init(LocalMatrix.Data[3, 0], LocalMatrix.Data[3, 1], LocalMatrix.Data[3, 2]);

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

function DBodyPart.LocalCenter: TVector3;
var
  LocalMatrix: TMatrix4;
begin
  {StartProfiler}

  LocalMatrix := LocalTransformationMatrix;
  Result.Init(LocalMatrix.Data[3, 0], LocalMatrix.Data[3, 1], LocalMatrix.Data[3, 2]);
  Result := Result + BoundingBox.Center;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DBodyPart.RequestBodyPart;
begin
  //if body is unavailable then request loading of the body
  FTransformReady := false;
  if not TryGetBodyPart then
    LoadTask.StartTask(CriticalTask);
end;

{-----------------------------------------------------------------------------}

procedure DBodyPart.LoadBodyPartContent;
begin
  //in case the body was alerady loaded by a previous Task just use it
  if TryGetBodyPart then
    Exit;

  LoadSceneByName(BodyPartAlias);

  //there should be no freeze here, as TryGetBody doesn't call or queue LoadBody task
  if not TryGetBodyPart then
    Log(CurrentRoutine, 'WARNING: Unable to load ' + BodyPartAlias + ' BodyPart for ' + Parent.ClassName); //DBody(Parent).BodyName
end;

{-----------------------------------------------------------------------------}

function DBodyPart.TryGetBodyPart: Boolean;
var
  BodyPartContent: DScene;
begin
  FreeBodyPart;
  {try get body from a dictionary}
  if TryGetSceneByName(BodyPartAlias, BodyPartContent) then
  begin
    //body is loaded and ready to use.
    Result := true;
    Self.Add(BodyPartContent);
    BodyPartContent.AddRef;
    FTransformReady := true;
  end else
    Result := false; //the body wasn't loaded this time so the task will be spawned to load it.
end;

{-----------------------------------------------------------------------------}

procedure DBodyPart.FreeBodyPart;
var
  i: Integer;
begin
  {StartProfiler}

  for i := 0 to Pred(List.Count) do
    if List[i] is DBodyPart then
      DBodyPart(List[i]).FreeBodyPart //ask all children to free recoursively
    else
    if List[i] is DScene then
      DScene(List[i]).ReleaseRef; //and free scenes content (release Ref count)

  Self.Clear;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

constructor DBodyPart.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  LoadTask := DDelayedTask.Create;
  LoadTask.Task := @LoadBodyPartContent;
  BodyPartAlias := '';
end;

{-----------------------------------------------------------------------------}

destructor DBodyPart.Destroy;
begin
  LoadTask.Free;
  FreeBodyPart;
  inherited Destroy;
end;

end.

