{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Base actor - posessing stats and location, but no body or AI *)

{$INCLUDE compilerconfig.inc}

unit DecoNavigation;

interface

uses
  CastleVectors,
  DecoGlobal;

type
  { Location of the Actor on the grid
    Vector3Integer is (x, y, z)
    for flat worlds z = 0
    Pay Attention: TNav only has sense if attached to a specific World }
  TNav = TVector3Integer;

type
  { Boolean 3D array }
  TNavArrayBoolean = array of array of array of Boolean;

{ Creates a TNavArrayBoolean of the defined size filled with "false" values }
function FreeNavArray(const SizeX, SizeY, SizeZ: DInt16): TNavArrayBoolean;
{............................................................................}
implementation
uses
  DecoLog {%Profiler%};

function FreeNavArray(const SizeX, SizeY, SizeZ: Integer): TNavArrayBoolean;
var
  ix, iy, iz: Integer;
begin
  {StartProfiler}

  SetLength(Result, SizeX);
  for ix := 0 to Pred(SizeX) do
  begin
    SetLength(Result[ix], SizeY);
    for iy := 0 to Pred(SizeY) do
    begin
      SetLength(Result[ix, iy], SizeZ);
      for iz := 0 to Pred(SizeZ) do
        Result[ix, iy, iz] := false;
    end;
  end;

  {StopProfiler}
end;


end.

