{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Indexed wrapper for TCastleScene *)

{$INCLUDE compilerconfig.inc}

unit DecoScene;

interface

uses
  Classes,
  CastleScene, CastleVectors,
  DecoAnimations,
  DecoTime, DecoGlobal;

type
  { Ref-counter interface for an object in 3D World. }
  IRefCountedObject = Interface
    ['{B108A63B-6EA9-460D-9535-111300F0DC85}']
    { Adds a ref-count for this object }
    procedure AddRef;
    { Removes a ref-count for this object
      and frees it if refcount is zero }
    procedure ReleaseRef;
  end;

type
  { Ref-counted CastleScene }
  DScene = class(TCastleScene, IRefCountedObject)
  strict private
    FRefCount: DInt16;
  public
    procedure AddRef;
    procedure ReleaseRef;
  public
    { Return the best available animation name to fit the requested animation name
      If everything fails, falls-back to atIdle='idle' }
    function DoAnimationFallBack(const AnimationName: TAnimationType): TAnimationType;
    { Cease current animation and start the requested one }
    procedure StartAnimation(const AnimationName: TAnimationType);
    { Request animation duration (will automatically fall-back if unavailable) }
    function GetAnimationDuration(const AnimationName: TAnimationType): DTime;
    { Highlight this scene }
    procedure Highlight(const aColor: TVector3);
    { Remove highlight from this scene }
    procedure UnHighlight;
  public
    { Cached name of the Scene to remove }
    SceneName: String;
  public
    constructor Create(AOwner: TComponent); override;
  end;

{............................................................................}
implementation
uses
  CastleTextureImages,
  DecoSceneLoader,
  DecoLog {%Profiler%};

function DScene.DoAnimationFallBack(const AnimationName: TAnimationType): TAnimationType;
begin
  if Self.HasAnimation(AnimationName) or (AnimationName = atDefault) then
    Result := AnimationName
  else
    Result := DoAnimationFallBack(AnimationFallBack(AnimationName));
end;

{-----------------------------------------------------------------------------}

function DScene.GetAnimationDuration(const AnimationName: TAnimationType): DTime;
begin
  Result := AnimationDuration(DoAnimationFallBack(AnimationName));
end;

{-----------------------------------------------------------------------------}

procedure DScene.StartAnimation(const AnimationName: TAnimationType);
begin
  PlayAnimation(DoAnimationFallBack(AnimationName), false, true);
  ResetTime(0); //not sure about this
end;

{-----------------------------------------------------------------------------}

procedure DScene.Highlight(const aColor: TVector3);
begin
  Attributes.WireframeEffect := weSilhouette;
  Attributes.WireframeColor := aColor;
end;

{-----------------------------------------------------------------------------}

procedure DScene.UnHighlight;
begin
  Attributes.WireframeEffect := weNormal;
end;

{-----------------------------------------------------------------------------}

constructor DScene.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Attributes.PhongShading := true;
  Attributes.MinificationFilter := minLinearMipmapLinear; //minNearestMimpapLinear?
  Attributes.MagnificationFilter := magLinear;
  Attributes.Opacity := 1.0;
  Attributes.EnableTextures := true;
end;

{============================================================================}

procedure DScene.AddRef;
begin
  inc(FRefCount);
end;
procedure DScene.ReleaseRef;
begin
  dec(FRefCount);
  if FRefCount = 0 then
    UnloadSceneByName(SceneName);
end;

end.

