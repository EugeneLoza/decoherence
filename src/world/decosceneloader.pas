{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Loads and unloads 3D models
   including animated and static,
   provides for manual refcounting of DScene *)

{$INCLUDE compilerconfig.inc}

unit DecoSceneLoader;

interface

uses
  DecoScene;

{ Tries to get a DScene by Alias
  Returns "true" if Scene is already loaded and "false" otherwise }
function TryGetSceneByName(const aName: String; out Scene: DScene): Boolean;
{ Loads a DScene by Alias from HDD
  (usually preceeded by TryGetSceneByName=false,
   i.e. only one copy of each scene may be loaded) }
procedure LoadSceneByName(const aName: String);
{ Frees memory used by DScene if it refcount = 0 }
procedure UnloadSceneByName(const aName: String);
{ Initialize SceneLoader }
procedure InitSceneLoader;
{ Free memory used by SceneLoader }
procedure FreeSceneLoader;
{............................................................................}
implementation
uses
  Generics.Collections,
  {$IFDEF ExactBodyCollisions}CastleSceneCore,{$ENDIF}
  DecoGenerics,
  DecoFolders, DecoWindow, DecoLog {%Profiler%};

type
  { List of Alias->URL for each scene }
  DSceneInfoDictionary = DURLDictionary;

type
  { List of Alias->DSCene loaded instances }
  DSceneDictionary = specialize TObjectDictionary<String, DScene>;

var
  SceneInfoDictionary: DSceneInfoDictionary;
  SceneDictionary: DSceneDictionary;

function TryGetSceneByName(const aName: String; out Scene: DScene): Boolean;
begin
  Result := SceneDictionary.TryGetValue(aName, Scene);
end;

{-----------------------------------------------------------------------------}

procedure LoadSceneByName(const aName: String);
var
  URL: String;
  Scene: DScene;
begin
  if SceneInfoDictionary.TryGetValue(aName, URL) then
  begin
    Log(CurrentRoutine, 'Loading scene: ' + aName);
    Scene := DScene.Create(nil);
    Scene.Load(GameFolder(URL));
    Scene.SceneName := aName;
    Scene.Spatial := [{$IFDEF ExactBodyCollisions}ssDynamicCollisions{$ENDIF}];
    Window.SceneManager.PrepareResources(Scene);
    SceneDictionary.Add(aName, Scene);
  end else
    Log(CurrentRoutine, 'ERROR: Failed to find URL for scene named ' + aName + '!');
end;

{-----------------------------------------------------------------------------}

procedure UnloadSceneByName(const aName: String);
var
  ExtractedScene: DScene;
begin
  if SceneDictionary.TryGetValue(aName, ExtractedScene) then
  begin
    SceneDictionary.ExtractPair(aName); //extract and discard result;
    ExtractedScene.Free;
  end else
    Log(CurrentRoutine, 'ERROR: Failed to unload scene named ' + aName + '!');
end;

{.............................................................................}

procedure LoadSceneInfo;
begin
  SceneInfoDictionary := DSceneInfoDictionary.Create;

  SceneInfoDictionary.Add('suzanne','models/tmp/suzanne.x3d');
  SceneInfoDictionary.Add('spherecrawler','models/tmp/spherecrawler.x3d');
end;

{-----------------------------------------------------------------------------}

procedure InitSceneLoader;
begin
  SceneDictionary := DSceneDictionary.Create([]); //owns nothing
  LoadSceneInfo;
end;

{-----------------------------------------------------------------------------}

procedure FreeSceneLoader;
var
  v: DScene;
begin
  SceneInfoDictionary.Free;
  for v in SceneDictionary.Values do //delete all scenes left over
    v.Free;
  SceneDictionary.Free;
end;

end.

