{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* A recoursively-animated TCastleTransform which passes its animation
   state to all children (i.e. supporting multicomponent bodies) *)

{$INCLUDE compilerconfig.inc}

unit DecoTransformAnimated;

interface

uses
  Classes,
  CastleVectors,
  DecoTransformCore, DecoAnimations,
  DecoTime, DecoTimer, DecoGlobal;

const
  //a fall-back for animation duration in case no animation is available
  DefaultAnimationDuration = 0.3;

type
  { A wrapper for TCastleTransform with additional functions regarding to easier
    animations management.
    If the content is animated, then the corresponding animation is passed to it and played.
    Animation and calls are passed to every "lower" child transform }
  DTransformAnimated = class abstract(DTransformCore)
  strict protected
    { Parent transform of this Transform }
    fParent: DTransformAnimated;
  strict protected
    { Current "time" for the scene to determine the animation frame to display }
    Time: DTime;
    FTransformReady: Boolean;
    { Move Actor's time forward }
    procedure AdvanceTime(const SecondsPassed: Single); TryInline
    { Return idle animation of the animated Transform
      should be overriden in children, if they may have a ground state <> atIdle }
    function StartIdleAnimation: TAnimationType; virtual;
    function GetTransformReady: Boolean;
  strict protected
    procedure SetHighlightState(const aValue: THighlightState); override;
  public
    { Name of current and next animation }
    CurrentAnimationName, NextAnimationName: TAnimationType;
    { Coefficient to slow the time.
      E.g. 0.9 will slow time by 10% for this Transform animations }
    SlowTimeRate: DFloat;
    { Is this Transform loaded and ready to work with? }
    property isTransformReady: Boolean read GetTransformReady;
    { How long is the current animation }
    function AnimationDuration(const AnimationName: TAnimationType): DTime;
    { Forces immediate change of the animation }
    procedure ForceAnimation(const AnimationName: TAnimationType);
    { Softly changes the animation (when the current animation cycle ends) }
    procedure AnimateTo(const AnimationName: TAnimationType);
    { Resets the current playing animation }
    procedure ResetAnimation;
    { Transformation matrix of this Transform in World coordinate system }
    function GlobalTransformationMatrix: TMatrix4; TryInline
    { Location of this Transform in World coordinate system }
    function GlobalTranslation: TVector3;
    { Location of this Transform's Center in World coordinate system }
    function GlobalCenter: TVector3;
  public
    { Some hint may be displayed onMouseOver }
    Hint: String;
    { Preforms routines for this Transform every frame
      (updates local time and manages animations change) }
    procedure Manage; virtual;
  public
    constructor Create(AOwner: TComponent); override;
  end;

{............................................................................}
implementation
uses
  TypInfo,
  CastleTransform, CastleScene, CastleQuaternions,
  DecoScene,
  DecoLog {%Profiler%};

procedure DTransformAnimated.AdvanceTime(const SecondsPassed: Single); TryInline
begin
  {StartProfiler}

  Time += SecondsPassed * SlowTimeRate;

  // manage end of the animation
  if isTransformReady and (Time > AnimationDuration(CurrentAnimationName)) then
  begin
    if NextAnimationName <> '' then
    begin
      //if "next animation" is waiting to start, when this one is over
      CurrentAnimationName := NextAnimationName;
      ResetAnimation;
      NextAnimationName := '';
    end
    else
      //otherwise do an automatic fall-back to some other animation (e.g. idle)
      case AnimationEnd(CurrentAnimationName) of
        aeLoop: Time -= AnimationDuration(CurrentAnimationName);
        aeIdle:
          begin
            CurrentAnimationName := StartIdleAnimation;
            ResetAnimation;
          end;
        aeStop: Time := AnimationDuration(CurrentAnimationName) - 0.01;  //this is not very nice
        else
          Log(CurrentRoutine, 'Unknown AnimationEnd = ' + GetEnumName(TypeInfo(TAnimationEnd), Ord(AnimationEnd(CurrentAnimationName))));
      end;
  end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

function DTransformAnimated.StartIdleAnimation: TAnimationType;
begin
  {StartProfiler}

  Result := atIdle;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

function DTransformAnimated.GetTransformReady: Boolean;
var
  i: Integer;
begin
  {StartProfiler}

  Result := FTransformReady{ and (List.Count > 0)};
  if Result then
    for i := 0 to Pred(List.Count) do
      if List[i] is DTransformAnimated then
        Result := Result and DTransformAnimated(List[i]).isTransformReady;
      {else
      if List[i] is DScene then
        Result := true;} // if the scene is available, then transform is ready

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

function DTransformAnimated.AnimationDuration(const AnimationName: TAnimationType): DTime;
var
  i: Integer;
begin
  {StartProfiler}

  Result := 0;
  for i := 0 to Pred(List.Count) do
    if List[i] is DTransformAnimated then
      Result := DTransformAnimated(List[i]).AnimationDuration(CurrentAnimationName)
    else
    if List[i] is DScene then
      Result := DScene(List[i]).GetAnimationDuration(CurrentAnimationName);

  if Result = 0 then
    Result := DefaultAnimationDuration;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DTransformAnimated.ForceAnimation(const AnimationName: TAnimationType);
var
  i: Integer;
begin
  {StartProfiler}

  for i := 0 to Pred(List.Count) do
    if List[i] is DTransformAnimated then
      DTransformAnimated(List[i]).ForceAnimation(AnimationName);
  CurrentAnimationName := AnimationName;
  ResetAnimation;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DTransformAnimated.AnimateTo(const AnimationName: TAnimationType);
var
  i: Integer;
begin
  {StartProfiler}

  NextAnimationName := AnimationName;
  for i := 0 to Pred(List.Count) do
    if List[i] is DTransformAnimated then
      DTransformAnimated(List[i]).AnimateTo(AnimationName);

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DTransformAnimated.ResetAnimation;
var
  i: Integer;
begin
  {StartProfiler}

  for i := 0 to Pred(List.Count) do
    if List[i] is DTransformAnimated then
      DTransformAnimated(List[i]).ResetAnimation
    else
    if List[i] is DScene then
      DScene(List[i]).StartAnimation(CurrentAnimationName);

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

function DTransformAnimated.GlobalTransformationMatrix: TMatrix4; TryInline
begin
  {StartProfiler}

  if fParent is DTransformAnimated then
    Result := Self.Transform * DTransformAnimated(fParent).GlobalTransformationMatrix
  else
    Result := Self.Transform;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

function DTransformAnimated.GlobalTranslation: TVector3;
var
  GlobalMatrix: TMatrix4;
begin
  {StartProfiler}

  GlobalMatrix := GlobalTransformationMatrix; // := WorldTransform;
  Result.Init(GlobalMatrix.Data[3, 0], GlobalMatrix.Data[3, 1], GlobalMatrix.Data[3, 2]);

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

function DTransformAnimated.GlobalCenter: TVector3;
var
  GlobalMatrix: TMatrix4;
begin
  {StartProfiler}

  GlobalMatrix := GlobalTransformationMatrix;
  Result.Init(GlobalMatrix.Data[3, 0], GlobalMatrix.Data[3, 1], GlobalMatrix.Data[3, 2]);
  Result := Result + BoundingBox.Center;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DTransformAnimated.Manage;
var
  i: Integer;
begin
  {StartProfiler}

  for i := 0 to Pred(List.Count) do
    if List[i] is DTransformAnimated then
      DTransformAnimated(List[i]).Manage;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DTransformAnimated.SetHighlightState(const aValue: THighlightState);
var
  i: Integer;
begin
  {StartProfiler}

  if aValue <> FHighlightState then
  begin
    FHighlightState := aValue;
    for i := 0 to Pred(List.Count) do
      if List[i] is DTransformAnimated then
        DTransformAnimated(List[i]).HighlightState := aValue;
  end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

constructor DTransformAnimated.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Time := 0; //redundant
  SlowTimeRate := 1.0;

  Hint := ''; //redundant

  FTransformReady := false;

  CurrentAnimationName := atIdle;
  ResetAnimation;
end;

end.

