{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* A wrapper for TCastleTranform
   in case of Z-up Y-forward oriented objects *)

{$INCLUDE compilerconfig.inc}

unit DecoTransformCore;

interface

uses
  CastleTransform, CastleVectors, CastleCameras,
  DecoGlobal;

const
  { The bodies must be stored in files in Z-up,Y-forward orientation }
  DefaultZupDirection: TVector3 = (Data: (0, 1, 0));

type
  { How a Transform is higlighted? }
  THighlightState = (
    { The Transform is not higlighted (default state) }
    hsNone,
    { The Transform is higlighted as a player's character (green) }
    hsGreen,
    { The Transform is higlighted as a hostile enemy (red) }
    hsRed,
    { The Transform is higlighted as a friendly ally / npc (blue) }
    hsBlue,
    { The Transform is higlighted as neutral (yellow) }
    hsYellow,
    { ... }
    hsCyan,
    { ... }
    hsPurple,
    { Interactible objects are higlighted with white color }
    hsWhite);

type
  { A wrapper for TCastleTransform Translation, Rotation and Direction
    which operate in Z-up,Y-forward orientation,
    automatically adjusted according to WorldGravity and DefaultZupDirection }
  DTransformCore = class abstract(TCastleTransform)
  strict private
    procedure SetZupTranslation(const aTranslation: TVector3);
    function GetZupTranslation: TVector3;
    procedure SetZupDirection(const aDirection: TVector3);
    function GetZupDirection: TVector3;
    procedure SetZupRotation(const aRotation: TVector4);
    function GetZupRotation: TVector4;
  public
    { Exact current of this object in 3D world }
    property Translation: TVector3 read GetZupTranslation write SetZupTranslation;
    { Exact current of this object in 3D world }
    property Direction: TVector3 read GetZupDirection write SetZupDirection;
    { Exact rotation of this object in 3D world }
    property Rotation: TVector4 read GetZupRotation write SetZupRotation;
  strict protected
    FHighlightState: THighlightState;
    { Return a TVector3 color for a THighlightState }
    function GetHighlightColor(const aHighlight: THighlightState): TVector3;
    { Set highlight for the Transform }
    procedure SetHighlightState(const aValue: THighlightState); virtual; abstract;
    { Higlight this Transform
      At this point only top-level transforms implement these,
      e.g. in Body, but not in BodyPart }
    //procedure Highlight; virtual; abstract;
    { Unhighlight this Transform }
    procedure Unhighlight;
  protected //not strict
    procedure LocalRender(const Params: TRenderParams); override;
  public
    { How this Transform is higlighted }
    property HighlightState: THighlightState read FHighlightState write SetHighlightState default hsNone;
  end;

{............................................................................}
implementation
uses
  TypInfo,
  DecoColors, DecoScene,
  DecoArchitect, DecoLog {%Profiler%};

procedure DTransformCore.SetZupTranslation(const aTranslation: TVector3);
begin
  inherited Translation := aTranslation;
end;
function DTransformCore.GetZupTranslation: TVector3;
begin
  Result := inherited Translation;
end;

{-----------------------------------------------------------------------------}

procedure DTransformCore.SetZupDirection(const aDirection: TVector3);
begin
  inherited Rotation := OrientationFromDirectionUp(aDirection, WorldGravity);
end;
function DTransformCore.GetZupDirection: TVector3;
begin
  Result := RotatePointAroundAxis(GetZupRotation, DefaultZupDirection);
end;

{-----------------------------------------------------------------------------}

procedure DTransformCore.SetZupRotation(const aRotation: TVector4);
begin
  inherited Rotation := aRotation;
end;
function DTransformCore.GetZupRotation: TVector4;
begin
  Result := inherited Rotation;
end;

{-----------------------------------------------------------------------------}

procedure DTransformCore.Unhighlight;
begin
  {StartProfiler}

  HighlightState := hsNone;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

function DTransformCore.GetHighlightColor(const aHighlight: THighlightState): TVector3;
begin
  {StartProfiler}

  case aHighlight of
    hsGreen: Result := rgbLime;
    hsRed: Result := rgbRed;
    hsBlue: Result := rgbBlue;
    hsYellow: Result := rgbYellow;
    hsCyan: Result := rgbAqua;
    hsPurple: Result := rgbFuchsia;
    hsWhite: Result := rgbWhite;
    else
    begin
      Result := rgbWhite;
      Log(CurrentRoutine, 'Unknown HighlightState = ' + GetEnumName(TypeInfo(THighlightState), Ord(aHighlight)));
    end;
  end;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DTransformCore.LocalRender(const Params: TRenderParams);
var
  i: Integer;
begin
  { we highlight and unhighlight scene in case it's needed because
    highlighting is a rare case, and most other scenes are unhighlighted,
    so in general it should speed up things in case of reused models }
  if FHighlightState <> hsNone then
  begin
    for i := 0 to Pred(List.Count) do
      if List[i] is DScene then
        DScene(List[i]).Highlight(GetHighlightColor(FHighlightState));

    inherited LocalRender(Params);

    for i := 0 to Pred(List.Count) do
      if List[i] is DScene then
        DScene(List[i]).UnHighlight;
  end else
    inherited LocalRender(Params); //just to make one less if-check
end;

end.

