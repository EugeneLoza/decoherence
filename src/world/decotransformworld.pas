{ Copyright (C) 2012-2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* An object ready to be added to the World,
   Supports mirror-only rendering and mouse events *)

{$INCLUDE compilerconfig.inc}

unit DecoTransformWorld;

interface

uses
  Classes,
  CastleTransform,
  DecoTransformCore, DecoTransformAnimated,
  DecoGlobal;

type
  { Is this object is rendered relative to mirrors/World }
  TMirrorRender = (
    { Render the object both in the World and in Mirrors }
    mrAll,
    { Render the object only in Mirrors (i.e. a Player Character or a ghost) }
    mrMirror,
    { Render the object only in World, but not in Mirrors
      (i.e. a Vampire sort of thing :) ) }
    mrWorld);

type
  { A wrapper for TCastleTransform ready to be added to the World
    Supports MirrorRender, OfflineRender and Mouse events }
  DTransformWorld = class abstract(DTransformAnimated)
  strict private
    FMirrorRender: TMirrorRender;
    procedure SetMirrorRender(const aMirrorRender: TMirrorRender);
  public
    { Is this Transform located only in one SceneManager, or can it be located in several?
      World sorts such objects into Safe and Unsafe content,
      Unsafe content should be disabled before rendering something in SceneManager
      different than Window.SceneManager }
    isRenderSafe: Boolean;
    { Is this object is rendered in World/Mirror }
    property MirrorRender: TMirrorRender read FMirrorRender write SetMirrorRender default mrAll;
  protected //not strict
    procedure LocalRender(const Params: TRenderParams); override;
  strict private
    { A stored MirrorRender state (for OfflineRender) }
    MirrorRenderMemory: TMirrorRender;
    { A stored HighlightState state (for OfflineRender) }
    HighlightStateMemory: THighlightState;
  public
    { Prepare Transform for offline rendering }
    procedure PrepareForOfflineRender;
    { Return Transform to normal rendering }
    procedure ReleaseOfflineRender;
  public
    { If mouse is over this transform }
    isMouseOver: Boolean;
    { Mouse/touch Events }
    OnMouseEnter: TSimpleProcedure;
    OnMouseLeave: TSimpleProcedure;
    OnMouseLeftButton: TSimpleProcedure;
    OnMouseRightButton: TSimpleProcedure;
    { Callback for mouse pointer entering over this object }
    procedure WorldMouseEnter;
    { Callback for mouse pointer leaving this object}
    procedure WorldMouseLeave;
  public
    constructor Create(AOwner: TComponent); override;
  end;

{............................................................................}
implementation
uses
  DecoLog {%Profiler%};

procedure DTransformWorld.SetMirrorRender(const aMirrorRender: TMirrorRender);
begin
  {StartProfiler}

  FMirrorRender := aMirrorRender;
  if aMirrorRender = mrMirror then
    Self.Pickable := false //disable mouse events on object
  else
    Self.Pickable := true; //enable mouse events on object

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DTransformWorld.LocalRender(const Params: TRenderParams);
begin
  //render target in the World
  if (FMirrorRender = mrAll) or ((FMirrorRender = mrWorld) and (Params.RenderingCamera.Target = rtScreen)) then
    inherited LocalRender(Params)
  else
    //render target in the Mirror
    if ((FMirrorRender = mrMirror) and (Params.RenderingCamera.Target = rfRenderedTexture)) then
      inherited LocalRender(Params);
end;

{-----------------------------------------------------------------------------}

procedure DTransformWorld.WorldMouseEnter;
begin
  {StartProfiler}

  isMouseOver := true;
  if Assigned(onMouseEnter) then
    onMouseEnter;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DTransformWorld.WorldMouseLeave;
begin
  {StartProfiler}

  isMouseOver := false;
  if Assigned(onMouseLeave) then
    onMouseLeave;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

constructor DTransformWorld.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  isRenderSafe := true; //normal objects don't use unsafe render
  MirrorRender := mrAll;
end;


{-----------------------------------------------------------------------------}

procedure DTransformWorld.PrepareForOfflineRender;
begin
  {StartProfiler}

  MirrorRenderMemory := MirrorRender;
  MirrorRender := mrAll; //we may use mrWorld here, but mrAll is a tiny bit more efficient as it skips other checks
  HighlightStateMemory := HighlightState;
  HighlightState := hsNone;

  {StopProfiler}
end;

{-----------------------------------------------------------------------------}

procedure DTransformWorld.ReleaseOfflineRender;
begin
  {StartProfiler}

  MirrorRender := MirrorRenderMemory;
  HighlightState := HighlightStateMemory;

  {StopProfiler}
end;

end.

