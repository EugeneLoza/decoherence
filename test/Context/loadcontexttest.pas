unit LoadContextTest;

{$mode objfpc}{$H+}

interface

uses
  DecoPhrase, DecoContext;

var
  All: DPhraseCollector;

procedure LoadPhrases;
implementation
uses
  SysUtils;

procedure LoadPhrases;
begin
  All := DPhraseCollector.Create;
  with All do
  begin
    {$INCLUDE phrases.inc}
  end;
end;


finalization
  FreeAndNil(All);

end.

