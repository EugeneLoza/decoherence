//greeting

Add('Hi!', [cnx('greeting', DEMAND)], [cnx('greeting-response', DEMAND)], true);
Add('Hello!', [cnx('greeting', DEMAND)], [cnx('greeting-response', DEMAND)], true);
Add('Greetings!', [cnx('greeting', DEMAND)], [cnx('greeting-response', DEMAND)], true);
Add('Peace to you!', [cnx('greeting', DEMAND)], [cnx('greeting-response', DEMAND)], true);
Add('Blessings upon you!', [cnx('greeting', DEMAND)], [cnx('greeting-response', DEMAND)], true);
Add('Could you spare me a moment?', [cnx('greeting', DEMAND)], [cnx('greeting-response', DEMAND)], true);
Add('Can I have a word with you?', [cnx('greeting', DEMAND)], [cnx('greeting-response', DEMAND)], true);

Add('Hi, %name%!', [cnx('greeting', DEMAND), cnx('knowname', DEMAND)], [cnx('greeting-response', DEMAND)], true);
Add('Hello, %name%!', [cnx('greeting', DEMAND), cnx('knowname', DEMAND)], [cnx('greeting-response', DEMAND)], true);

Add('Hi, friend!', [cnx('greeting', DEMAND), cnx('friend', DEMAND)], [cnx('greeting-response', DEMAND)], true);
Add('Hello, friend!', [cnx('greeting', DEMAND), cnx('friend', DEMAND)], [cnx('greeting-response', DEMAND)], true);
Add('Howdy!', [cnx('greeting', DEMAND), cnx('friend', DEMAND)], [cnx('greeting-response', DEMAND)], true);

Add('Hey, %name%!', [cnx('greeting', DEMAND), cnx('knowname', DEMAND), cnx('friend', DEMAND)], [cnx('greeting-response', DEMAND)], true);

Add('Good morning!', [cnx('greeting', DEMAND), cnx('morning', DEMAND)], [cnx('greeting-response', DEMAND)], true);
Add('Good afternoon!', [cnx('greeting', DEMAND), cnx('day', DEMAND)], [cnx('greeting-response', DEMAND)], true);
Add('Good evening!', [cnx('greeting', DEMAND), cnx('evening', DEMAND)], [cnx('greeting-response', DEMAND)], true);
Add('Good night!', [cnx('greeting', DEMAND), cnx('night', DEMAND)], [cnx('greeting-response', DEMAND)], true);
Add('Good morning, %name%!', [cnx('greeting', DEMAND), cnx('morning', DEMAND), cnx('knowname', DEMAND)], [cnx('greeting-response', DEMAND)], true);
Add('Good day, %name%!', [cnx('greeting', DEMAND), cnx('day', DEMAND), cnx('knowname', DEMAND)], [cnx('greeting-response', DEMAND)], true);
Add('Good evening, %name%!', [cnx('greeting', DEMAND), cnx('evening', DEMAND), cnx('knowname', DEMAND)], [cnx('greeting-response', DEMAND)], true);
Add('Good night, %name%!', [cnx('greeting', DEMAND), cnx('night', DEMAND), cnx('knowname', DEMAND)], [cnx('greeting-response', DEMAND)], true);

//greeting-response

Add('Hi!', [cnx('greeting-response', DEMAND)], [], true);
Add('Hello!', [cnx('greeting-response', DEMAND)], [], true);
Add('Greetings!', [cnx('greeting-response', DEMAND)], [], true);
Add('Nice to see you!', [cnx('greeting-response', DEMAND)], [], true);
Add('Nice to meet you!', [cnx('greeting-response', DEMAND)], [], true);
Add('Glad to meet you!', [cnx('greeting-response', DEMAND)], [], true);
Add('Pleased to meet you!', [cnx('greeting-response', DEMAND)], [], true);

Add('Howdy!', [cnx('greeting-response', DEMAND), cnx('friend', DEMAND)], [], true);
Add('I''m so glad to see you!', [cnx('greeting-response', DEMAND), cnx('friend', DEMAND)], [], true);
Add('I''m so happy to see you!', [cnx('greeting-response', DEMAND), cnx('friend', DEMAND)], [], true);
Add('Just seeing you makes me happy!', [cnx('greeting-response', DEMAND), cnx('friend', DEMAND)], [], true);
Add('Glad you''re safe!', [cnx('greeting-response', DEMAND), cnx('friend', DEMAND)], [], true);
Add('I''m so glad you''re safe!', [cnx('greeting-response', DEMAND), cnx('friend', DEMAND)], [], true);

Add('Long-time no see!', [cnx('greeting-response', DEMAND), cnx('friend', DEMAND), cnx('longtimenosee', DEMAND)], [], true);
Add('What a pleasant surprise!', [cnx('greeting-response', DEMAND), cnx('friend', DEMAND), cnx('longtimenosee', DEMAND)], [], true);


//goodbye

Add('Bye!', [cnx('goodbye', DEMAND)], [cnx('goodbye-response', DEMAND)], true);
Add('Goodbye!', [cnx('goodbye', DEMAND)], [cnx('goodbye-response', DEMAND)], true);
Add('Farewell!', [cnx('goodbye', DEMAND)], [cnx('goodbye-response', DEMAND)], true);
Add('Fare thee well!', [cnx('goodbye', DEMAND)], [cnx('goodbye-response', DEMAND)], true);
Add('Goodbye, %name%!', [cnx('goodbye', DEMAND), cnx('knowname', DEMAND)], [cnx('goodbye-response', DEMAND)], true);
Add('Farewell, %name%!', [cnx('goodbye', DEMAND), cnx('knowname', DEMAND)], [cnx('goodbye-response', DEMAND)], true);
Add('I must be going.', [cnx('goodbye', DEMAND)], [cnx('goodbye-response', DEMAND)], true);

//goodbye-response

Add('Bye!', [cnx('goodbye-response', DEMAND)], [], true);
Add('Goodbye!', [cnx('goodbye-response', DEMAND)], [], true);
Add('Farewell!', [cnx('goodbye-response', DEMAND)], [], true);
Add('Until we meet again.', [cnx('goodbye-response', DEMAND)], [], true);
Add('Till we meet again.', [cnx('goodbye-response', DEMAND)], [], true);
Add('Be safe.', [cnx('goodbye-response', DEMAND)], [], true);
Add('Keep safe!', [cnx('goodbye-response', DEMAND)], [], true);
Add('May gods watch over you.', [cnx('goodbye-response', DEMAND)], [], true);
Add('Take care of yourself.', [cnx('goodbye-response', DEMAND)], [], true);
Add('Good luck!', [cnx('goodbye-response', DEMAND)], [], true);
Add('Goodbye and good luck.', [cnx('goodbye-response', DEMAND)], [], true);
Add('All the best!', [cnx('goodbye-response', DEMAND)], [], true);
Add('I''m looking forward to see you again.', [cnx('goodbye-response', DEMAND)], [], true);
Add('I hope to see you sood.', [cnx('goodbye-response', DEMAND)], [], true);
Add('I''ve enjoyed seeing you.', [cnx('goodbye-response', DEMAND)], [], true);

Add('See ya!', [cnx('goodbye-response', DEMAND), cnx('friend', DEMAND)], [], true);
Add('See you around!', [cnx('goodbye-response', DEMAND), cnx('friend', DEMAND)], [], true);
Add('See you later!', [cnx('goodbye-response', DEMAND), cnx('friend', DEMAND)], [], true);
Add('See you again!', [cnx('goodbye-response', DEMAND), cnx('friend', DEMAND)], [], true);
Add('See you soon!', [cnx('goodbye-response', DEMAND), cnx('friend', DEMAND)], [], true);
Add('Take care!', [cnx('goodbye-response', DEMAND), cnx('friend', DEMAND)], [], true);
Add('So long!', [cnx('goodbye-response', DEMAND), cnx('friend', DEMAND)], [], true);
Add('Chao!', [cnx('goodbye-response', DEMAND), cnx('friend', DEMAND)], [], true);
Add('Cheerio!', [cnx('goodbye-response', DEMAND), cnx('friend', DEMAND)], [], true);

Add('Enjoy this morning!', [cnx('goodbye-response', DEMAND), cnx('morning', DEMAND)], [], true);
Add('Have a nice day!', [cnx('goodbye-response', DEMAND), cnx('day', DEMAND)], [], true);
Add('Good night!', [cnx('goodbye-response', DEMAND), cnx('night', DEMAND)], [], true);

//howareyou

Add('How are you?', [cnx('howareyou', DEMAND)], [cnx('howareyou-response', DEMAND)], true);
Add('How are you doing?', [cnx('howareyou', DEMAND)], [cnx('howareyou-response', DEMAND)], true);
Add('How are you feeling?', [cnx('howareyou', DEMAND)], [cnx('howareyou-response', DEMAND)], true);
Add('How are you getting along?', [cnx('howareyou', DEMAND)], [cnx('howareyou-response', DEMAND)], true);
Add('What''s new?', [cnx('howareyou', DEMAND)], [cnx('howareyou-response', DEMAND)], true);
Add('Is everything ok?', [cnx('howareyou', DEMAND)], [cnx('howareyou-response', DEMAND)], true);
Add('How it''s going?', [cnx('howareyou', DEMAND), cnx('friend', DEMAND)], [cnx('howareyou-response', DEMAND)], true);
Add('What''s up?', [cnx('howareyou', DEMAND), cnx('friend', DEMAND)], [cnx('howareyou-response', DEMAND)], true);

//howareyou-response

Add('Great!', [cnx('howareyou-response', DEMAND), cnx('mood-happy', DEMAND)], [], true);
Add('I''m happy!', [cnx('howareyou-response', DEMAND), cnx('mood-happy', DEMAND)], [], true);
Add('Enjoying myself!', [cnx('howareyou-response', DEMAND), cnx('mood-happy', DEMAND)], [], true);
Add('Very well, thank you!', [cnx('howareyou-response', DEMAND), cnx('mood-happy', DEMAND)], [], true);
Add('Wonderful!', [cnx('howareyou-response', DEMAND), cnx('mood-happy', DEMAND)], [], true);
Add('Perfect!', [cnx('howareyou-response', DEMAND), cnx('mood-happy', DEMAND)], [], true);

Add('I''m fine, thanks.', [cnx('howareyou-response', DEMAND), cnx('mood-ok', DEMAND)], [], true);
Add('Thank you, I''m fine.', [cnx('howareyou-response', DEMAND), cnx('mood-ok', DEMAND)], [], true);
Add('I''m ok.', [cnx('howareyou-response', DEMAND), cnx('mood-ok', DEMAND)], [], true);
Add('I''m alright.', [cnx('howareyou-response', DEMAND), cnx('mood-ok', DEMAND)], [], true);
Add('All is well.', [cnx('howareyou-response', DEMAND), cnx('mood-ok', DEMAND)], [], true);
Add('Nothing to complain about.', [cnx('howareyou-response', DEMAND), cnx('mood-ok', DEMAND)], [], true);
Add('Not bad, thank you.', [cnx('howareyou-response', DEMAND), cnx('mood-ok', DEMAND)], [], true);
Add('Everything is good, thank you.', [cnx('howareyou-response', DEMAND), cnx('mood-ok', DEMAND)], [], true);
Add('Nothing special.', [cnx('howareyou-response', DEMAND), cnx('mood-ok', DEMAND)], [], true);

Add('I''ve seen better days.', [cnx('howareyou-response', DEMAND), cnx('mood-bad', DEMAND)], [], true);
Add('Not that I''m complaining.', [cnx('howareyou-response', DEMAND), cnx('mood-bad', DEMAND)], [], true);
Add('I don''t feel so good.', [cnx('howareyou-response', DEMAND), cnx('mood-bad', DEMAND)], [], true);
Add('On the bright side, it could be worse.', [cnx('howareyou-response', DEMAND), cnx('mood-bad', DEMAND)], [], true);
Add('So-so.', [cnx('howareyou-response', DEMAND), cnx('mood-bad', DEMAND)], [], true);

Add('I feel lonely.', [cnx('howareyou-response', DEMAND), cnx('mood-bad', DEMAND), cnx('novillagers', DEMAND)], [], true);
Add('I feel so lonely.', [cnx('howareyou-response', DEMAND), cnx('mood-bad', DEMAND), cnx('novillagers', DEMAND)], [], true);
Add('I miss home.', [cnx('howareyou-response', DEMAND), cnx('mood-bad', DEMAND), cnx('novillagers', DEMAND)], [], true);

//thankyou

Add('Thanks!', [cnx('thankyou', DEMAND)], [], true);
Add('Thank you!', [cnx('thankyou', DEMAND)], [], true);
Add('Thank you very much!', [cnx('thankyou', DEMAND)], [], true);
Add('Thank you so much!', [cnx('thankyou', DEMAND)], [], true);
Add('So kind of you!', [cnx('thankyou', DEMAND)], [], true);
Add('It''s very kind of you!', [cnx('thankyou', DEMAND)], [], true);
Add('Many thanks!', [cnx('thankyou', DEMAND)], [], true);
Add('You were most helpful!', [cnx('thankyou', DEMAND)], [], true);
Add('I''m grateful.', [cnx('thankyou', DEMAND)], [], true);
Add('I''m very grateful.', [cnx('thankyou', DEMAND)], [], true);

//thankyou-response

Add('You''re welcome!', [cnx('thankyou-response', DEMAND)], [], true);
Add('You''re always welcome!', [cnx('thankyou-response', DEMAND)], [], true);
Add('No trouble at all.', [cnx('thankyou-response', DEMAND)], [], true);
Add('It was my pleasure.', [cnx('thankyou-response', DEMAND)], [], true);
Add('Sure! Any time!', [cnx('thankyou-response', DEMAND)], [], true);
Add('Don''t mention it.', [cnx('thankyou-response', DEMAND)], [], true);
